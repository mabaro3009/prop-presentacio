package dades;

import java.io.*;
import java.nio.file.*;

/**
* Clase abstracta base de todos los gestores de persistencia. Permite gestionar
* (leer, guardar, comprobar si existe) los diversos objetos en disco.
* @author Carles Gonzalvo
*/

public abstract class GestorDades<T> {

  protected final String BASE_PATH;
  protected final String SEPARATOR;
  protected final String FILE_EXT;

  protected File f;

/**
* Crea un gestor de datos. Establece el separador(segun SO), la extension de los
* archivos a guardar, y la ruta base.
*/
  protected GestorDades() {
    SEPARATOR = File.separator;
    BASE_PATH = System.getProperty("user.dir") + SEPARATOR +  "data";
    FILE_EXT = ".ser";
  }

  /**
  * Comprueba si el objeto representado por f existe y es un fichero.
  *
  * @return true si existe y es un fichero, false si no existe o es carpeta.
  */
  protected boolean existeixObjecte() {
    return (f.exists() && !f.isDirectory());
  }

  /**
  * Comprueba si el objeto representado por f es una carpeta.
  *
  * @return true si lo es, false si es un fichero o no existe.
  */
  protected boolean existeixCarpeta() {
    return (f.exists() && f.isDirectory());
  }

  /**
  * Guarda el objeto en disco. Si este ya existe, lo sobreescribe.
  *
  * @exception IOException si hay un error de I/O
  * @param objecte objeto a guardar
  */
  protected void guardaObjecte(T objecte) throws IOException {
    try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f))) {
      out.writeObject(objecte);
    }
  }

  /**
  * Lee el objeto guardado en el archivo representado por f.
  *
  * @exception IOException si hay un de I/O
  * @exception ClassNotFoundException si la clase del objeto leido en el
  * fichero f no se corresponde con ninguna clase.
  * @return objeto leido.
  */
  @SuppressWarnings("unchecked")
  protected T llegeixObjecte() throws ClassNotFoundException, IOException {
    T result = null;
    try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(f))) {
      result = (T) in.readObject();
    }
    return result;
  }

  /**
  * Borra el objeto en disco representado por f.
  * @exception IOException si hay error de I/O
  * @exception FileNotFoundException si no existe el fichero a borrar
  */
  protected void esborraObjecte() throws IOException, FileNotFoundException {
    if (existeixObjecte()) {
      boolean deleted = f.delete();
      if (!deleted) throw new IOException("El fichero " + f.getPath() + "no se pudo borrar.");
    }
    else {
      throw new FileNotFoundException("El fichero " + f.getPath() + " no existe.");
    }
  }

}
