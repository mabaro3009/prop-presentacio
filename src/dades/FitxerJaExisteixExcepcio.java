package dades;

public class FitxerJaExisteixExcepcio extends Exception {

  private static final long serialVersionUID = 42L;

  public FitxerJaExisteixExcepcio(String message) {
      super(message);
  }

}
