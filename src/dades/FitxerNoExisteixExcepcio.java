package dades;

public class FitxerNoExisteixExcepcio extends Exception {

  private static final long serialVersionUID = 42L;

  public FitxerNoExisteixExcepcio(String message) {
      super(message);
  }

}
