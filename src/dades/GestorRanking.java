package dades;

import domini.clases.Ranking;
import java.io.*;
import java.nio.file.*;

/** Clase singleton que extiende del gestor de datos para gestionar el ranking.
* @author Carles Gonzalvo
*/

public final class GestorRanking extends GestorDades<Ranking> {

  private static GestorRanking instancia = null;
  private static String DIR = "ranking";

  private GestorRanking() {
    // Comprova carpeta
    String pathCarpeta = BASE_PATH + SEPARATOR + DIR;
    f = new File(pathCarpeta);
    if (!existeixCarpeta()) f.mkdir();
  }

  /**
  * Devuelve la unica instancia de la clase.
  * @return Instancia unica de la clase
  */
  public static GestorRanking getInstancia() {
    if (instancia == null) {
      instancia =  new GestorRanking();
    }
    return instancia;
  }

  /**
  * Guarda el ranking especificado en disco. Si no existe se crea, y si ya
  * existe este se sobreescribe.
  * @param r ranking a guardar en disco
  * @exception IOExcepcio si hay un error de I/O
  */
  public void guardaRanking(Ranking r) throws IOExcepcio {
    String fileName = "ranking" + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    if (!super.existeixObjecte()) {
      try {
          System.out.println(fullPath);
        f.createNewFile();
      } catch (IOException ioe) {
        throw new IOExcepcio(ioe.getMessage());
      }
    }
    try {
      super.guardaObjecte(r);
    } catch (IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    }
  }


  /**
  * Lee el ranking en disco.
  * @exception FitxerNoExisteixExcepcio si el fichero que representa el ranking
  * no existe.
  * @exception FitxerInvalidExcepcio si el fichero en disco que corresponde al
  * ranking existe pero es invalido.
  * @exception IOExcepcio sy hay error de I/O
  * @return ranking leido del disco
  */
  public Ranking llegeixRanking() throws FitxerNoExisteixExcepcio, FitxerInvalidExcepcio, IOExcepcio {
    Ranking resultat = null;
    String fileName = "ranking" + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);

    if (!super.existeixObjecte()) throw new FitxerNoExisteixExcepcio("No existeix el fitxer: " + fullPath);
    try {
      resultat = super.llegeixObjecte();
    } catch (IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    } catch (ClassNotFoundException cnfe) {
      throw new FitxerInvalidExcepcio(cnfe.getMessage());
    }
    return resultat;
  }

}
