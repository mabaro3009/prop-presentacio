package dades;

import domini.clases.Partida;
import java.io.*;
import java.nio.file.*;

/** Clase singleton que extiende del gestor de datos para gestionar las partidas en juego.
* @author Carles Gonzalvo
*/

public final class GestorPartida extends GestorDades<Partida> {

  private static GestorPartida instancia = null;
  private static String DIR = "partides";

  private GestorPartida() {
    // Comprova carpeta
    String pathCarpeta = BASE_PATH + SEPARATOR + DIR;
    f = new File(pathCarpeta);
    if (!existeixCarpeta()) f.mkdir();
  }

  /**
  * Devuelve la unica instancia de la clase.
  * @return Instancia unica de la clase
  */
  public static GestorPartida getInstancia() {
    if (instancia == null) {
      instancia =  new GestorPartida();
    }
    return instancia;
  }

  /**
  * Guarda la partida especificada.
  * @param p partida a guardar en disco
  * @exception IOExcepcio si hay un error de I/O
  * @exception FitxerJaExisteixExcepcio si ya existe el objeto a guardar en disco
  */
  public void guardaPartida(Partida p) throws IOExcepcio, FitxerJaExisteixExcepcio {
    String fileName = p.id + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    if (super.existeixObjecte()) throw new FitxerJaExisteixExcepcio("Ja existeix el fitxer: " + fullPath);
    try {
      f.createNewFile();
      super.guardaObjecte(p);
    } catch (IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    }
  }

  /**
  * Actualiza el fichero en disco de la partida especificada.
  * @param p partida a actualizar
  * @exception IOExcepcio si hay un error de I/O
  * @exception FitxerNoExisteixExcepcio si no existe el fichero en disco de la
  * partida a actualizar
  */
  public void actualitzaPartida(Partida p) throws IOExcepcio, FitxerNoExisteixExcepcio {
    String fileName = p.id + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    if (!super.existeixObjecte()) throw new FitxerNoExisteixExcepcio("No existeix el fitxer: " + fullPath);
    try {
      super.guardaObjecte(p);
    } catch (IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    }
  }

  /**
  * Borra la partida en disco especificada.
  * @param p partida a borrar
  * @exception IOExcepcio si hay error de I/O
  * @exception FitxerNoExisteixExcepcio si no existe el fichero en disco de la
  * partida a borrar.
  */
  public void esborraPartida(Partida p) throws IOExcepcio, FitxerNoExisteixExcepcio {
    String fileName = p.id + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    try {
      super.esborraObjecte();
    } catch (FileNotFoundException fnfe) {
      throw new FitxerNoExisteixExcepcio(fnfe.getMessage());
    } catch(IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    }
  }

  /**
  * Lee la partida en disco especificada por su id.
  * @param idPartida id de la partida a leer
  * @exception FitxerNoExisteixExcepcio si el fichero que representa la partida
  * idPartida no existe.
  * @exception FitxerInvalidExcepcio si el fichero en disco que corresponde a la
  * partida existe pero es invalido.
  * @exception IOExcepcio sy hay error de I/O
  * @return partida leida del disco
  */
  public Partida llegeixPartida(String idPartida) throws FitxerNoExisteixExcepcio, FitxerInvalidExcepcio, IOExcepcio {
    Partida resultat = null;
    String fileName = idPartida + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    if (!super.existeixObjecte()) throw new FitxerNoExisteixExcepcio("No existeix el fitxer: " + fullPath);
    try {
      resultat = super.llegeixObjecte();
    } catch (IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    } catch (ClassNotFoundException cnfe) {
      throw new FitxerInvalidExcepcio(cnfe.getMessage());
    }
    return resultat;
  }

}
