package dades;

import domini.clases.Jugador;
import java.io.*;
import java.nio.file.*;

/** Clase singleton que extiende del gestor de datos para gestionar los jugadores.
* @author Carles Gonzalvo
*/

public final class GestorJugador extends GestorDades<Jugador> {

  private static GestorJugador instancia = null;
  private static String DIR = "jugadors";

  private GestorJugador() {
    // Comprova carpeta
    String pathCarpeta = BASE_PATH + SEPARATOR + DIR;
    f = new File(pathCarpeta);
    if (!existeixCarpeta()) f.mkdir();
  }

  /**
  * Devuelve la unica instancia de la clase.
  * @return Instancia unica de la clase
  */
  public static GestorJugador getInstancia() {
    if (instancia == null) {
      instancia =  new GestorJugador();
    }
    return instancia;
  }

  /**
  * Guarda el jugador especificado.
  * @param j jugador a guardar en disco
  * @exception IOExcepcio si hay un error de I/O
  * @exception FitxerJaExisteixExcepcio si ya existe el objeto a guardar en disco
  */
  public void guardaJugador(Jugador j) throws IOExcepcio, FitxerJaExisteixExcepcio {
    String fileName = j.nom + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    if (super.existeixObjecte()) throw new FitxerJaExisteixExcepcio("Ja existeix el fitxer: " + fullPath);
    try {
      f.createNewFile();
      super.guardaObjecte(j);
    } catch (IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    }
  }

  /**
  * Actualiza el fichero en disco del jugador especificado.
  * @param j jugador a actualizar
  * @exception IOExcepcio si hay un error de I/O
  * @exception FitxerNoExisteixExcepcio si no existe el fichero en disco del
  * jugador a actualizar
  */
  public void actualitzaJugador(Jugador j) throws IOExcepcio, FitxerNoExisteixExcepcio {
    String fileName = j.nom + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    if (!super.existeixObjecte()) throw new FitxerNoExisteixExcepcio("No existeix el fitxer: " + fullPath);
    try {
      super.guardaObjecte(j);
    } catch (IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    }
  }

  /**
  * Borra el jugador en disco  especificado.
  * @param j jugador a borrar
  * @exception IOExcepcio si hay error de I/O
  * @exception FitxerNoExisteixExcepcio si no existe el fichero en disco del
  * jugador a borrar.
  */
  public void esborraJugador(Jugador j) throws IOExcepcio, FitxerNoExisteixExcepcio {
    String fileName = j.nom + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    try {
      super.esborraObjecte();
    } catch (FileNotFoundException fnfe) {
      throw new FitxerNoExisteixExcepcio(fnfe.getMessage());
    } catch(IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    }
  }

  /**
  * Lee el jugador en disco especificado por su nombre.
  * @param nomJugador nombre del jugador a leer
  * @exception FitxerNoExisteixExcepcio si el fichero que representa el jugador
  * nomJugador no existe.
  * @exception FitxerInvalidExcepcio si el fichero en disco que corresponde al
  * jugador existe pero es invalido.
  * @exception IOExcepcio sy hay error de I/O
  * @return jugador leido del disco
  */
  public Jugador llegeixJugador(String nomJugador) throws FitxerNoExisteixExcepcio, FitxerInvalidExcepcio, IOExcepcio {
    Jugador resultat = null;
    String fileName = nomJugador + FILE_EXT;
    String fullPath = BASE_PATH + SEPARATOR + DIR + SEPARATOR + fileName;
    f = new File(fullPath);
    if (!super.existeixObjecte()) throw new FitxerNoExisteixExcepcio("No existeix el fitxer: " + fullPath);
    try {
      resultat = super.llegeixObjecte();
    } catch (IOException ioe) {
      throw new IOExcepcio(ioe.getMessage());
    } catch (ClassNotFoundException cnfe) {
      throw new FitxerInvalidExcepcio(cnfe.getMessage());
    }
    return resultat;
  }

}
