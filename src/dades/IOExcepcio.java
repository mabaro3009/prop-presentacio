package dades;

public class IOExcepcio extends Exception {

  private static final long serialVersionUID = 42L;

  public IOExcepcio(String message) {
      super(message);
  }

}
