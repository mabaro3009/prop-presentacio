package domini.clases;

import java.lang.*;
import java.util.*;
import java.io.Serializable;

/** Clase para el ranking. Singleton, guarda una lista de los registros de las partidas.
* @author Daniel Tarrés
*/

public class Ranking implements Serializable{

    private static final long serialVersionUID = 42L;
    private static Ranking instancia = null;
    private List<Registre> registres;

    public Ranking () {
        registres = new ArrayList<Registre>();
    }

    public static Ranking getInstancia() {
      if (instancia == null) {
        instancia = new Ranking();
      }
      return instancia;
    }

    /** imprimeix ranking
    * @param quants Nombre de registres que mostra
    */
    public void mostra_rank(int quants){ //quants es per si vols fer el top 10, etc. Si els vols tots, quants = 0
        System.out.println("Ranking");
        System.out.println("Posició\tUsuari\t\tPunts");
        System.out.println("--------------------");
        if (quants > registres.size() || quants == 0){
            for (int i = 0; i < registres.size(); i++){
                System.out.print("    " + (i+1) + "\t");
                registres.get(i).imprimeix();
            }
        }else{
            for (int i = 0; i < quants; i++){
                System.out.print("    " + (i+1) + "\t");
                registres.get(i).imprimeix();
            }
        }
        System.out.println("--------------------");
    }

    public List <String> getRankList(){
        List <String> llista = new ArrayList<String>();
        char tab = 9;
        for (int i = 0; i < registres.size(); i++){
                llista.add("" + (i+1) + " - " + registres.get(i).getuser() + " - " + registres.get(i).getPunts());
            }
        return llista;
    }
    
    public void afegir(Registre r){
        int i = 0;
        boolean segueix = true;
        while(i < registres.size() && segueix){
            if (registres.get(i).getpuntuacio() < r.getpuntuacio()){
                segueix = false;
                registres.add(i, r);
            }
            i++;
        }
        if (segueix) registres.add(r);
//         System.out.println("Registre afegit");
//         System.out.println("mostro rank");
        mostra_rank(0);

    }
}
