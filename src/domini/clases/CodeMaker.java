package domini.clases;

import java.util.Scanner;
import java.lang.*;
import java.util.*;

/** Subclase de Partida. Desde aquí se juega como CodeMaker.
 * @author Marc Badia
 */

public class CodeMaker extends Partida {

	private static final long serialVersionUID = 42L;

	public CodeMaker(String id, String user){
		super(id, user);
	}
	
	/**Inici de la partida en CodeMaker
	*/
	public int jugar() throws Exception{
	  get_solucio();
	  t.imprimeix_solucio();
	  t.imprimeix_tauler();
	  t.instMaker();
	  play();
	  return 0;
	}
	
	//L'usuari introdueix la solució
	public void get_solucio() throws Exception{
	  Scanner var = new Scanner(System.in);
	  int c;
	  System.out.println("Introdueix la teva solució (per exemple: 1 2 3 4)");
	  List<Integer> aux = new ArrayList<Integer>();;
	  for (int i = 0; i < nColumnes; ++i){
	    c = var.nextInt();
	    aux.add(c);
	  }
	  boolean b = true;
	  for(int i = 0; i < nColumnes && b; ++i){
	    if(colorOk(aux.get(i), i)) t.setColor(0, i, aux.get(i), 0);
	    else{ b = false; get_solucio(); }
	    
	  }
	  
	}

	public boolean colorOk(int c, int i) throws Exception{
	  if(c == 0){
	    System.out.println("Can't have a null color");
	    return false;
	  }
	  if(c < 0 || c > nColors){
	    System.out.println("0 < color <= nColors");
	    return false;
	  }
	  if(t.colorExists(c, i)){
	    System.out.println("Can't have duplicated colors");
	    return false;
	  }
	  return true;
	}
	
	/**Funció principal del CodeMaker. L'usuari va escrivint comandes i el sistema respon adequadament.
	* "Salir" -> Surt de l'aplicació
	* "Guardar" -> Guarda l'estat actual de la partida
	* "Play" -> Col·loca casella a columna
    * "Check" -> Sistema comprova que la jugada del usuari sigui correcte i calcula la seguent fila
    */
	public void play() throws Exception{
	  Scanner var = new Scanner(System.in);
	  t.algorisme(fila_actual);
	  while(fila_actual < nFiles && !solucio_trobada){
	    System.out.println(" ");
	    t.imprimeix_solucio();
	    t.imprimeix_tauler();
	    t.instMaker();
	    System.out.println(" ");
	    String c = var.next();
	    if(c.equals("Salir")) System.exit(0);
	    else if (c.equals("Check")){
	      if(t.comprova(fila_actual)){
		if(t.solucio_correcte(fila_actual)){
		  System.out.println("Solucio trobada!");
		  solucio_trobada = true;
		} else {
		  ++fila_actual;
		  if(fila_actual == nFiles){
		    System.out.println("Has perdut :(");
		    System.exit(0);
		  }
		  t.algorisme(fila_actual);
		}
	      }
	      else System.out.println("ERROR: La comprobación no es correcta, por favor vuelve a realizarla");
	    }
	    else if (c.equals("Guardar")){}
	    else if (c.equals("Play")){
	      int v;
	      for(int i = 0; i < nColumnes; ++i){
		v = var.nextInt();
		t.setColor(fila_actual, i, v, 2);
	      }
	    }
	    else System.out.println("Invalid command");
	    
	 }
      }


}
