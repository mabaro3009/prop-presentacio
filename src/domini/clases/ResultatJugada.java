package domini.clases;

/**
 * Representa el resultado de jugar una fila, en rojos (color y posicion acertados) y blancos (color acertado, posicion incorrecta)
 * @author Carles Gonzalvo
 */
public class ResultatJugada {
    
    private final int vermells;
    private final int blancs;
    
    public ResultatJugada(int vermells, int blancs) {
        this.vermells = vermells;
        this.blancs = blancs;
    }
    
    public int getBlancs() {
        return blancs;
    }
    
    public int getVermells() {
        return vermells;
    }
    
    @Override
    public String toString() {
        return "Vermells: " + vermells + " Blancs: " + blancs;
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof ResultatJugada)) return false;
        
        ResultatJugada altre = (ResultatJugada) other;
        return vermells == altre.vermells && blancs == altre.blancs;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.vermells;
        hash = 97 * hash + this.blancs;
        return hash;
    }
}
