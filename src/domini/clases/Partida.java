package domini.clases;

import java.util.Scanner;
import java.lang.*;
import java.util.*;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Clase abstracta de Partida. Las subclases són CodeBreaker y CodeMaker
@author Marc Badia
*/

public abstract class Partida implements Serializable {

  private static final long serialVersionUID = 42L;
  public String id;
  public int puntuacio;
  public String user;
  protected int nFiles;
  protected int nColumnes;
  protected int nColors;
  protected Tauler t;
  protected int fila_actual;
  protected boolean solucio_trobada;
  protected boolean guardar;
  protected int n;

  public Partida(String id, String user){
	  this.id = id;
	  this.fila_actual = 0;
	  this.solucio_trobada = false;
	  this.guardar = false;
	  this.user = user;
	  puntuacio = 0;
  }

  public String getID(){
	return this.id;
  }

  public String getuser(){
	return this.user;
  }

  public int getPuntuacio(){
	return this.puntuacio;
  }

  public int getnFiles(){
	return this.nFiles;
  }

  public int getnColumnes(){
	return this.nColumnes;
  }

  public int getnColors(){
	return this.nColors;
  }

  public int getfilaActual(){
	return this.fila_actual;
  }
  
  public String getCodi(int fila_actual){
      try {
          System.out.println("filaaaaaa " + fila_actual);
          t.algorisme(fila_actual);
      } catch (Exception ex) {
          Logger.getLogger(Partida.class.getName()).log(Level.SEVERE, null, ex);
      }
      return t.getCodi();
  }
  
  public void setfilaActual(int fila_nova){
      this.fila_actual = fila_nova -1;
  }

  public void crear_tauler(int f, int c, int colors)throws Exception{
	  this.nFiles = f;
	  this.nColumnes = c;
	   this.nColors = colors;
	  t = new Tauler(nFiles, nColumnes, nColors);
	  System.out.println(" ");
	  //t.imprimeix_tauler();

  }
  public void crea_solucio() throws Exception{
      try{
      t.solucio_random();
      }catch (Exception e){
          System.out.println("hi ha algun problema al fer la solució random :(((");
      }
  }
  
  public void imprimeix_solucio_vista(){
      t.imprimeix_solucio_vista();
  }
    
  public void introdueixFila(List<Integer> posicions, int fila){
      for (int i = 0; i < posicions.size(); i++){
          try{
            t.setColor(fila-1, i, posicions.get(i), 1);
          }catch (Exception e){
              System.out.print(i);
              System.out.println("hi ha algun problema en el moment de fer el setcolor");
              e.printStackTrace();
          }
      }
  }
  
  public List<Integer> check_vista(List<Integer> posicions) throws Exception{
      return t.check_vista(posicions);
  }
  
  public int get_color_solucio(int posicio){
      return t.get_color_solucio(posicio);
  }
  
  public int retorna_puntuacio(int fila){
      return 0;
  }
  
    public void afegeix_pista_color(){
    }
    public void afegeix_pista_posicio_color(){
    }
    public int get_color_posicio(int fila, int posicio){
        return t.get_color_posicio(fila, posicio);
    }
    public void set_encert_pos_col(int nfila, int n){
        t.set_encert_pos_col(nfila, n);
    }
    public void set_encert_pos(int nfila, int n){
        t.set_encert_pos(nfila, n);
    }
    public int get_encert_pos_col(int nfila){
        return t.get_encert_pos_col(nfila);
    }
    public int get_encert_pos(int nfila){
        return t.get_encert_pos(nfila);
    }
    public void set_solucio(List<Integer> codi_maker){
        t.set_solucio(codi_maker);
    }
    
    public void setFilaComp(int fila_actual, String comp, int avis_vermell, int avis_blanc){
        t.setFilaComp(fila_actual, comp, avis_vermell, avis_blanc);
    }
  
    public boolean comprova(int fila_actual, int avis_vermell, int avis_blanc){
        t.set_blanc(avis_blanc);
        t.set_vermell(avis_vermell);
        boolean retorn = false;
      try {
          retorn =  t.comprova2(fila_actual);
      } catch (Exception ex) {
          Logger.getLogger(Partida.class.getName()).log(Level.SEVERE, null, ex);
      }
      return retorn;
    }
  /** Operació abstracta per CodeBreaker i CodeMaker
  */
  abstract int jugar() throws Exception;

}
