package domini.clases;

import java.util.Scanner;
import java.lang.*;
import java.util.*;

/**
* Esta es una subclase de Partida
* Desde aquí se juega como CodeBreaker
* @author Marc Badia
*/

public class CodeBreaker extends Partida {

	private static final long serialVersionUID = 42L;
	private int pistes_colors;
	private int pistes_pos_col;

	public CodeBreaker(String id, String user){
		super(id, user);
		pistes_colors = 0;
		pistes_pos_col = 0;
		n = 0;
	}
        public void afegeix_pista_color(){
            pistes_colors++;
        }
        public void afegeix_pista_posicio_color(){
            pistes_pos_col++;
        }
	
	/**Funció principal del CodeBreaker. L'usuari va escrivint comandes i 
    * el sistema respon adequadament.
	* "Salir" -> Surt de l'aplicació
    * "Guardar" -> Guarda l'estat actual de la partida
	* "Play" -> Col·loca casella a columna
	* "Check" -> Sistema retorna la comprovació de la jugada
	* "Pista" -> Sistema retorna una pista
	*/
	public int jugar() throws Exception{
        try{
        if (!t.sol_random_creada)
            t.solucio_random();
        t.imprimeix_tauler();
        t.instBreaker();
        Scanner var = new Scanner(System.in);
        String c = var.next();
       // n = 0; //caselles colocades
        // while(c != "Check" && n != nColumnes){
        while(fila_actual < nFiles && !solucio_trobada && !guardar){
            System.out.println(" ");
            if(c.equals("Salir")) System.exit(0);
            else if(c.equals("Check")){
                if(n < nColumnes) System.out.println("Aun faltan por colocar casillas " + n);
                else {
		    
                    solucio_trobada = t.check(fila_actual);
                    if (solucio_trobada){ //HA TROBAT LA SOLUCIO -> HA GUANYAT
                        t.imprimeix_tauler();
                        System.out.println("HAS TROBAT LA SOLUCIÓ! :D");
                        System.out.print("Puntuació: ");
                        calcula_puntuacio(fila_actual);
                        System.out.println(puntuacio);

                        //Registre r = new Registre(id, puntuacio, user);

                        //System.exit(0);
                    }
                    ++fila_actual;
                    n = 0;
                }
            }else if(c.equals("Play")){
                int columna = var.nextInt();
                int color = var.nextInt();
                System.out.println("Fila actual = " + fila_actual);
                if(color <= nColors && color > 0 && columna > 0 && columna <= nColumnes){
                    int val_ant = t.getColor(fila_actual, columna-1, 1);
                    t.setColor(fila_actual, columna-1, color, 1);
                            //int val_ant = t.colocaCasella(fila_actual, columna-1, color); //Coloca la casella (fila_actual, columna-1) del color color i retorna el color inicial
                            if(val_ant == 0) ++n;
                        }
                    else{
                        System.out.println("Columna = (0 < Columna <= nColumnas");
                        System.out.println("Color = (0 < Color <= nColores)");
                    }
            }else if(c.equals("Pista")){ //AIXO ES PODRIA FER AMB EL PATRO ESTRATEGIA PERO NO HO VEIG CLAR
                System.out.println("1 - Pista color");
                System.out.println("2 - Pista posición + color");
                System.out.println("3 - Pista color de una posición dada");
                int m = var.nextInt();
                int columna_pista;
                boolean b = true;
                if(m == 3){
                    System.out.println("Introduzca la posición de la que quiera saber el color:");
                    columna_pista = var.nextInt() - 1;
                    if (columna_pista < 0 || columna_pista >= nColumnes){
                        b = false;
                        System.out.println("Posición = (0 < Columna <= nColumnas)");
                    }
                }else{
                    Random rn = new Random();
                    columna_pista = rn.nextInt(nColumnes);
                }
                //System.out.println(columna_pista);
                if(b){
                    int color_pista = t.getPista(columna_pista);
                    //System.out.println("HOLAa");
                    if(m == 1){
                        System.out.println("En la solución está el color " + color_pista);
                        pistes_colors++;
                        if (pistes_colors > nColors) pistes_colors = nColors;
                    }else if(m == 2){
                        System.out.println("En la solución está la combinación posición + color: " + (columna_pista+1) + " " + color_pista);
                        pistes_pos_col++;
                        if (pistes_pos_col > nColumnes) pistes_pos_col = nColumnes;
                    }else if(m == 3){
                        System.out.println("La columna " + (columna_pista+1) + " tiene el color " + color_pista);
                        pistes_pos_col++;
                        if (pistes_pos_col > nColumnes) pistes_pos_col = nColumnes;
                    }
                }
            }else if (c.equals("Guardar")){
                guardar = true;
            
            }else System.out.println("Invalid command");

            if(fila_actual == nFiles && !solucio_trobada){ //HA ACABAT LA PARTIDA I HA PERDUT
                System.out.println("Has perdut :,(");
                System.out.println("Puntuació: " + puntuacio);
                System.exit(0);
            }
            else if (!solucio_trobada && !guardar){

                System.out.println(" ");
                t.imprimeix_tauler();
                t.instBreaker();
                c = var.next();
            }
        }


        }catch(Exception e){
        }
	
	return puntuacio;
	}
	public void calcula_puntuacio(int fila){
            puntuacio = (10000 / (fila+1)) * (nColumnes-pistes_pos_col) * (nColors - pistes_colors);
	}
        public int retorna_puntuacio(int fila){
            calcula_puntuacio(fila);
            return puntuacio;
        }

}
