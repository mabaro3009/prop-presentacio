package domini.clases;

/**
 *
 * @author Carles Gonzalvo
 */
public interface AlgorismeBreaker {

    /**
    * Devuelve la siguiente jugada segun el algoritmo
    * @param resultatUltimaJugada resultado obtenido al probar la ultima jugada indicada por el algoritmo. En caso de ser la primera jugada, no importa
    * @return fila que contiene los colores a jugar
    */
    public Fila seguentJugada(ResultatJugada resultatUltimaJugada);
    
}
