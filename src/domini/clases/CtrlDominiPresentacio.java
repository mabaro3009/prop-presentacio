/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domini.clases;

import dades.FitxerInvalidExcepcio;
import dades.FitxerJaExisteixExcepcio;
import dades.FitxerNoExisteixExcepcio;
import dades.GestorJugador;
import dades.GestorPartida;
import dades.GestorRanking;
import dades.IOExcepcio;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author User
 */
@Entity
public class CtrlDominiPresentacio implements Serializable {
    @Id
    private Jugador j;
    private Ranking r;
    private Partida p;
    private AlgorismeBreaker alg;

    public CtrlDominiPresentacio(){
        try {
        r = GestorRanking.getInstancia().llegeixRanking();
      } catch(FitxerNoExisteixExcepcio e) {
        r = Ranking.getInstancia();
        nouRanking(r);
      } catch(FitxerInvalidExcepcio | IOExcepcio e) {
        e.printStackTrace();
        System.exit(1);
      }
    }

    public Jugador getJ() {
        return j;
    }

    public void setJ(Jugador j) {
        this.j = j;
    }
    
    private void nouRanking(Ranking r) {
      try {
        GestorRanking.getInstancia().guardaRanking(r);
      } catch (IOExcepcio ioe) {
        ioe.printStackTrace();
        System.exit(1);
      }
    }
    
    public boolean crearNouUsuari(String nom, String pass){
        boolean usuariCreat = false;
        Jugador j = new Jugador(nom, pass);
        try {
          GestorJugador.getInstancia().guardaJugador(j);
          this.j = j;
          usuariCreat = true;
        } catch (IOExcepcio ioe) {
          ioe.printStackTrace();
          System.exit(1);
        } catch (FitxerJaExisteixExcepcio fjee) {
          System.out.println("Error: el usuario " + nom + " ya existe.");
        }

        return usuariCreat;
    }
    
    public List <String> obtenirRanking(){
        
        
        return r.getRankList();
    }
    
    public boolean logIn(String nom, String pass){
        boolean ret = false;
        try {
          j = GestorJugador.getInstancia().llegeixJugador(nom);
        } catch (FitxerNoExisteixExcepcio fnee) {
            System.out.println();
            System.out.println("Error: el usuario " + nom + " no existe.");
            System.out.println();
            return false;
        } catch (FitxerInvalidExcepcio | IOExcepcio e) {
          e.printStackTrace();
          System.exit(1);
        }

        if (!j.comprovaContrassenya(pass)) {
          System.out.println();
          System.out.println("Error: contraseña incorrecta. Al carrer.");
          System.out.println();
        } else {
          System.out.println("Bienvenid@, " + nom);
          this.j = j;
          ret = true;
        }

        return ret;
    }
    
    public String getUser(){
        return j.getNom();
    }

    public int getMaxP(){
        return j.getMaxPuntuacio();
    }
    
    public void borrar() throws FitxerNoExisteixExcepcio, IOExcepcio{
        GestorJugador.getInstancia().esborraJugador(j);
    }
    
    private void sortir() {
      try {
        GestorRanking.getInstancia().guardaRanking(r);
      } catch (IOExcepcio e) {
        e.printStackTrace();
        System.exit(1);
      }
      System.exit(0);
    }
    
    public void canviPass(String newPass) {
      j.canviaContrassenya(newPass);
      System.out.println("Contraseña cambiada satisfactoriamente jajaj.");
       try {
            GestorJugador.getInstancia().actualitzaJugador(j);
        } catch (IOExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FitxerNoExisteixExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void crearPartida(int files, int columnes, int colors, boolean mode){
        this.p = j.crearPartida(files, columnes, colors, mode);
    }
    
    public void introdueixFila(List<Integer> posicions, int fila){
        p.introdueixFila(posicions, fila);
        p.setfilaActual(fila);

    }
    public List<Integer> comprovaFila(List<Integer> posicions)throws Exception{
        return p.check_vista(posicions);
    }
    
    public int get_color_solucio(int posicio){
        return p.get_color_solucio(posicio);
    }
    public int getpuntuacio(int fila){
        return p.retorna_puntuacio(fila);
    }
    public void afegeix_pista_color(){
        p.afegeix_pista_color();
    }
    public void afegeix_pista_posicio_color(){
        p.afegeix_pista_posicio_color();
    }
    public boolean afegeix_puntuacio_jugador(int puntuacio){
        return j.afegeix_puntuacio_jugador(puntuacio);
    }
    public void afegir_ranking(int puntuacio){
        Registre reg = new Registre(p.getID(), puntuacio, j.getNom());
        r.afegir(reg);
        try {
            GestorRanking.getInstancia().guardaRanking(r);
        } catch (IOExcepcio e) {
            e.printStackTrace();
            System.exit(1);
      }
        
        try {
            GestorJugador.getInstancia().actualitzaJugador(j);
        } catch (IOExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FitxerNoExisteixExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void guardar_partida(){
        j.afegeixIds(p.getID());
        try {
          GestorPartida.getInstancia().guardaPartida(p);
        } catch (IOExcepcio ioe) {
          ioe.printStackTrace();
          System.exit(1);
        } catch (FitxerJaExisteixExcepcio fjee) {
          actualitzaNovaPartida(p);
        }
        try {
            GestorJugador.getInstancia().actualitzaJugador(j);
        } catch (IOExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FitxerNoExisteixExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void actualitzaNovaPartida(Partida p) {
      try {
        GestorPartida.getInstancia().actualitzaPartida(p);
      } catch (IOExcepcio | FitxerNoExisteixExcepcio e) {
        e.printStackTrace();
        System.exit(1);
      }
    }
    public List<String> carregar_partides_jugador(){
        return j.getIDsPartides();
    }
    
    public void carrrega_partida(String partida_seleccionada){
        try {
            this.p = GestorPartida.getInstancia().llegeixPartida(partida_seleccionada);
        } catch (FitxerNoExisteixExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FitxerInvalidExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOExcepcio ex) {
            Logger.getLogger(CtrlDominiPresentacio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public int getfiles(){
        return p.getnFiles();
    }
    public int getcolors(){
        return p.getnColors();
    }
    public int getcolumnes(){
        return p.getnColumnes();
    }
    public int getfila_actual(){
        return p.getfilaActual()+2;
    }
    
    public List<Integer> get_colors_fila(int i){
        List<Integer> colors_ret = new ArrayList<Integer>(9);
        int ii = 0;
        for (ii = 0; ii < p.getnColumnes(); ii++){
            colors_ret.add(ii, p.get_color_posicio(i-1, ii));
        }
        while(ii < 9){
            colors_ret.add(ii, 0);
            ii++;
        }
        return colors_ret;
    }
    
    public int get_color_posicio(int fila, int posicio){
        return p.get_color_posicio(fila, posicio);
    }
    
    public void set_encert_pos_col(int nfila, int n){
        p.set_encert_pos_col(nfila, n);
    }
    public void set_encert_pos(int nfila, int n){
        p.set_encert_pos(nfila, n);
    }
    public int get_encert_pos_col(int nfila){
        return p.get_encert_pos_col(nfila);
    }
    public int get_encert_pos(int nfila){
        return p.get_encert_pos(nfila);
    }
    public void set_solucio(List<Integer> codi_maker){
        p.set_solucio(codi_maker);
    }
    
    public List<Integer> getCodi(int fila_actual){
        String codi = p.getCodi(fila_actual);
        List<Integer> code = new ArrayList<Integer>();
        int i = 0;
        while(i < codi.length()){
            code.add(Character.getNumericValue(codi.charAt(i)));
            ++i;
        }
        while(i < 9){
            code.add(0);
            ++i;
        }
        for(int j = 0; j < code.size(); ++j){
             System.out.println(code.get(j));
        }
        return code;
    }

    public void setFilaComp(int fila_actual, int avis_vermell, int avis_blanc, int columnes) {
        int avis_negre = columnes - avis_vermell - avis_blanc;
        String comp = "";
        for(int i = 0; i < avis_vermell; ++i){
            comp += '2';
        }
        for(int i = 0; i < avis_blanc; ++i){
            comp += '1';
        }
        for(int i = 0; i < avis_negre; ++i){
            comp += '0';
        }
        System.out.println("fila actual: " + fila_actual + "comp : " + comp);
        p.setFilaComp(fila_actual, comp, avis_vermell, avis_blanc);
    }
    public boolean comprova(int fila_actual, int avis_vermell, int avis_blanc){
        return p.comprova(fila_actual, avis_vermell, avis_blanc);
    }
    
    public List<Integer> seguentJugada(int avis_vermell, int avis_blanc) {
        System.out.println("pensant");
        Fila resultat = alg.seguentJugada(new ResultatJugada(avis_vermell, avis_blanc));
        
        List<Integer> codi = new ArrayList<Integer>(9);
        int i = 0;
        
        while (i < resultat.mida()){
            codi.add(i, resultat.getColor(i));
            i++;
        }
        while(i < 9){
            codi.add(i, 0);
            i++;
        }
        return codi;
    }
    
    public void instanciaAlgoritme(int nCaselles, int nColors) {
        alg = new AlgorismeBreakerFiveGuess(nCaselles, nColors, false);
    }
}