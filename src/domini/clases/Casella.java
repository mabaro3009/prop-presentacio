package domini.clases;

import java.io.Serializable;


/**
* Esta clase nos permite tener las casillas como objeto, de manera que 
* sea más facil rellenar las diferentes filas
* @author Daniel Tarres, Carles Gonzalvo

*/

public class Casella implements Serializable{

  private static final long serialVersionUID = 42L;
  private int color; 

  public Casella() {
    color = 0;
  }
  
  public Casella(int color) {
      this.color = color;
  }
  
  @Override
  public boolean equals(Object other) {
      if (other == null) return false;
      if (other == this) return true;
      if (!(other instanceof Casella)) return false;
      
      Casella altra = (Casella) other;
      return this.color == altra.color;
  }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + this.color;
        return hash;
    }

  /**
  * Esta función pone un color en la casilla
  * @param c es el color
  */
  public void setColor(int c) {
    //if (c<0) throw new Exception("Color invalid");
    color = c;
  }

  public int getColor() {
    return color;
  }

}
