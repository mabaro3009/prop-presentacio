package domini.clases;

import java.util.Scanner;
import java.lang.*;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/** La clase del objeto Jugador
* @author Daniel Tarrés
*/

public class Jugador implements Serializable {

    private static final long serialVersionUID = 42L;
    public String nom; // Cal que sigui public per CtrlJugador.java
    private String pass;
    protected List<String> id_partides; // Partides guardades
    private int max_puntuacio;

    public Jugador(String nom, String pass) {
        this.nom = nom;
        this.pass = pass;
        max_puntuacio = 0;
        id_partides = new ArrayList<String>();
    }
    
    public List<String> getIDsPartides(){
        return id_partides;
    }

    public void canviaContrassenya(String pass) {
      this.pass = pass;
    }

    public boolean comprovaContrassenya(String pass) {
      return (pass.equals(this.pass));
    }

    public int getMaxPuntuacio() {
      return max_puntuacio;
    }
    
    public String getNom(){
        return nom;
    }
    
    public String getPass(){
        return pass;
    }
    
    public void afegeixIds(String idd){
        id_partides.add(idd);
    }

    /** Crea la partida
    */
    public Partida crear_partida() throws Exception {
        Partida p = null;

        Scanner var = new Scanner(System.in);
        System.out.println(" ");
        System.out.println("1 - CodeBreaker");
        System.out.println("2 - CodeMaker");
        int mode = var.nextInt(); //1 = CodeBreaker 2 = CodeMaker
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
        Date date = new Date();
        if(mode == 1){
            String id_partida = "BR_" + nom + "_" + dateFormat.format(date);
            p = new CodeBreaker(id_partida, nom);
        }
        else if (mode == 2){
            String id_partida = "MK_" + nom + "_" + dateFormat.format(date);
            p = new CodeMaker(id_partida, nom);
        }
	 creaTauler(p);
	
        int puntss = p.jugar();
        if (p.solucio_trobada && puntss > max_puntuacio) max_puntuacio = puntss;

        return p;
    }
	public void creaTauler(Partida p) throws Exception{
		Scanner var = new Scanner(System.in);
		boolean b = true;
		System.out.println(" ");
		System.out.println("Número de Filas:");
        	int f = var.nextInt();
		if(f<0){b=false; System.out.println("Dato inválido");}
        	System.out.println("Número de Columnas:");
        	int c = var.nextInt();
		if(c<0){b=false; System.out.println("Dato inválido");}
        	System.out.println("Número de Colores (Colores >= Columnas):");
        	int colors = var.nextInt();
		if(colors<c){b=false; System.out.println("Dato inválido");}
        	if(b) p.crear_tauler(f, c, colors);
		else creaTauler(p);
	}
        
        public Partida crearPartida(int files, int columnes, int colors, boolean mode){
            Partida p = null;
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
            Date date = new Date();
            if(mode == true){
                 String id_partida = "BR_" + nom + "_" + dateFormat.format(date);
                p = new CodeBreaker(id_partida, nom);
            }
            else if (mode == false){
                 String id_partida = "MK_" + nom + "_" + dateFormat.format(date);
                 p = new CodeMaker(id_partida, nom);
            }
            try {
                p.crear_tauler(files, columnes, colors);
            } catch (Exception ex) {
                Logger.getLogger(Jugador.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (mode){
                try{
                      p.crea_solucio();
                }catch (Exception e){
                      e.printStackTrace();
                }
            }
        
        return p;
        }

    /** Carrega la partida
    */
    public Partida carrega_partida(Partida part_xd) throws Exception{
        id_partides.remove(part_xd.getID());
        int puntss = part_xd.jugar();

        if (part_xd.solucio_trobada && puntss > max_puntuacio) max_puntuacio = puntss;

        return part_xd;
    }

    /** Imprimeix ids de partides
    */
    public void imprimeix_ids(){
        int i = 0;
        while(i < id_partides.size()){
            System.out.println("\t" + (i+1) + " - " + id_partides.get(i));
            i++;
        }
        if (i == 0) System.out.println("No hi ha partides guardades");
    }
    
    public boolean afegeix_puntuacio_jugador(int puntuacio){
        boolean canvia_maxim = false;
        if (puntuacio > max_puntuacio){
            canvia_maxim = true;
            max_puntuacio = puntuacio;
        }
        return canvia_maxim;
    }

}
