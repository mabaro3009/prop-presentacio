package domini.clases;

import java.util.Scanner;
import java.io.Serializable;

/** Clase para los registros de la partida. Guarda la información principal de las partidas ganadas.
* @author Daniel Tarrés
*/

public class Registre implements Serializable {

    private static final long serialVersionUID = 42L;
    private String id;
    private String user;
    private int puntuacio;

    public Registre (String id, int puntuacio, String user){
        this.id = id;
        this.puntuacio = puntuacio;
        this.user = user;
    }

    public String getid(){
        return id;
    }

    public int getpuntuacio(){
        return puntuacio;
    }
    
    public String getPunts(){
        return Integer.toString(puntuacio);
    }
    
    public String getuser(){
        return user;
    }

    public void imprimeix(){
        System.out.print(user);
        if (user.length() < 8) System.out.print("\t");
        System.out.println("\t" + puntuacio);
    }

}
