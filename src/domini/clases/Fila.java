package domini.clases;

import java.util.*;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Clase del objeto fila para que sea más facil modificar el tablero.
* @author Daniel Tarrés, Carles Gonzalvo
*/

public class Fila implements Serializable {

  private static final long serialVersionUID = 42L;
  private final int nCaselles;
  private final List<Casella> caselles;
  private int encert_pos_col;
  private int encert_pos;

  /**
   * Crea una nueva fila vacia de nCaselles casillas
   * @param nCaselles numero de casillas
   */
  public Fila(int nCaselles) {
    this.nCaselles = nCaselles;
    caselles = new ArrayList<>(nCaselles);
    encert_pos_col = 0;
    encert_pos = 0;

    for (int i=0; i<nCaselles; i++) {
      caselles.add(new Casella());
    }
  }
  
  /**
   * Crea una nueva fila a partir de colors
   * @param colors lista de enteros que representan colores
   */
  public Fila(List<Integer> colors) {
      nCaselles = colors.size();
      caselles = new ArrayList<>(nCaselles);
      encert_pos_col = 0;
      encert_pos = 0;
      
      for (int i=0; i<nCaselles; i++) {
          caselles.add(new Casella(colors.get(i)));
      }
  }
  
   /**
   * Crea una nueva fila copia del argumento
   * @param f fila a copiar
   */
  public Fila(Fila f) {
      nCaselles = f.nCaselles;
      caselles = new ArrayList<>(f.caselles);
      encert_pos_col = f.encert_pos_col;
      encert_pos = f.encert_pos;
  }
  
  /**
   * Compara dos filas. Devuelve el resultado en rojos (mismo color y posicion) y blancos (color correcto, posicion incorrecta)
   * @param solucio fila a la que comparar
   * @return Instancia de ResultatJugada que contiene los rojos y blancos obtenidos
   */
  public ResultatJugada compara(Fila solucio) {
      int vermells = 0;
      int blancs = 0;
      
      List<Casella> casellesSolucio = new ArrayList<>(solucio.caselles);
      List<Casella> casellesJugada = new ArrayList<>(this.caselles);
      
      Iterator<Casella> itSolucio = casellesSolucio.iterator();
      Iterator<Casella> itJugada = casellesJugada.iterator();
      
      while(itSolucio.hasNext()) {
          Casella casellaSolucio = itSolucio.next();
          Casella casellaJugada = itJugada.next();
          
          if (casellaSolucio.equals(casellaJugada)) {
              vermells++;
              itSolucio.remove();
              itJugada.remove();
          }
      }
      
      itJugada = casellesJugada.iterator();
      while (itJugada.hasNext()) {
          Casella casellaJugada = itJugada.next();
          itSolucio = casellesSolucio.iterator(); // reset iterador
          boolean blancTrobat = false;
          
          while (itSolucio.hasNext() && !blancTrobat) {
              Casella casellaSolucio = itSolucio.next();
              
              if (casellaSolucio.equals(casellaJugada)) {
                  blancs++;
                  itSolucio.remove();
                  blancTrobat = true;
              }
          }
      }
      
      return new ResultatJugada(vermells, blancs);
  }
  
  /**
   * Indica si la fila contiene el color c
   * @param c color que buscar en la fila
   * @return true si lo contiene, falso en caso contrario
   */
  public boolean conteColor(int c) {
      boolean resultat = false;
      
      for (int i=0; i<nCaselles && !resultat; i++) {
          if (caselles.get(i).getColor() == c) resultat = true;
      }
      
      return resultat;
  }
  
  /**
   * Indica si la fila tiene colores repetidos
   * @return true si contiene algun color que se repite, false en caso contrario
   */
  public boolean teRepetits() {
      Set<Casella> colorsTrobats = new HashSet<>();
      
      for (Casella c : caselles) {
          if (colorsTrobats.contains(c)) {
              return true;
          } else{
              colorsTrobats.add(c);
          }
      }
      
      return false;
  }
  
    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Fila)) return false;

        Fila altra = (Fila) other;
        return this.caselles.equals(altra.caselles);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.caselles);
        return hash;
    }
    
    /**
     * Genera una fila de nCaselles casillas y con colores entre 1 y nColors incluidos
     * @param nColors numero de colores distintos que generar
     * @param nCaselles numero de casillas que debe tener la fila generada
     * @return 
     */
    public static Fila filaRandom(int nColors, int nCaselles) {
        Fila resultat = new Fila(nCaselles);
	Random rn = new Random();
	
        for (int i = 0; i<nCaselles; i++) {
            int colorRandom = rn.nextInt(nColors) + 1;
            resultat.setColor(i, colorRandom);
        }
        
        return resultat;
   }
  
  public void set_encert_pos_col(int n){
      encert_pos_col = n;
  }
  public void set_encert_pos(int n){
      encert_pos = n;
  }
  public int get_encert_pos_col(){
      return encert_pos_col;
  }
  public int get_encert_pos(){
      return encert_pos;
  }

  public int getColumnes(){
	return this.nCaselles;
  }
  public void setColor(int index, int color) /*throws Exception ?? */ {
    caselles.get(index).setColor(color);
  }

  public int getColor(int index){
    return caselles.get(index).getColor();
  }

  public String retorna_fila() {
    String s = "[ ";

    for (Casella c : caselles) {
      s = s + Integer.toString(c.getColor()) + " ";
    }

    s = s + "]";

    return s;
  }

  public Fila getFila(){ // WTF??
	return this;
}

  /** Retorna una solució aleatoria
  */
  /*
  public void solucio_random(int nColors) {
	Random rn = new Random();
	int i = 0;
	while(i < nCaselles){
		int answer = rn.nextInt(nColors) + 1;
		boolean segueix = true;
		int j = 0;
		while(segueix && j < i){
			if(caselles.get(j).getColor() == answer){
				segueix = false;
				i--;
			}
			++j;
		}
		if(segueix) caselles.get(i).setColor(answer);
		++i;
	}

   }*/
    //Comprova si la fila_jugada es correcte i actualitza la fila comprobació.
   public boolean check(Fila fila_jugada, Fila fila_solucio) throws Exception{
	int enc_pos = 0;
        int enc_col = 0;
	for(int i = 0; i < nCaselles; ++i){
		if(fila_jugada.getColor(i) == fila_solucio.getColor(i)) ++enc_pos;
		else{
			boolean segueix = true; int j = 0;
			while(segueix && j < nCaselles){
				if(fila_jugada.getColor(i) == fila_solucio.getColor(j)){
					enc_col++;
					segueix = false;
				}
				++j;
			}
		}
	}
	for (int i = 0; i < enc_pos; ++i){
		caselles.get(i).setColor(2);
	}
	for(int j = enc_pos; j < (enc_col + enc_pos); ++j) {
		caselles.get(j).setColor(1);
	}
	return (enc_pos == nCaselles);
    }
   
   

    public boolean colorExists(int c, int i) throws Exception{
      for(int j = 0; j < i; ++j){
	if(c == caselles.get(j).getColor()) return true;
      }
      return false;
    }


    public void imprimeix(){
        for (int i = 0; i < nCaselles; i++){
            System.out.println(caselles.get(i).getColor());
        }
    }

    public int get_color_solucio(int posicio){
        int pos = posicio - 1;
        return caselles.get(pos).getColor();
    }
    
    public void set_solucio(List<Integer> codi_maker){
        for (int i = 0; i < nCaselles; i++){
            try {
                caselles.get(i).setColor(codi_maker.get(i));
            } catch (Exception ex) {
                Logger.getLogger(Fila.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("error al fer el set de color de la solució");
            }
        }
    }
    public int mida(){
        return caselles.size();
    }


}
