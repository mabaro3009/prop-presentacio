package domini.clases;

import java.util.Scanner;
import dades.*;
import java.util.List;

/** Controlador del dominio. Gestiona el flujo del programa.
* @author Carles Gonzalvo
*/

public class CtrlDomini {

    private static final int MP_CREARUSUARIO  = 1;
    private static final int MP_INICIARSESION = 2;
    private static final int MP_VERRANKING    = 3;
    private static final int MP_SALIR         = 4;

    private static final int MJ_NUEVAPARTIDA  = 1;
    private static final int MJ_CARGARPARTIDA = 2;
    private static final int MJ_RANKING       = 3;
    private static final int MJ_MEJORPP       = 4;
    private static final int MJ_BORRAR        = 5;
    private static final int MJ_CAMBIARPASS   = 6;
    private static final int MJ_SALIR         = 7;

    private Scanner sca;
    private Jugador j;
    private Ranking r;


    /**
    * Crea un nuevo CtrlDomini.
    */
    public CtrlDomini() {
      sca = new Scanner(System.in);
      try {
        r = GestorRanking.getInstancia().llegeixRanking();
      } catch(FitxerNoExisteixExcepcio e) {
        r = Ranking.getInstancia();
        nouRanking(r);
      } catch(FitxerInvalidExcepcio | IOExcepcio e) {
        e.printStackTrace();
        System.exit(1);
      }
    }

	public void inicialitzar(){
	}
	
    private void nouRanking(Ranking r) {
      try {
        GestorRanking.getInstancia().guardaRanking(r);
      } catch (IOExcepcio ioe) {
        ioe.printStackTrace();
        System.exit(1);
      }
    }

    /**
    * Inicia el flujo del programa.
    */
    public void inici() throws Exception {
        boolean salir = false;

        while (!salir) {
          System.out.println("Menú principal");
          System.out.println(MP_CREARUSUARIO + " - Crear Usuario");
          System.out.println(MP_INICIARSESION + " - Iniciar sesión");
          System.out.println(MP_VERRANKING   + " - Ver Ranking");
          System.out.println(MP_SALIR        + " - Salir del programa");

          switch(sca.nextInt()) {
            case MP_CREARUSUARIO:  boolean usuariCreat = crearUsuari();
                                   if (usuariCreat) menuJugador();
                                   break;
            case MP_INICIARSESION: boolean sessioIniciada = iniciaSessio();
                                   if (sessioIniciada) menuJugador();
                                   break;
            case MP_VERRANKING:    r.mostra_rank(0);
                                   break;
            case MP_SALIR:         salir = true;
          }
        }

        sortir();
    }

    private boolean crearUsuari() {
        

        System.out.println(" ");
        System.out.println("Nombre de usuario:");
        String nom = sca.next();
        System.out.println(" ");
        System.out.println("Contraseña:");
        String pass = sca.next();
        return crearNouUsuari(nom, pass);
    }
    
    public boolean crearNouUsuari(String nom, String pass){
        boolean usuariCreat = false;
        Jugador j = new Jugador(nom, pass);
        try {
          GestorJugador.getInstancia().guardaJugador(j);
          this.j = j;
          usuariCreat = true;
        } catch (IOExcepcio ioe) {
          ioe.printStackTrace();
          System.exit(1);
        } catch (FitxerJaExisteixExcepcio fjee) {
          System.out.println("Error: el usuario " + nom + " ya existe.");
        }

        return usuariCreat;
    }
    
    public List <String> obtenirRanking(){
        
        
        return r.getRankList();
    }

    private boolean iniciaSessio() throws Exception {
        String nom, pass;

        System.out.println("Nombre de usuario:");
        nom = sca.next();
        System.out.println("Contraseña:");
        pass = sca.next();
        return logIn(nom, pass);
    }
    
    public boolean logIn(String nom, String pass){
        boolean ret = false;
        try {
          j = GestorJugador.getInstancia().llegeixJugador(nom);
        } catch (FitxerNoExisteixExcepcio fnee) {
            System.out.println();
            System.out.println("Error: el usuario " + nom + " no existe.");
            System.out.println();
            return false;
        } catch (FitxerInvalidExcepcio | IOExcepcio e) {
          e.printStackTrace();
          System.exit(1);
        }

        if (!j.comprovaContrassenya(pass)) {
          System.out.println();
          System.out.println("Error: contraseña incorrecta. Al carrer.");
          System.out.println();
        } else {
          System.out.println("Bienvenid@, " + nom);
          this.j = j;
          ret = true;
        }

        return ret;
    }

    private void menuJugador() throws Exception {
        boolean salir = false;
        boolean borrar = false;

        while (!salir) {
          System.out.println(" ");
          System.out.println("Menú del jugador");
          System.out.println(MJ_NUEVAPARTIDA  + " - Nueva Partida");
          System.out.println(MJ_CARGARPARTIDA + " - Cargar Partida");
          System.out.println(MJ_RANKING       + " - Ranking");
          System.out.println(MJ_MEJORPP       + " - Mejor puntuación personal");
          System.out.println(MJ_BORRAR        + " - Borrar usuario");
          System.out.println(MJ_CAMBIARPASS   + " - Cambiar contraseña");
          System.out.println(MJ_SALIR         + " - Salir");

          switch (sca.nextInt()) {

            case MJ_NUEVAPARTIDA:  novaPartida();
                                   break;
            case MJ_CARGARPARTIDA: carregaPartida();
                                   break;
            case MJ_RANKING:       r.mostra_rank(0);
                                   break;
            case MJ_MEJORPP:       System.out.println("Mejor puntuación personal: " + j.getMaxPuntuacio());
                                   break;
            case MJ_BORRAR:        borrar = true;
                                   salir = true;
                                   break;
            case MJ_CAMBIARPASS:   canviarPass();
                                   break;
            case MJ_SALIR:         salir = true;
          }

        }

        if (!borrar){
            try {
                GestorJugador.getInstancia().actualitzaJugador(j);
            } catch (FitxerNoExisteixExcepcio | IOExcepcio e) {
                e.printStackTrace();
                System.exit(1);
            }
        } else {
            //GestorJugador.getInstancia().esborraJugador(j);
            borrar();
        }

    }
    
    public String getUser(){
        return j.getNom();
    }

    public int getMaxP(){
        return j.getMaxPuntuacio();
    }
    
    public void borrar() throws FitxerNoExisteixExcepcio, IOExcepcio{
        GestorJugador.getInstancia().esborraJugador(j);
    }
    
    private void carregaPartida() throws Exception{
        System.out.println("Selecciona la partida que quieres cargar:");
        j.imprimeix_ids();
        if (j.id_partides.size() > 0) {
          int partida_carr = sca.nextInt();
          if (partida_carr > j.id_partides.size() || partida_carr < 1) System.out.println("La partida no existe.");
          else{
            String id_carregar = j.id_partides.get(partida_carr-1);
            Partida p = GestorPartida.getInstancia().llegeixPartida(id_carregar);
            Partida pp = null;
            try {
                pp = j.carrega_partida(p);
            } catch(Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
            if (pp.solucio_trobada) { // partida acabada, guardar al ranking.
                if (pp.id.charAt(0) == 'B'){
                    Registre reg = new Registre(pp.id, pp.puntuacio, j.nom);
                    r.afegir(reg);
                }
                
                //esborrar partida
                try{
                    GestorPartida.getInstancia().esborraPartida(pp);
                } catch (FitxerNoExisteixExcepcio | IOExcepcio e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            } else if (pp.guardar){ // partida inacabada, guardar-la
                pp.guardar = false;
                j.id_partides.add(pp.id);
                try {
                GestorPartida.getInstancia().guardaPartida(pp);
                } catch (IOExcepcio ioe) {
                ioe.printStackTrace();
                System.exit(1);
                } catch (FitxerJaExisteixExcepcio fjee) {
                actualitzaNovaPartida(p);
                }
            }
          }
        }
    }

    private void novaPartida() {
      Partida p = null;
      try {
        p = j.crear_partida();
      } catch(Exception e) {
        e.printStackTrace();
        System.exit(1);
      }
      if (p.solucio_trobada) { // partida acabada, guardar al ranking.
        if (p.getID().charAt(0) == 'B'){
            Registre reg = new Registre(p.getID(), p.getPuntuacio(), j.getNom());
            r.afegir(reg);
        }
      } else if (p.guardar){ // partida inacabada, guardar-la
        p.guardar = false;
        j.id_partides.add(p.id);
        try {
          GestorPartida.getInstancia().guardaPartida(p);
        } catch (IOExcepcio ioe) {
          ioe.printStackTrace();
          System.exit(1);
        } catch (FitxerJaExisteixExcepcio fjee) {
          actualitzaNovaPartida(p);
        }
      }
    }

    private void actualitzaNovaPartida(Partida p) {
      try {
        GestorPartida.getInstancia().actualitzaPartida(p);
      } catch (IOExcepcio | FitxerNoExisteixExcepcio e) {
        e.printStackTrace();
        System.exit(1);
      }
    }

    private void sortir() {
      try {
        GestorRanking.getInstancia().guardaRanking(r);
      } catch (IOExcepcio e) {
        e.printStackTrace();
        System.exit(1);
      }
      System.exit(0);
    }

    private void canviarPass() {
      System.out.println("Introduce tu contraseña actual.");
      String currentPass = sca.next();
      boolean b = j.comprovaContrassenya(currentPass);
      if (!b) {
        System.out.println("Contraseña incorrecta.");
        return;
      }
      System.out.println("Introduce tu nueva contraseña.");
      String newPass = sca.next();
      System.out.println("Vuelve a escribir la nueva contraseña.");
      String v = sca.next();
      if (!newPass.equals(v)) {
        System.out.println("Las contraseñas no coinciden.");
        return;
      }
      canviPass(newPass);
    }
    public void canviPass(String newPass) {
      j.canviaContrassenya(newPass);
      System.out.println("Contraseña cambiada satisfactoriamente jajaj.");
    }

}
