package domini.clases;

import java.lang.*;
import java.util.*;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Clase principal del programa. El tablerto tiene las filas, columnas y colores de la partida.
* 3 tipos de fila asociadas: filas de las jugadas, filas de comprobación y fila solución
* @author Marc Badia, Carles Gonzalvo and Daniel Tarrés
*/

public class Tauler implements Serializable {

	private static final long serialVersionUID = 42L;
	private int nFiles;
	private int nColumnes;
	private int nColors;
	private List<Fila> files;
	private List<Fila> files_comp;
	private Fila fila_solucio;
	
	private List<String> combPos = new ArrayList<String>(); //Totes les combinacions posibles
	private List<String> combinacions = new ArrayList<String>();
	private String comb = "";
	private String codi;
	private int blackOG = 0;
	private int redOG = 0;
	public boolean sol_random_creada;

	public Tauler(int nFiles, int nColumnes, int nColors) throws Exception {
		this.nFiles = nFiles;
		this.nColumnes = nColumnes;
		this.nColors = nColors;
		files = new ArrayList<Fila>(nFiles);
		files_comp = new ArrayList<Fila>(nFiles);
        sol_random_creada = false;
		for(int i=0; i < nFiles; ++i){
			files.add(new Fila(nColumnes));
		}

		for(int i=0; i < nFiles; ++i){
			files_comp.add(new Fila(nColumnes));
		}
		this.fila_solucio = new Fila(nColumnes);
		setCombPos();
		//llegeixComb();
	}
	
	public int getFiles(){
		return this.nFiles;
	}

	public int getColumnes(){
		return this.nColumnes;
	}

	public int getColors(){
		return this.nColors;
	}
        
        public String getCodi(){
            return codi;
        }
	//Mostra el tauler per pantalla
	public void imprimeix_tauler() {
		for(int i = nFiles-1; i >= 0; --i){
			String fila = files.get(i).retorna_fila();
			String fila_c = files_comp.get(i).retorna_fila();
			System.out.println(fila + fila_c);
		}
		System.out.println(" ");
	}

	public void instBreaker(){
		System.out.println("Para poner un color escribir 'Play' seguido de el número de columna y el número del color (por ejemplo: '4 3')");
		System.out.println("Otros comandos: 'Guardar', 'Check', 'Salir', 'Pista'");
	}

	public void instMaker(){
		System.out.println("Para poner una casilla escribid 'Play' seguido de 2 si en hay un color en la posición correcta o 1 si hay un color correcto");
		System.out.println("Otros comandos: 'Guardar', 'Check', 'Salir'");
	}

	public int getColor(int fila, int columna, int mode) throws Exception{
		if(mode == 0) return fila_solucio.getColor(columna);
		else if(mode == 1) return files.get(fila).getColor(columna);
		else if(mode == 2) return files_comp.get(fila).getColor(columna);
		else return 0;
	}

	public void setColor(int fila, int columna, int color, int mode) throws Exception{
		if(mode == 0) fila_solucio.setColor(columna, color);
		else if(mode == 1) files.get(fila).setColor(columna, color);
		else if(mode == 2) files_comp.get(fila).setColor(columna, color);
	}
    
    /** Retorna una solució aleatoria
    */
	public void solucio_random() throws Exception{
                fila_solucio = Fila.filaRandom(nColors, nFiles);
		//fila_solucio.solucio_random(nColors);
		sol_random_creada = true;
		//String fila_s = fila_solucio.retorna_fila();
		//System.out.println(fila_s);
	}
	
	public boolean check(int fila_act) throws Exception{
		Fila fila_jugada = files.get(fila_act);
		return files_comp.get(fila_act).check(fila_jugada, fila_solucio);
	}

    /** Retorna una pista
    * @param c tipus de pista
    */
	public int getPista(int c){
		return fila_solucio.getColor(c);
	}

	public boolean colorExists(int c, int i) throws Exception{
		return fila_solucio.colorExists(c, i);
	}

	public void imprimeix_solucio() throws Exception{
		String fila_s = fila_solucio.retorna_fila();
		System.out.println(fila_s);
	}
        
        public void setFilaComp(int fila_actual, String comp, int avis_vermell, int avis_blanc){
            redOG = avis_vermell;
            blackOG = avis_blanc;
            try {
                omplirFila(files_comp.get(fila_actual), comp);
            } catch (Exception ex) {
                Logger.getLogger(Tauler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void set_blanc(int avis_blanc){
            blackOG = avis_blanc;
        }
        public void set_vermell(int avis_vermell){
             redOG = avis_vermell;
        }
	
	/** Algorisme del Five-Guess per el CodeMaker
	* @param fila_actual fila en la que es troba la partida en aquest moment
	*/
	public void algorisme (int fila_actual) throws Exception{
          if(fila_actual > 0) System.out.println(files.get(fila_actual-1).retorna_fila() + files_comp.get(fila_actual-1).retorna_fila() + combPos.size());
	  if(fila_actual ==  0 || combPos.size() == 1){ //Primera jugada o nomes hi ha una solució possible
	    codi = combPos.get(0);
	  }
	  else { //Resta de jugades
              
	    esborrar(fila_actual);
	   // llegeixComb();
	    //minimax();
	    
	    codi = combPos.get(0);
	  }
	  Fila f = files.get(fila_actual);
	  omplirFila(f, codi);
	}
	
	/** Crea un set amb totes les possibles solucions d'aquest tauler
	*/
	public void setCombPos() throws Exception{
	  //System.out.println(comb);
	  if(comb.length() == nColumnes) {
	    if(noRepe(comb)){
	      combPos.add(comb);
	      combinacions.add(comb);
	     }
	  }
	  else {
	    for(int i = 1; i <= nColors; ++i){
	      String oldComb = comb;
	      comb = comb + i;
	      setCombPos();
	      comb = oldComb;
	    }
	  }
	}
	
        public void imprimeix_solucio_vista(){
            fila_solucio.imprimeix();
        }
	
	public void llegeixComb() throws Exception{
	  for(int i = 0; i < combPos.size(); ++i){
	    System.out.println(combPos.get(i));
	  }
	}
	    
	public boolean noRepe(String c) throws Exception{
	  for(int i = 0; i < c.length(); ++i){
	    for(int j = i+1; j < c.length(); ++j){
	      if(c.charAt(i) == c.charAt(j)) return false; //es repeteix
	    }
	  }
	  return true;
	}
	
	public void omplirFila(Fila f, String codi)throws Exception{
	  for(int i = 0; i < codi.length(); ++i) f.setColor(i, Character.getNumericValue(codi.charAt(i)));
	}
	
	//Esborra les respostes que ja no son possibles
	public void esborrar(int fila_actual) throws Exception{
	  combinacions.remove(codi);
	  combPos.remove(codi);
	  for(int j = 0; j < combPos.size(); ++j){
	    String solucio = combPos.get(j);
	    if(comprovaReds(solucio, redOG) || comprovaBlacks(solucio, blackOG+redOG)) { combPos.remove(solucio); --j; }
	  }
	}
	
	public boolean comprovaReds(String solucio, int redOG){
	  int red = 0;
	  for(int i = 0; i < nColumnes; ++i){
	    if(codi.charAt(i) == solucio.charAt(i)) ++red;
	  }
	  return (redOG != red);
	}
	
	public boolean comprovaBlacks(String solucio, int blackOG){
	  int black = 0;
	  for(int i = 0; i < nColumnes; ++i){
	    for(int j = 0; j < nColumnes; ++j){
	       if(codi.charAt(i) == solucio.charAt(j)) ++black;
	     }
	  }
	  return (blackOG != black);
	}
	
	public boolean comprova(int fila_actual)throws Exception{
	  iniRedBlack(fila_actual);
	  Fila f = new Fila(nColumnes);
	  boolean b = f.check(files.get(fila_actual), fila_solucio);
	 // System.out.println("COMPROVACIO CORRECTA: " + f.retorna_fila());
	  int red, black;
	  red = black = 0;
	  for(int i = 0; i < nColumnes; ++i){
	    if(f.getColor(i) == 1) ++black;
	    else if(f.getColor(i) == 2) ++red;
	  }
	  //System.out.println("redog: " + redOG +  " red: " + red + " blackog: " + blackOG  + " black: " + black);
	  return (black == blackOG && red == redOG);
	}
        
        public boolean comprova2(int fila_actual)throws Exception{
	  Fila f = new Fila(nColumnes);
	  boolean b = f.check(files.get(fila_actual), fila_solucio);
	  System.out.println("COMPROVACIO CORRECTA: " + f.retorna_fila());
	  int red, black;
	  red = black = 0;
	  for(int i = 0; i < nColumnes; ++i){
	    if(f.getColor(i) == 1) ++black;
	    else if(f.getColor(i) == 2) ++red;
	  }
	  //System.out.println("redog: " + redOG +  " red: " + red + " blackog: " + blackOG  + " black: " + black);
	  return (black == blackOG && red == redOG);
	}
	
	public void iniRedBlack(int fila_actual){
	  redOG = 0;
	  blackOG = 0;
	  for(int i = 0; i < nColumnes; ++i){
	    if(files_comp.get(fila_actual).getColor(i) == 1) ++blackOG;
	    else if(files_comp.get(fila_actual).getColor(i) == 2) ++redOG;
	  }
	}
	
	//Retorna true si la jugada es correcte
	public boolean solucio_correcte(int fila_actual){
	  for(int i = 0; i < nColumnes; ++i){
	    if(files_comp.get(fila_actual).getColor(i) != 2) return false;
	  }
	  return true;
	}

	/** part del minimax de l'algorisme
	*/
	public void minimax(){
	  int maxmin = 0;
	  ArrayList<String> millorsJugades = new ArrayList<String>();
	  for(int i = 0; i < combinacions.size(); ++i){
	    String codiNoUtilitzat = combinacions.get(i);
	    for(int j = 0; j < combPos.size(); ++j){
	      String codiPossible = combPos.get(j);
	      int puntuacio = getPuntuacio(codiNoUtilitzat, codiPossible);
	      if(puntuacio > maxmin){
		maxmin = puntuacio;
		millorsJugades.clear();
		millorsJugades.add(codiNoUtilitzat);
	      }
	      else if(puntuacio == maxmin){
		millorsJugades.add(codiNoUtilitzat);
	      }
	    }

	  }
	  boolean b = true;
	  for(int k = 0; k < millorsJugades.size() && b; ++k){
	    String aux = millorsJugades.get(k);
	    //System.out.println(k + " = " + aux);
	    if(combPos.contains(aux)){
	      codi = aux;
	      b = false;
	     }
	  }
	  if(b){
	    //Random rn = new Random();
	    codi = combPos.get(0);
	  }
	}
	
	public int getPuntuacio(String codiNoUtilitzat, String codiPossible){
	  int p = 0;
	  int red = getReds(codiNoUtilitzat, codiPossible);
	  int black = getBlacks(codiNoUtilitzat, codiPossible);
	  int red2, black2;
	  for(int i = 0; i < combPos.size(); ++i){
	    String guess = combPos.get(i);
	    red2 = getReds(guess, codiPossible);
	    black2 = getBlacks(guess, codiPossible);
	    if(red != red2 || black != black2) ++p;
	  }
	  return p;
	}
	
	public int getReds(String codiNoUtilitzat, String codiPossible){
	  int red = 0;
	  for(int i = 0; i < nColumnes; ++i){
	    if(codiNoUtilitzat.charAt(i) == codiPossible.charAt(i)) ++red;
	  }
	  return red;
	}
	
	public int getBlacks(String codiNoUtilitzat, String codiPossible){
	  int black = 0;
	  for(int i = 0; i < nColumnes; ++i){
	    for(int j = 0; j < nColumnes; ++j){
	       if(codiNoUtilitzat.charAt(i) == codiPossible.charAt(j)) ++black;
	     }
	  }
	  return black;
	}
        
        
        public List<Integer> check_vista(List<Integer> fila_jugada) throws Exception{
            int enc_pos = 0;
            int enc_col = 0;
            for(int i = 0; i < nColumnes; ++i){
                    if(fila_jugada.get(i) == fila_solucio.getColor(i)) ++enc_pos;
                    else{
                            boolean segueix = true; int j = 0;
                            while(segueix && j < nColumnes){
                                    if(fila_jugada.get(i) == fila_solucio.getColor(j)){
                                            enc_col++;
                                            segueix = false;
                                    }
                                    ++j;
                            }
                    }
            }
            //return (enc_pos == nColumnes);
            List<Integer> resultat = new ArrayList<Integer>(2);//(enc_pos, enc_col);
            resultat.add(0, enc_pos);
            resultat.add(1, enc_col);
            return resultat;
        }
        
        
        public int get_color_solucio(int posicio){
            return fila_solucio.get_color_solucio(posicio);
        }
        public int get_color_posicio(int fila, int posicio){
            return files.get(fila).getColor(posicio);
        }
        
        public void set_encert_pos_col(int nfila, int n){
            files.get(nfila-1).set_encert_pos_col(n);
        }
        public void set_encert_pos(int nfila, int n){
            files.get(nfila-1).set_encert_pos(n);
        }
        public int get_encert_pos_col(int nfila){
            return files.get(nfila-1).get_encert_pos_col();
        }
        public int get_encert_pos(int nfila){
            return files.get(nfila-1).get_encert_pos();
        }
        public void set_solucio(List<Integer> codi_maker){
            fila_solucio.set_solucio(codi_maker);
        }        
	  
    }