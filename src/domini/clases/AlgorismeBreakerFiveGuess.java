package domini.clases;

import java.util.*;

/**
 * Clase que implementa el algorito Five Guess para la modalidad CodeBreaker de Mastermind
 * @author Carles Gonzalvo
 */
public class AlgorismeBreakerFiveGuess implements AlgorismeBreaker {
    
    private final Set<Fila> combinacionsTotals;
    private final Set<Fila> combinacionsNoDescartades;
    private final List<ResultatJugada> resultatsPossibles;
    private final int nColors;
    private final int nCaselles;
    private final boolean permetRepetits;
    private Fila ultimaJugada;
    
    /**
    * Crea una nueva instancia de AlgorismeBreakerFiveGuess
    * @param nColors numero de colores con los que jugar. Debe ser mayor o igual a 1
    * @param nCaselles numero de columnas que forma cada fila. Debe ser mayor o igual a 1
    * @param permetRepetits determina si se quiere que el algoritmo funcione para colores repetidos o no
    */
    public AlgorismeBreakerFiveGuess(int nCaselles, int nColors, boolean permetRepetits) {
        this.nColors   = nColors;
        this.nCaselles = nCaselles;
        this.permetRepetits = permetRepetits;
        combinacionsTotals = new HashSet<>();
        resultatsPossibles = new ArrayList<>();
        if (permetRepetits) {
            generaCombinacionsTotalsRepetits();
        } else{
            generaCombinacionsTotalsNoRepetits();
        }
        combinacionsNoDescartades = new HashSet<>(combinacionsTotals);
        
        generaResultatsPossibles();
        //System.out.println(resultatsPossibles);
    }
    
    /**
    * Devuelve la siguiente jugada segun el algoritmo
    * @param resultatUltimaJugada resultado obtenido al probar la ultima jugada indicada por el algoritmo. En caso de ser la primera jugada, no importa
    * @return fila que contiene los colores a jugar.
    */
    @Override
    public Fila seguentJugada(ResultatJugada resultatUltimaJugada) {
        if (ultimaJugada == null) {
            // primera jugada
            if (permetRepetits) {
                ultimaJugada = new Fila(combinacioBase());
            } else {
                ultimaJugada = new Fila(combinacioBaseNoRep());
            }
            
            return new Fila(ultimaJugada);
        }
        if (resultatUltimaJugada.getVermells() == nCaselles) return ultimaJugada;
      
        descartaCombinacions(resultatUltimaJugada);
        //ultimaJugada = combinacionsNoDescartades.iterator().next();
        ultimaJugada = minimax();
        return new Fila(ultimaJugada);
    }
    
    /**
     * Indica si el algoritmo instanciado permite codigos repetidos o no
     * @return true si los permite, false en caso contrario
     */
    public boolean permetRepetits() {
        return permetRepetits;
    }
    
    public static void main(String argv[]) {
        AlgorismeBreaker alg = new AlgorismeBreakerFiveGuess(4, 6, false);        
        Fila solucio = new Fila(Arrays.asList(1,3,6,4));

        int njugades = 0;
        boolean trobat = false;
        Fila jugadaActual;
        ResultatJugada rs = null;
        
        while (njugades < 100 && !trobat) {
            jugadaActual = alg.seguentJugada(rs);
            rs=jugadaActual.compara(solucio);
            System.out.println("Jugada " + njugades + ": " + jugadaActual.retorna_fila());
            System.out.println(rs);
            if (rs.getVermells() == solucio.getColumnes()) {
                trobat = true;
            } else {
                njugades++;
            }
            
        }
    }
    
    private void generaCombinacionsTotalsRepetits() {
        List<Integer> colorsFila = combinacioBase();
        combinacionsTotals.add(new Fila(colorsFila));
        
        while (!colorsFila.equals(combMax())) {
            colorsFila = combSeguent(colorsFila);
            combinacionsTotals.add(new Fila(colorsFila));
        }
    }
    
    private void generaCombinacionsTotalsNoRepetits() {
        List<Integer> colorsFila = combinacioBase();
        combinacionsTotals.add(new Fila(colorsFila));
        
        while (!colorsFila.equals(combMax())) {
            colorsFila = combSeguent(colorsFila);
            Fila f = new Fila(colorsFila);
            if (!f.teRepetits()) combinacionsTotals.add(f);
        }
    }
    
    private List<Integer> combMax() {
        List<Integer> combMax = new ArrayList<>(nCaselles);
        for(int i=0; i<nCaselles; i++) {
            combMax.add(nColors);
        }
        
        return combMax;
    }
    
    private List<Integer> combinacioBase() {
        List<Integer> combBase = new ArrayList<>(nCaselles);
        for (int i=0; i<nCaselles; i++) {
            combBase.add(i, 1);
        }
        return combBase;
    }
    
    private List<Integer> combinacioBaseNoRep() {
        List<Integer> combBase = new ArrayList<>(nCaselles);
        for (int i=0; i<nCaselles; i++) {
            combBase.add(i, i+1);
        }
        return combBase;
    }
    
    private List<Integer> combSeguent(List<Integer> colorsFila) {
        List<Integer> resultat = new ArrayList<>(colorsFila);
        for (int i = nCaselles-1; i>=0; i--) {
            int colorSeleccionat = resultat.get(i);
            if (colorSeleccionat < nColors) {
                resultat.set(i, colorSeleccionat+1);
                for (int j = i+1; j<resultat.size(); j++) {
                    resultat.set(j, 1);
                }
                return resultat;
            }
        }
        return resultat; //colorsFila maxim retorna colorsFila maxim
    }
    
    /*
    * Descarta de combinacionsNoDescartades les combinacions que, si fossin solucio,
    * donarien un resultat diferent a resultatJugada en cas de jugar ultimaJugada.
    */
    private void descartaCombinacions(ResultatJugada resultatJugada) {
        int total = 0;
        Iterator<Fila> iterador = combinacionsNoDescartades.iterator();
        while(iterador.hasNext()) {
            Fila filaActual = iterador.next();
            ResultatJugada resultatFilaActual = ultimaJugada.compara(filaActual);
            
            if (!resultatFilaActual.equals(resultatJugada)) {
                iterador.remove();
                total++;
            }
        }
    }
    
    private Fila minimax() {
        
        int min = Integer.MAX_VALUE;
        Fila minCombinacio = null;
        
        for (Fila solucioPossible : combinacionsNoDescartades) {
            int max = 0;
            
            for (ResultatJugada r : resultatsPossibles) {
                int count = 0;
                
                for (Fila f : combinacionsTotals) {
                    if (f.compara(solucioPossible).equals(r)) count++;
                }
                if (count > max) max = count;
            }
            if (max < min) {
                min = max;
                minCombinacio = solucioPossible;
            }
        }
        
        return minCombinacio;
    }
    
    private void generaResultatsPossibles() {
        
        for (int vermells = 0; vermells<=nCaselles; vermells++) {
            for (int blancs = 0; blancs <=nCaselles-vermells; blancs++) {
                ResultatJugada rs = new ResultatJugada(vermells, blancs);
                resultatsPossibles.add(rs);
            }
        }
    }
}
