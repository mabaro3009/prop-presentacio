package domini.controladors;

import domini.clases.Registre;
import java.lang.*;
import java.util.Scanner;

/** Test de Registre.java
* @author Daniel Tarrés
*/
public class RegistreDriver {
    static Registre reg;
    Scanner sca = new Scanner(System.in);

    public  void testConstructor(){
        System.out.println("testConstructor");
        System.out.println("Introduce el id de la partida");
        String id = sca.next();
        System.out.println("Introduce la puntuación de la partida");
        int puntuacio = sca.nextInt();
        System.out.println("Introduce el usuario de la partida");
        String user = sca.next();
        this.reg = new Registre(id, puntuacio, user);
        System.out.println("Registro creado");
    }

    public void testGetid(){
        System.out.println("testGetid");
        String id = reg.getid();
        System.out.println("El id es: " + id);
    }

    public void testGetpuntuacio(){
        System.out.println("testGetpuntuacio");
        int punts = reg.getpuntuacio();
        System.out.println("La puntuación es: " + punts);
    }

    public void testGetuser(){
        System.out.println("testGetuser");
        String user = reg.getuser();
        System.out.println("El user es: " + user);
    }
    
    public void testimprimeix(){
        System.out.println("testimprimeix");
        reg.imprimeix();
    }
    
    public void menuDriver(){
        System.out.println("-------------------------");
        System.out.println("Selecciona el número de la función que quieres probar:");
        System.out.println("0- Salir del Driver");
        System.out.println("1- testGetid");
        System.out.println("2- testGetpuntuacio");
        System.out.println("3- testGetuser");
        System.out.println("4- testimprimeix");
        System.out.println("-------------------------");
    }
    
    public static void main(String[] args) throws Exception{
        System.out.println("Inicio test Registre.java");
        RegistreDriver regd = new RegistreDriver();
        regd.testConstructor();
        Scanner sca2 = new Scanner(System.in);
        regd.menuDriver();
        int n = sca2.nextInt();
        while(n != 0){
            switch(n){
                case 1:
                    regd.testGetid();
                    break;
                case 2:
                    regd.testGetpuntuacio();
                    break;
                case 3:
                    regd.testGetuser();
                    break;
                case 4:
                    regd.testimprimeix();
                    break;
                
            }
            if (n != 0){
                regd.menuDriver();
                n = sca2.nextInt();
            }
        }
        System.out.println("Fin del test");
    }

}
