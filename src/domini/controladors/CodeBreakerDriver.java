package domini.controladors;

import domini.clases.*;
import java.lang.*;
import java.util.Scanner;

/** Test de CodeBreaker.java
* @author Marc Badia
*/

public class CodeBreakerDriver {
  static CodeBreaker cb;
  Scanner sca = new Scanner(System.in);
  public  void testConstructor() throws Exception{
    System.out.println("testConstructor");
    System.out.println("Introduce el id de la partida");
    String id = sca.nextLine();
    System.out.println("Introduce el user de la partida");
    String user = sca.nextLine();
    cb = new CodeBreaker(id, user);
  }

 public boolean creaTauler() throws Exception{
		boolean b = true;
    		System.out.println(" ");
		System.out.println("Número de Filas:");
        	int f = sca.nextInt();
		if(f<0){b=false; System.out.println("Dato inválido");}
        	System.out.println("Número de Columnas:");
        	int c = sca.nextInt();
		if(c<0){b=false; System.out.println("Dato inválido");}
        	System.out.println("Número de Colores (Colores >= Columnas):");
        	int colors = sca.nextInt();
		if(colors<c){b=false; System.out.println("Dato inválido");}
        	if(b) cb.crear_tauler(f, c, colors);
                return b;
  }
  
  public  void testgetters() throws Exception{
    System.out.println("testgetters");
    System.out.println("id: " + cb.getID() + ", user: " + cb.getuser() + ", puntuacio: " + cb.getPuntuacio());
    System.out.println("filas: " + cb.getnFiles() + ", columnas: " + cb.getnColumnes() + ", colores: " + cb.getnColors());
  }
  
  public  void testjugar() throws Exception{
    System.out.println("testjugar");
    cb.jugar();
  }
  
  public  void testcalcula_puntuacio() throws Exception{
    cb.calcula_puntuacio(cb.getfilaActual());
  }
  
  public void menu() {
    System.out.println("----------------------------------------");
    System.out.println("Menú");
    System.out.println("1 - getters");
    System.out.println("2 - jugar");
    System.out.println("3 - calcula_puntuacio");
    System.out.println("0 - Salir");
  }
  
   public static void main (String[] args) throws Exception{
    System.out.println("Inicio test CodeBreaker.java");
    CodeBreakerDriver tcb = new CodeBreakerDriver();
    tcb.testConstructor();
    boolean b = false;
    while(!b) {
	b = tcb.creaTauler();
    }
    tcb.menu();
    Scanner sc = new Scanner(System.in);
    int op = sc.nextInt();
    while(op != 0) {
      switch(op) {
	case 1:
	   tcb.testgetters();
	   break;
	case 2:
	   tcb.testjugar();
	   break;
	case 3:
	   tcb.testcalcula_puntuacio();
	   break;
      }
      tcb.menu();
      op = sc.nextInt();
    }
  }
}
