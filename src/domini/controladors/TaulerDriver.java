package domini.controladors;

import domini.clases.Tauler;
import java.lang.*;
import java.util.Scanner;

/** Test de Tauler.java
* @author Marc Badia
*/

public class TaulerDriver {
  static Tauler t;
  Scanner sca = new Scanner(System.in);
  public  void testConstructor() throws Exception{
    System.out.println("testConstructor");
    boolean b = true;
    System.out.println("Introduce el número de Filas");
    int nFiles = sca.nextInt();
    if(nFiles <= 0){ b = false; System.out.println("nFilas ha de ser > 0"); }
    System.out.println("Introduce el número de Columnas");
    int nColumnas = sca.nextInt();
    if(nColumnas <= 0){ b = false; System.out.println("nColumnas ha de ser > 0"); }
    System.out.println("Introduce el número de Colores");
    int nColors = sca.nextInt();
    if(nColors < nColumnas){ b = false; System.out.println("nColores ha de ser >= nColumnas"); }
    
    if(b) t = new Tauler(nFiles, nColumnas, nColors);
    else testConstructor();

  }
  
  public void testgetters(){
    System.out.println("testgetters");
    System.out.println("nFilas: " + t.getFiles() + ", nColumnas: " + t.getColumnes() + ", nColores: " + t.getColors());
  }
  
  public void testimprimeix_tauler(){
    System.out.println("testimprimeix_tauler");
    t.imprimeix_tauler();
  }
  
  public void testsetColor()throws Exception{
    System.out.println("testsetColor");
    boolean b = true;
    System.out.println("Introduce la fila a modificar");
    int fila = sca.nextInt();
    if(fila < 1 || fila > t.getFiles()){ b = false; System.out.println("Fila inválida"); }
    System.out.println("Introduce la columna a modificar");
    int columna = sca.nextInt();
    if(columna < 1 || columna > t.getColumnes()){ b = false; System.out.println("Columna inválida"); }
    System.out.println("Introduce el color");
    int color = sca.nextInt();
    if(color < 0 || color > t.getColors()){ b = false; System.out.println("Color inválido"); }
    System.out.println("Introduce el modo (0 - solució, 1 - jugada, 2 - comprobación");
    int modo = sca.nextInt();
    if(modo < 0 || modo > 2){ b = false; System.out.println("Modo inválido"); }
    if(b) t.setColor(fila-1, columna-1, color, modo);
  }
  
  public void testgetColor() throws Exception{
    System.out.println("testgetColor");
    boolean b = true;
    System.out.println("Introduce la fila");
    int fila = sca.nextInt();
    if(fila < 1 || fila > t.getFiles()){ b = false; System.out.println("Fila inválida"); }
    System.out.println("Introduce la columna");
    int columna = sca.nextInt();
    if(columna < 1 || columna > t.getColumnes()){ b = false; System.out.println("Columna inválida"); }
    System.out.println("Introduce el modo (0 - solució, 1 - jugada, 2 - comprobación");
    int modo = sca.nextInt();
    if(modo < 0 || modo > 2){ b = false; System.out.println("Modo inválido"); }
    if(b) System.out.println("El color es: " + t.getColor(fila-1, columna-1, modo));
  }
  
  public void testsolucio_random() throws Exception{
    System.out.println("testsolucio_random");
    t.solucio_random();
  }
  
  public void testcolorExists() throws Exception{
    System.out.println("testsolucio_random");
    System.out.println("Introduce el color a buscar en la solución");
    int a = sca.nextInt();
    if(a < 0) System.out.println("Color inválido");
    else System.out.println(t.colorExists(a, t.getColumnes()));
  }
  
  public void testcheck() throws Exception{
    System.out.println("testCheck");
    System.out.println("Introduce la fila para el check: ");
    int a = sca.nextInt();
    boolean b = t.check(a-1);
  }
  
   public void testimprimeix_solucio() throws Exception{
    System.out.println("testimprimeix_solucio");
    t.imprimeix_solucio();
  }
  
  public void testllegeixComb() throws Exception{
    System.out.println("testimprimeix_comb");
    t.llegeixComb();
  }
  
  
  public void menu() {
    System.out.println("----------------------------------------");
    System.out.println("Menú");
    System.out.println("1 - getters");
    System.out.println("2 - imprimeix_tauler");
    System.out.println("3 - setColor");
    System.out.println("4 - getColor");
    System.out.println("5 - solucio_random");
    System.out.println("6 - check");
    System.out.println("7 - colorExists");
    System.out.println("8 - imprimeix_solucio");
    System.out.println("9 - imprimeix_comb");
    System.out.println("0 - Salir");
  }
  
   public static void main (String[] args) throws Exception{
    System.out.println("Inicio test Tauler.java");
    TaulerDriver td = new TaulerDriver();
    td.testConstructor();
    td.menu();
    Scanner sc = new Scanner(System.in);
    int op = sc.nextInt();
    while(op != 0) {
      switch(op) {
	case 1:
	   td.testgetters();
	   break;
	case 2:
	   td.testimprimeix_tauler();
	   break;
	case 3:
	   td.testsetColor();
	   break;
	case 4:
	   td.testgetColor();
	   break;
	case 5:
	   td.testsolucio_random();
	   break;
	case 6:
	   td.testcheck();
	   break;
	case 7:
	   td.testcolorExists();
	   break;
	case 8:
	   td.testimprimeix_solucio();
	   break;
	case 9:
	   td.testllegeixComb();
	   break;
      }
      td.menu();
      op = sc.nextInt();
    }
  }
}
