package domini.controladors;

import domini.clases.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class CodeMakerTest {
  	CodeMaker cm;
  	@Test
	public void testgetID()throws Exception{
		this.cm = new CodeMaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals("id_partida", cm.getID());
	}

	@Test
	public void testgetuser()throws Exception{
		this.cm = new CodeMaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals("id_jugador", cm.getuser());
	}
	
	@Test
	public void testgetPuntuacio()throws Exception{
		this.cm = new CodeMaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(0, cm.getPuntuacio());
	}

	@Test
	public void testgetnFiles()throws Exception{
		this.cm = new CodeMaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(10, cm.getnFiles());
	}	

	@Test
	public void testgetnColumnes()throws Exception{
		this.cm = new CodeMaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(5, cm.getnColumnes());
	}

	@Test
	public void testgetnColors()throws Exception{
		this.cm = new CodeMaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(6, cm.getnColors());
	}

	@Test
	public void testColorOk()throws Exception{
		this.cm = new CodeMaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(false, cm.colorOk(0, 5));
	}
}
