package domini.controladors;

import domini.clases.Casella;
import java.lang.*;
import java.util.Scanner;

/** Test de Casella.java
* @author Marc Badia
*/
public class CasellaDriver {
  static Casella c;
  public  void testConstructor(){
    System.out.println("testConstructor");
    this.c = new Casella();
    System.out.println("Casilla creada");
  }
  public  void testGetColor(){
    System.out.println("testgetColor");
    int color = c.getColor();
    System.out.println("La casilla es del color: " + color);
  }
  public void testSetColor(int color2) throws Exception{
    try {
      System.out.println("testSetColor");
      if(color2 < 0) System.out.println("Color ha de ser mayor o igual que 0");
      else {
	c.setColor(color2);
	System.out.println("El nuevo color es: " + c.getColor());
      }
    }catch(Exception e){
      throw e;
    }
  }
  public static void main(String[] args) throws Exception{
    System.out.println("Inicio test Casella.java");
    CasellaDriver cd = new CasellaDriver();
    cd.run();
    
  }
  public void run() throws Exception{
    Scanner sca = new Scanner(System.in);
    testConstructor();
    testGetColor();
    System.out.println("Introduce un color");
    int color2 = sca.nextInt();
    testSetColor(color2);
    System.out.println("Fin del test");
  }
}
  
  
