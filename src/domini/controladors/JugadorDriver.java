package domini.controladors;

import domini.clases.Jugador;
import domini.clases.Partida;
import java.lang.*;
import java.util.Scanner;

/** Test de Jugador.java
* @author Daniel Tarrés
*/
public class JugadorDriver {
    static Jugador jug;
    static Partida p;
    Scanner sca = new Scanner(System.in);

    public  void testConstructor(){
        System.out.println("testConstructor");
        System.out.println("Introduce el nombre de usuario del jugador");
        String nom = sca.next();
        System.out.println("Introduce la contraseña del jugador");
        String pass = sca.next();
        this.jug = new Jugador(nom, pass);
        System.out.println("Jugador creado");
    }

    public void testGetNom(){
        System.out.println("testGetNom");
        String nom = jug.getNom();
        System.out.println("El nombre del jugador es: " + nom);
    }
    
    public void testGetPass(){
        System.out.println("testGetPass");
        String pass = jug.getPass();
        System.out.println("La contraseña del jugador es: " + pass);
    }
    
    public void testgetMaxPuntuacio(){
        System.out.println("testgetMaxPuntuacio");
        int max = jug.getMaxPuntuacio();
        System.out.println("La máxima puntuación del jugador es " + max);
    }
    
    public void testcanviaContrassenya(){
        System.out.println("testcanviaContrassenya");
        System.out.println("Entra la nueva contraseña:");
        String nova_pass = sca.next();
        jug.canviaContrassenya(nova_pass);
    }
    
    public void testcomprovaContrassenya(){
        System.out.println("testcomprovaContrassenya");
        System.out.println("Entra la contraseña que quieras comprovar");
        String pass_es = sca.next();
        if (jug.comprovaContrassenya(pass_es)) System.out.println("Corrcto!");
        else System.out.println("Mal!");
    }
    
    public void testimprimeix_ids(){
        System.out.println("testimprimeix_ids");
        jug.imprimeix_ids();
    }
    
    public void testafegeixIds(){
        System.out.println("testafegeixIds");
        System.out.println("Introduce el id de la partida a añadir");
        String idd = sca.next();
        jug.afegeixIds(idd);
    }
    
    public void testcrear_partida() throws Exception{
        System.out.println("testcrear_partida");
        p = jug.crear_partida();
    }
    
    public void testcarrega_partida() throws Exception{
        System.out.println("testcarrega_partida");
        Partida pp = jug.carrega_partida(p);
    }
    
    public void menuDriver(){
        System.out.println("-------------------------");
        System.out.println("Selecciona el número de la función que quieres probar:");
        System.out.println("0- Salir del Driver");
        System.out.println("1- testGetNom");
        System.out.println("2- testGetPass");
        System.out.println("3- testGetMaxPuntuacio");
        System.out.println("4- testcanviaContrassenya");
        System.out.println("5- testcomprovaContrassenya");
        System.out.println("6- testimprimeix_ids");
        System.out.println("7- testafegeixIds");
        System.out.println("8- testcrear_partida");
        System.out.println("9- testcarrega_partida");
        System.out.println("-------------------------");
    }
    
    public static void main(String[] args) throws Exception{
        System.out.println("Inicio test Jugador.java");
        JugadorDriver jugd = new JugadorDriver();
        jugd.testConstructor();
        Scanner sca2 = new Scanner(System.in);
        jugd.menuDriver();
        int n = sca2.nextInt();
        while(n != 0){
            switch(n){
                case 1:
                    jugd.testGetNom();
                    break;
                case 2:
                    jugd.testGetPass();
                    break;
                case 3:
                    jugd.testgetMaxPuntuacio();
                    break;
                case 4:
                    jugd.testcanviaContrassenya();
                    break;
                case 5:
                    jugd.testcomprovaContrassenya();
                    break;
                case 6:
                    jugd.testimprimeix_ids();
                    break;
                case 7:
                    jugd.testafegeixIds();
                    break;
                case 8:
                    jugd.testcrear_partida();
                    break;
                case 9:
                    jugd.testcarrega_partida();
                    break;
                
            }
            if (n != 0){
                jugd.menuDriver();
                n = sca2.nextInt();
            }
        }
        System.out.println("Fin del test");
    }

}
