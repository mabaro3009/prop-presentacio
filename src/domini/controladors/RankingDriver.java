package domini.controladors;

import domini.clases.Ranking;
import java.lang.*;
import java.util.Scanner;
import domini.clases.Registre;

/** Test de Ranking.java
* @author Daniel Tarrés
*/
public class RankingDriver {
    static Ranking rank;
    Scanner sca = new Scanner(System.in);

    public  void testConstructor(){
        System.out.println("testConstructor");
        this.rank = new Ranking();
        System.out.println("Ranking creado");
    }
    
    public void testmostra_rank(){
        System.out.println("testmostra_rank");
        System.out.println("Introduce el número de registros a mostrar");
        int quants = sca.nextInt();
        rank.mostra_rank(quants);
    }
   
    public void testafegir(){
        System.out.println("testafegir");
        System.out.println("Crea un registro antes:");
        System.out.println("introduce id");
        String id = sca.next();
        System.out.println("introduce user");
        String user = sca.next();
        System.out.println("introduce puntuación");
        int punt = sca.nextInt();
        Registre reg = new Registre(id, punt, user);
        rank.afegir(reg);
        System.out.println("Registro añadido al ranking");
    }
    
    public void menuDriver(){
        System.out.println("-------------------------");
        System.out.println("Selecciona el número de la función que quieres probar:");
        System.out.println("0- Salir del Driver");
        System.out.println("1- Mostrar ranking");
        System.out.println("2- Añadir registro al ranking");
        System.out.println("-------------------------");
    }
    
    public static void main(String[] args) throws Exception{
        System.out.println("Inicio test Registre.java");
        RankingDriver rankd = new RankingDriver();
        rankd.testConstructor();
        Scanner sca2 = new Scanner(System.in);
        rankd.menuDriver();
        int n = sca2.nextInt();
        while(n != 0){
            switch(n){
                case 1:
                    rankd.testmostra_rank();
                    break;
                case 2:
                    rankd.testafegir();
                    break;
                
            }
            if (n != 0){
                rankd.menuDriver();
                n = sca2.nextInt();
            }
        }
        System.out.println("Fin del test");
    }

}
