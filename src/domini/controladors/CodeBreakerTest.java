package domini.controladors;

import domini.clases.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class CodeBreakerTest {
  	CodeBreaker cm;
  	@Test
	public void testgetID()throws Exception{
		cm = new CodeBreaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals("id_partida", cm.getID());
	}

	@Test
	public void testgetuser()throws Exception{
		cm = new CodeBreaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals("id_jugador", cm.getuser());
	}
	
	@Test
	public void testgetPuntuacio()throws Exception{
		cm = new CodeBreaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(0, cm.getPuntuacio());
	}

	@Test
	public void testgetnFiles()throws Exception{
		cm = new CodeBreaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(10, cm.getnFiles());
	}	

	@Test
	public void testgetnColumnes()throws Exception{
		cm = new CodeBreaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(5, cm.getnColumnes());
	}

	@Test
	public void testgetnColors()throws Exception{
		cm = new CodeBreaker("id_partida", "id_jugador");
		cm.crear_tauler(10, 5, 6);
		assertEquals(6, cm.getnColors());
	}
	
	@Test
	public void testcalculapuntuacio()throws Exception{
	  cm = new CodeBreaker("id_partida", "id_jugador");
	  cm.crear_tauler(10, 5, 6);
	  cm.calcula_puntuacio(4);
	  assertEquals(60000, cm.getPuntuacio());
	}
	
}