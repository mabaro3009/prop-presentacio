package domini.controladors;

import domini.clases.Fila;
import java.lang.*;
import java.util.Scanner;

/** Test de Fila.java
* @author Marc Badia
*/

public class FilaDriver {
  static Fila f;
  Scanner sca = new Scanner(System.in);
  
  public  void testConstructor(){
    System.out.println("testConstructor");
    System.out.println("Introduce el número de columnas");
    int nc = sca.nextInt();
    if(nc <= 0){ System.out.println("Columnas > 0"); testConstructor();}
    else {
      this.f = new Fila(nc);
      System.out.println("Fila creada");
    }
  }
  
  public void testgetColumnes(){
    System.out.println("testgetColumnes");
    System.out.println("Esta fila tiene " + f.getColumnes() + " columnas");
  }
  
  public void testsetColor()throws Exception{
    System.out.println("testsetColor");
    int color;
    System.out.println("Introduce los colores de la fila");
    for(int i = 0; i < f.getColumnes(); ++i){
      color = sca.nextInt(); 
      if(color < 0) System.out.println("Color ha de ser mayor o igual que 0");
      else f.setColor(i, color);
    }
  }
  
  public void testgetColor(){
    System.out.println("testgetColor");
      for(int i = 0; i < f.getColumnes(); ++i)
	System.out.println("El color de la columna " + i + " es " + f.getColor(0));
  }
  
  public void testRetorna_fila(){
    System.out.println("testRetorna_fila");
    System.out.println(f.retorna_fila());
  }
  
  public void testsolucio_random()throws Exception{
    System.out.println("testSolucio_random");
    System.out.println("Introduce el número de colores");
    int nColors = sca.nextInt();
    System.out.println("hola");
    if(nColors < 0) System.out.println("nColores ha de ser mayor o igual que 0");
    else {
        f = Fila.filaRandom(nColors, nColors);
        System.out.println(f.retorna_fila());
    }
  }
  
  public void testColorExists()throws Exception{
    System.out.println("testColorExists");
    System.out.println("Introduce el color a buscar");
    int busca = sca.nextInt();
    if(busca < 0) System.out.println("Color ha de ser mayor o igual que 0");
    else {
      System.out.println(f.colorExists(busca, f.getColumnes()));
    }
  }
  
  public void testcheck() throws Exception{
    System.out.println("testcheck");
    Fila fs = new Fila(f.getColumnes());
    Fila fj = new Fila(f.getColumnes());
    int color;
    System.out.println("Introduce los colores de la fila solución");
    for(int i = 0; i < f.getColumnes(); ++i){
      color = sca.nextInt(); 
      if(color < 0) System.out.println("Color ha de ser mayor o igual que 0");
      else fs.setColor(i, color);
    }
    System.out.println("Introduce los colores de la fila jugada");
    for(int i = 0; i < f.getColumnes(); ++i){
      color = sca.nextInt(); 
      if(color < 0) System.out.println("Color ha de ser mayor o igual que 0");
      else fj.setColor(i, color);
    }
    boolean b = f.check(fj, fs);
    System.out.println("Esta es la fila comprobación resultante:");
    System.out.println(f.retorna_fila());
  }
  
  public void menu() {
    System.out.println("---------------------------------------");
    System.out.println("Menú");
    System.out.println("1 - getColumnes");
    System.out.println("2 - setColor");
    System.out.println("3 - getColor");
    System.out.println("4 - RetornaFila");
    System.out.println("5 - solucio_random");
    System.out.println("6 - ColorExists");
    System.out.println("7 - Check");
    System.out.println("0 - Salir");
  }
  
  public static void main (String[] args) throws Exception{
    System.out.println("Inicio test Fila.java");
    FilaDriver fd = new FilaDriver();
    fd.testConstructor();
    fd.menu();
    Scanner sc = new Scanner(System.in);
    int op = sc.nextInt();
    while(op != 0) {
      switch(op) {
	case 1:
	   fd.testgetColumnes();
	   break;
	case 2:
	   fd.testsetColor();
	   break;
	case 3:
	   fd.testgetColor();
	   break;
	case 4:
	   fd.testRetorna_fila();
	   break;
	case 5:
	   fd.testsolucio_random();
	   break;
	case 6:
	   fd.testColorExists();
	   break;
	case 7:
	   fd.testcheck();
	   break;
      }
      fd.menu();
      op = sc.nextInt();
    }
  }
}
