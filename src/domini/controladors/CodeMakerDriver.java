package domini.controladors;

import domini.clases.*;
import java.lang.*;
import java.util.Scanner;

/** Test de CodeMaker.java
* @author Marc Badia
*/

public class CodeMakerDriver {
  static CodeMaker cm;
  Scanner sca = new Scanner(System.in);
  int n = 0;
  public  void testConstructor() throws Exception{
    System.out.println("testConstructor");
    System.out.println("Introduce el id de la partida");
    String id = sca.nextLine();
    System.out.println("Introduce el user de la partida");
    String user = sca.nextLine();
    cm = new CodeMaker(id, user);
    //tcm.creaTauler();

  }

  public boolean creaTauler() throws Exception{
		boolean b = true;
    		System.out.println(" ");
		System.out.println("Número de Filas:");
        	int f = sca.nextInt();
		if(f<0){b=false; System.out.println("Dato inválido");}
        	System.out.println("Número de Columnas:");
        	int c = sca.nextInt();
		if(c<0){b=false; System.out.println("Dato inválido");}
        	System.out.println("Número de Colores (Colores >= Columnas):");
        	int colors = sca.nextInt();
		if(colors<c){b=false; System.out.println("Dato inválido");}
        	if(b) cm.crear_tauler(f, c, colors);
		return b;
  }
  
  public  void testgetters() throws Exception{
    System.out.println("testgetters");
    System.out.println("id: " + cm.getID() + ", user: " + cm.getuser() + ", puntuacio: " + cm.getPuntuacio());
    System.out.println("filas: " + cm.getnFiles() + ", columnas: " + cm.getnColumnes() + ", colores: " + cm.getnColors());
  }
  
  public  void testget_solucio() throws Exception{
    System.out.println("testget_solucio");
    cm.get_solucio();
  }
  
  public  void testcolorOk() throws Exception{
    System.out.println("testColorOk");
    System.out.println("Introduce color");
    int c = sca.nextInt();
    System.out.println("Introduce posición");
    int i = sca.nextInt();
    cm.colorOk(c,i);
  }
  
  public void testjugar() throws Exception{
    System.out.println("testjugar");
    cm.play();
  }
  
  public void menu() {
    System.out.println("----------------------------------------");
    System.out.println("Menú");
    System.out.println("1 - getters");
    System.out.println("2 - get_solucio");
    System.out.println("3 - colorOk");
    System.out.println("4 - jugar");
    System.out.println("0 - Salir");
  }
  
   public static void main (String[] args) throws Exception{
    System.out.println("Inicio test CodeMaker.java");
    CodeMakerDriver tcm = new CodeMakerDriver();
    tcm.testConstructor();
    boolean b = false;
    while(!b) {
	b = tcm.creaTauler();
    }
    tcm.menu();
    Scanner sc = new Scanner(System.in);
    int op = sc.nextInt();
    while(op != 0) {
      switch(op) {
	case 1:
	   tcm.testgetters();
	   break;
	case 2:
	   tcm.testget_solucio();
	   break;
	case 3:
	   tcm.testcolorOk();
	   break;
	case 4:
	   tcm.testjugar();
	   break;
      }
      tcm.menu();
      op = sc.nextInt();
    }
  }
}
