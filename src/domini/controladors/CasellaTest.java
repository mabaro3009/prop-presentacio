package domini.controladors;

import domini.clases.Casella;
import static org.junit.Assert.*;
import org.junit.Test;

public class CasellaTest {
  @Test
  public void getColor_new() {
    Casella c = new Casella();
    int result = c.getColor();
    assertEquals(0, result, 0);
  }
  
  @Test
  public void getColor_mod() throws Exception {
    Casella c = new Casella();
    c.setColor(5);
    int result = c.getColor();
    assertEquals(5, result, 0);
  }
}
