package presentacio;
import dades.FitxerNoExisteixExcepcio;
import domini.clases.CtrlDominiPresentacio;
import dades.GestorDades;
import dades.IOExcepcio;
import java.util.List;
import java.util.*;

public class CtrlPresentacio {
	private CtrlDominiPresentacio cd;
	//private GestorDades gd; 
	private VPrincipal vistaPrincipal = null;
	
	public CtrlPresentacio() {
		this.cd = new CtrlDominiPresentacio();
		//this.gd = new GestorDades();
		if(vistaPrincipal == null)
			this.vistaPrincipal = new VPrincipal(this);
	}
	
	public void inici() throws Exception{
		//cd.inicialitzar(); //Es pot treure quan vagi la vista d'abaix
		vistaPrincipal.setVisible(true); //setVisible = Vista que es mostra. No cal implementar.
	}
       
        
        public static void main(String[] argv) {
            		javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            
                        @Override
			public void run() {
				CtrlPresentacio cp = new CtrlPresentacio();
				try{
					cp.inici();
				}catch(Exception e){
					System.exit(1);
				}
		}});
        }

    public boolean crearUser(String user, String pass) {
        return cd.crearNouUsuari(user, pass);
    }
    
    public boolean logIn(String user, String pass){
        return cd.logIn(user, pass);
    }
    public List <String> getRanking(){
        List<String> aaa = new ArrayList<String>(); //???
        
        
        return cd.obtenirRanking();
    }
    
    public int getMaxP(){
        return cd.getMaxP();
    }
    
    public void borrar() throws IOExcepcio, FitxerNoExisteixExcepcio{
        cd.borrar();
    }
    
    public String getUser(){
        return cd.getUser();
    }
    
    public void canviarPass(String pass){
        cd.canviPass(pass);
    }
    
    public void crearPartida(int files, int columnes, int colors, boolean mode){
        cd.crearPartida(files, columnes, colors, mode);
    }
    
    public List<Integer> jugaFila(List<Integer> posicions, int fila) throws Exception{
        cd.introdueixFila(posicions, fila);
        return cd.comprovaFila(posicions);
    }
    
    public int get_color_solucio(int posicio){
        return cd.get_color_solucio(posicio);
    }
    public int getpuntuacio(int fila){
        return cd.getpuntuacio(fila);
    }
    public void afegeix_pista_color(){
        cd.afegeix_pista_color();
    }
    public void afegeix_pista_posicio_color(){
        cd.afegeix_pista_posicio_color();
    }
    public boolean afegeix_puntuacio_jugador(int puntuacio){
        return cd.afegeix_puntuacio_jugador(puntuacio);
    }
    public void afegir_ranking(int puntuacio){
        cd.afegir_ranking(puntuacio);
    }
    
    public void guardar_partida(){
        cd.guardar_partida();
    }
    
    public List<String> carregar_partides_jugador(){
        return cd.carregar_partides_jugador();
    }
    public void carrrega_partida(String partida_seleccionada){
        cd.carrrega_partida(partida_seleccionada);
    }
    public int getfiles(){
        return cd.getfiles();
    }
    public int getcolors(){
        return cd.getcolors();
    }
    public int getcolumnes(){
        return cd.getcolumnes();
    }
    public int getfila_actual(){
        return cd.getfila_actual();
    }
    public List<Integer> get_colors_fila(int i){
        return cd.get_colors_fila(i);
    }
    
    public void set_encert_pos_col(int nfila, int n){
        cd.set_encert_pos_col(nfila, n);
    }
    public void set_encert_pos(int nfila, int n){
        cd.set_encert_pos(nfila, n);
    }
    public int get_encert_pos_col(int nfila){
        return cd.get_encert_pos_col(nfila);
    }
    public int get_encert_pos(int nfila){
        return cd.get_encert_pos(nfila);
    }
    public int get_color_posicio(int fila, int posicio){
        return cd.get_color_posicio(fila, posicio);
    }
    public void set_solucio(List<Integer> codi_maker){
        cd.set_solucio(codi_maker);
    }
    
    public List<Integer> getCodi(int fila_actual){
        return cd.getCodi(fila_actual);
    }
    
    public void setFilaComp(int fila_actual, int avis_vermell, int avis_blanc){
        cd.setFilaComp(fila_actual, avis_vermell, avis_blanc, cd.getcolumnes()); 
    }
    public boolean comprova(int fila_actual, int avis_vermell, int avis_blanc){
        return cd.comprova(fila_actual, avis_vermell, avis_blanc);
    }
    public List<Integer> seguentJugada(int avis_vermell, int avis_blanc) {
        return cd.seguentJugada(avis_vermell, avis_blanc);
    }
    public void instanciaAlgoritme(int nCaselles, int nColors) {
        cd.instanciaAlgoritme(nCaselles, nColors);
    }

}
