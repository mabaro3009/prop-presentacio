/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacio;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author danie
 */
public class VTauler extends javax.swing.JFrame {
    private int color_seleccionat; //0 = no seleccionat
    private CtrlPresentacio cp;
    private int columnes;
    private int files;
    private int colors;
    private int fila_actual;
    private List<Integer> posicions_marcades;
    private VMenuJugador vistaMenuJugador;
    private int pistes_colors;
    private int pistes_pos_col;
    private boolean estat_joc_breaker;
    private int avis_vermell;
    private int avis_blanc;
    private List<Integer> color_avis;
    private List<Integer> solucio;
    private List<Integer> seguent_jugada;
    
    public VTauler(CtrlPresentacio cp, VMenuJugador vmj, boolean estat_breaker) {
        initComponents();
        this.setLocationRelativeTo(null);
        estat_joc_breaker = estat_breaker;
        if (estat_breaker){
            components_maker_invisibles();
        }else{
            components_breaker_invisibles();
        }
        this.cp = cp;
        color_seleccionat = 0;
        avis_vermell = 0;
        avis_blanc = 0;
        fila_actual = 1; 
        this.vistaMenuJugador = vmj;
        pistes_colors = 0;
        pistes_pos_col = 0;
        color_avis = new ArrayList<Integer>(9);
        for (int i = 0; i < 9; i++){
            color_avis.add(i, 0);
        }
        
    }
    private void neteja_color_avis(){
        for (int i = 0; i < 9; i++){
            color_avis.set(i, 0);
        }
    }
    public void set_codi(List<Integer> codi_crea_codi){
        solucio = codi_crea_codi;
    }
    
    public void set_seguent_jugada(List<Integer>code){
        seguent_jugada = code;
    }
    
    private boolean comprova_solucio_possible(){
        int vermells_temp = 0;
        int blancs_temp = 0;
        for (int i = 0; i < columnes; i++){
            if (seguent_jugada.get(i) == solucio.get(i))vermells_temp++;
            else{
                for (int j = 0; j < columnes; j++){
                    if (seguent_jugada.get(j) == solucio.get(i)) blancs_temp++;
                }
            }
        }
        return (blancs_temp == avis_blanc && vermells_temp == avis_vermell);
    }
    
    private void actualitza_numero_labels_codi(){
        if (columnes < 9) jLabel297.setVisible(false);
        if (columnes < 8) jLabel293.setVisible(false);
        if (columnes < 7) jLabel298.setVisible(false);
        if (columnes < 6) jLabel294.setVisible(false);
        if (columnes < 5) jLabel291.setVisible(false);
        if (columnes < 4) jLabel290.setVisible(false);
        if (columnes < 3) jLabel296.setVisible(false);
        if (columnes < 2) jLabel292.setVisible(false);
        
    }
    
    private void components_maker_invisibles(){
        jPanel18.setVisible(false);
        jButton16.setVisible(false);
    }
    private void components_breaker_invisibles(){
        jButton13.setVisible(false);
        jButton14.setVisible(false);
        jButton15.setVisible(false);
        jButton11.setVisible(false);
        jButton1.setVisible(false);
        jButton2.setVisible(false);
        jButton3.setVisible(false);
        jButton4.setVisible(false);
        jButton5.setVisible(false);
        jButton6.setVisible(false);
        jButton7.setVisible(false);
        jButton8.setVisible(false);
        jButton9.setVisible(false);
        jButton10.setVisible(false);
    }
    public void set_pistes_colors(int n){
        pistes_colors = n;
    }
    public void set_pistes_pos_col(int n){
        pistes_pos_col = n;
    }
    
    public void pinta_codi(List<Integer> codi_mk){
        jLabel295.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(0) +  "_25.png")));
        jLabel292.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(1) +  "_25.png")));
        jLabel296.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(2) +  "_25.png")));
        jLabel290.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(3) +  "_25.png")));
        jLabel291.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(4) +  "_25.png")));
        jLabel294.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(5) +  "_25.png")));
        jLabel298.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(6) +  "_25.png")));
        jLabel293.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(7) +  "_25.png")));
        jLabel297.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + codi_mk.get(8) +  "_25.png")));
    }
    
    public void mostra_pista(int posicio){
        pistes_pos_col++;
        cp.afegeix_pista_posicio_color();

        int color_pista;
        color_pista = cp.get_color_solucio(posicio);
        String nom_pista;
        switch(color_pista){
            case 1:
                nom_pista = "Verde";
                break;
            case 2:
                nom_pista = "Azul fuerte";
                break;
            case 3:
                nom_pista = "Rojo";
                break;
            case 4:
                nom_pista = "Amarillo";
                break;
            case 5:
                nom_pista = "Naranja";
                break;
            case 6:
                nom_pista = "Rosa";
                break;
            case 7:
                nom_pista = "Azul claro";
                break;
            case 8:
                nom_pista = "Lila";
                break;
            case 9:
                nom_pista = "Negro";
                break;
            default:
                nom_pista = "";
        }
        javax.swing.JOptionPane.showMessageDialog(this, "La posición " + posicio + " tiene el color " + nom_pista, "Pista posición y color", javax.swing.JOptionPane.ERROR_MESSAGE);
    }
    
    public void mostra_pista_color(int posicio){
        cp.afegeix_pista_color();

        int color_pista;
        color_pista = cp.get_color_solucio(posicio);
        String nom_pista;
        switch(color_pista){
            case 1:
                nom_pista = "Verde";
                break;
            case 2:
                nom_pista = "Azul fuerte";
                break;
            case 3:
                nom_pista = "Rojo";
                break;
            case 4:
                nom_pista = "Amarillo";
                break;
            case 5:
                nom_pista = "Naranja";
                break;
            case 6:
                nom_pista = "Rosa";
                break;
            case 7:
                nom_pista = "Azul claro";
                break;
            case 8:
                nom_pista = "Lila";
                break;
            case 9:
                nom_pista = "Negro";
                break;
            default:
                nom_pista = "";
        }
        javax.swing.JOptionPane.showMessageDialog(this, "El color " + nom_pista + " está en la solución", "Pista color", javax.swing.JOptionPane.ERROR_MESSAGE);
    }
    
    private void canvia_color_seleccionat(int nou_color){
        color_seleccionat = nou_color;
        String source_color = "/presentacio/" + nou_color + ".png";
            //jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource(source_color)));

    }
    
    private void neteja_posicions(){
        for (int i = 0; i < columnes; i++){
            posicions_marcades.set(i, 0);
        }
    }
    private void crea_posicions(){
        for (int i = 0; i < columnes; i++){
            posicions_marcades.add(i, 0);
        }
    }
    
    public void marca_encerts(int fila, int pos_i_col, int pos){
       
        List<String> nom_color = new ArrayList<String>(15);
        //String source_icon_encert = "/presentacio/" +  +  ".png";
        int pos_col = pos_i_col;
        int pos_be = pos;
        int posicio = 0;
        while(pos_col > 0){
            nom_color.add(posicio, "vermell_9");
            pos_col--;
            posicio++;
        }
        while(pos_be > 0){
            nom_color.add(posicio, "blanc_9");
            pos_be--;
            posicio++;
        }
        while(posicio < 14){
            nom_color.add(posicio, "negre_9");
            posicio++;
        }
        switch(fila){
            case 1:
                 jLabel164.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel165.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel166.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel167.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel168.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel169.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel170.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel171.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel172.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 2:
                jLabel146.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel147.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel148.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel149.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel150.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel151.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel152.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel153.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel154.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 3:
                jLabel155.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel160.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel159.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel156.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel162.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel157.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel163.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel158.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel161.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 4:
                jLabel173.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel174.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel175.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel176.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel177.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel178.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel179.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel180.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel181.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 5:
                jLabel182.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel183.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel184.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel185.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel186.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel187.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel188.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel189.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel190.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 6:
                jLabel191.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel192.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel193.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel194.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel195.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel196.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel197.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel198.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel199.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 7:
                jLabel200.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel201.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel202.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel203.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel204.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel205.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel206.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel207.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel208.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 8:
                jLabel209.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel211.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel212.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel213.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel214.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel215.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel216.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel217.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 9:
                jLabel218.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel219.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel220.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel221.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel222.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel223.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel224.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel225.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel226.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 10:
                jLabel227.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel228.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel229.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel230.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel231.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel232.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel233.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel234.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel235.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 11:
                jLabel236.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel237.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel238.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel239.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel240.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel241.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel242.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel243.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel244.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 12:
                jLabel245.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel246.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel247.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel248.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel249.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel250.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel251.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel252.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel253.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 13:
               jLabel254.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel255.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel256.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel257.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel258.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel259.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel260.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel261.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel262.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 14:
                jLabel263.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel264.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel265.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel266.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel267.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel268.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel269.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel270.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel271.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
            case 15:
                jLabel272.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(0) +  ".png")));
                 jLabel273.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(1) +  ".png")));
                 jLabel274.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(2) +  ".png")));
                 jLabel275.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(3) +  ".png")));
                 jLabel276.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(4) +  ".png")));
                 jLabel277.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(5) +  ".png")));
                 jLabel278.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(6) +  ".png")));
                 jLabel279.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(7) +  ".png")));
                 jLabel280.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_color.get(8 ) +  ".png")));
                break;
                
        }
    }
    
    public void set_fila_actual(int nova_fila){
        fila_actual = nova_fila;
    }
    
    private void borra_columna9(){
        jLabel10.setVisible(false);
        jLabel19.setVisible(false);
        jLabel37.setVisible(false);
        jLabel28.setVisible(false);
        jLabel73.setVisible(false);
        jLabel46.setVisible(false);
        jLabel68.setVisible(false);
        jLabel56.setVisible(false);
        jLabel76.setVisible(false);
        jLabel94.setVisible(false);
        jLabel85.setVisible(false);
        jLabel101.setVisible(false);
        jLabel121.setVisible(false);
        jLabel126.setVisible(false);
        jLabel129.setVisible(false);
        
        jLabel172.setVisible(false);
        jLabel154.setVisible(false);
        jLabel161.setVisible(false);
        jLabel181.setVisible(false);
        jLabel190.setVisible(false);
        jLabel199.setVisible(false);
        jLabel208.setVisible(false);
        jLabel217.setVisible(false);
        jLabel226.setVisible(false);
        jLabel235.setVisible(false);
        jLabel244.setVisible(false);
        jLabel253.setVisible(false);
        jLabel262.setVisible(false);
        jLabel271.setVisible(false);
        jLabel280.setVisible(false);
    }
    private void borra_columna8(){
        jLabel7.setVisible(false);
        jLabel16.setVisible(false);
        jLabel34.setVisible(false);
        jLabel25.setVisible(false);
        jLabel70.setVisible(false);
        jLabel43.setVisible(false);
        jLabel65.setVisible(false);
        jLabel53.setVisible(false);
        jLabel100.setVisible(false);
        jLabel91.setVisible(false);
        jLabel82.setVisible(false);
        jLabel135.setVisible(false);
        jLabel133.setVisible(false);
        jLabel122.setVisible(false);
        jLabel115.setVisible(false);
        
        jLabel171.setVisible(false);
        jLabel153.setVisible(false);
        jLabel158.setVisible(false);
        jLabel180.setVisible(false);
        jLabel189.setVisible(false);
        jLabel198.setVisible(false);
        jLabel207.setVisible(false);
        jLabel216.setVisible(false);
        jLabel225.setVisible(false);
        jLabel234.setVisible(false);
        jLabel243.setVisible(false);
        jLabel252.setVisible(false);
        jLabel261.setVisible(false);
        jLabel270.setVisible(false);
        jLabel279.setVisible(false);
    }
    private void borra_columna7(){
        jLabel8.setVisible(false);
        jLabel17.setVisible(false);
        jLabel35.setVisible(false);
        jLabel26.setVisible(false);
        jLabel71.setVisible(false);
        jLabel44.setVisible(false);
        jLabel66.setVisible(false);
        jLabel54.setVisible(false);
        jLabel74.setVisible(false);
        jLabel92.setVisible(false);
        jLabel83.setVisible(false);
        jLabel103.setVisible(false);
        jLabel112.setVisible(false);
        jLabel118.setVisible(false);
        jLabel108.setVisible(false);
        
        jLabel170.setVisible(false);
        jLabel152.setVisible(false);
        jLabel163.setVisible(false);
        jLabel179.setVisible(false);
        jLabel188.setVisible(false);
        jLabel197.setVisible(false);
        jLabel206.setVisible(false);
        jLabel215.setVisible(false);
        jLabel224.setVisible(false);
        jLabel233.setVisible(false);
        jLabel242.setVisible(false);
        jLabel251.setVisible(false);
        jLabel260.setVisible(false);
        jLabel269.setVisible(false);
        jLabel278.setVisible(false);
    }
    private void borra_columna6(){
        jLabel6.setVisible(false);
        jLabel15.setVisible(false);
        jLabel33.setVisible(false);
        jLabel24.setVisible(false);
        jLabel69.setVisible(false);
        jLabel42.setVisible(false);
        jLabel61.setVisible(false);
        jLabel52.setVisible(false);
        jLabel99.setVisible(false);
        jLabel90.setVisible(false);
        jLabel81.setVisible(false);
        jLabel123.setVisible(false);
        jLabel117.setVisible(false);
        jLabel104.setVisible(false);
        jLabel102.setVisible(false);
        
        jLabel169.setVisible(false);
        jLabel151.setVisible(false);
        jLabel157.setVisible(false);
        jLabel178.setVisible(false);
        jLabel187.setVisible(false);
        jLabel196.setVisible(false);
        jLabel205.setVisible(false);
        jLabel214.setVisible(false);
        jLabel223.setVisible(false);
        jLabel232.setVisible(false);
        jLabel241.setVisible(false);
        jLabel250.setVisible(false);
        jLabel259.setVisible(false);
        jLabel268.setVisible(false);
        jLabel277.setVisible(false);
    }
    private void borra_columna5(){
        jLabel9.setVisible(false);
        jLabel18.setVisible(false);
        jLabel36.setVisible(false);
        jLabel27.setVisible(false);
        jLabel72.setVisible(false);
        jLabel45.setVisible(false);
        jLabel67.setVisible(false);
        jLabel55.setVisible(false);
        jLabel75.setVisible(false);
        jLabel93.setVisible(false);
        jLabel84.setVisible(false);
        jLabel109.setVisible(false);
        jLabel107.setVisible(false);
        jLabel113.setVisible(false);
        jLabel134.setVisible(false);
        
         jLabel168.setVisible(false);
        jLabel150.setVisible(false);
        jLabel162.setVisible(false);
        jLabel177.setVisible(false);
        jLabel186.setVisible(false);
        jLabel195.setVisible(false);
        jLabel204.setVisible(false);
        jLabel213.setVisible(false);
        jLabel222.setVisible(false);
        jLabel231.setVisible(false);
        jLabel240.setVisible(false);
        jLabel249.setVisible(false);
        jLabel258.setVisible(false);
        jLabel267.setVisible(false);
        jLabel276.setVisible(false);
    }
    private void borra_columna4(){
        jLabel4.setVisible(false);
        jLabel13.setVisible(false);
        jLabel31.setVisible(false);
        jLabel22.setVisible(false);
        jLabel63.setVisible(false);
        jLabel40.setVisible(false);
        jLabel59.setVisible(false);
        jLabel49.setVisible(false);
        jLabel97.setVisible(false);
        jLabel88.setVisible(false);
        jLabel79.setVisible(false);
        jLabel119.setVisible(false);
        jLabel116.setVisible(false);
        jLabel124.setVisible(false);
        jLabel132.setVisible(false);
        
        jLabel167.setVisible(false);
        jLabel149.setVisible(false);
        jLabel156.setVisible(false);
        jLabel176.setVisible(false);
        jLabel185.setVisible(false);
        jLabel194.setVisible(false);
        jLabel203.setVisible(false);
        jLabel212.setVisible(false);
        jLabel221.setVisible(false);
        jLabel230.setVisible(false);
        jLabel239.setVisible(false);
        jLabel248.setVisible(false);
        jLabel257.setVisible(false);
        jLabel266.setVisible(false);
        jLabel275.setVisible(false);
    }
    private void borra_columna3(){
        jLabel5.setVisible(false);
        jLabel14.setVisible(false);
        jLabel32.setVisible(false);
        jLabel23.setVisible(false);
        jLabel64.setVisible(false);
        jLabel41.setVisible(false);
        jLabel60.setVisible(false);
        jLabel50.setVisible(false);
        jLabel98.setVisible(false);
        jLabel89.setVisible(false);
        jLabel80.setVisible(false);
        jLabel111.setVisible(false);
        jLabel125.setVisible(false);
        jLabel120.setVisible(false);
        jLabel128.setVisible(false);
        
        jLabel166.setVisible(false);
        jLabel148.setVisible(false);
        jLabel159.setVisible(false);
        jLabel175.setVisible(false);
        jLabel184.setVisible(false);
        jLabel193.setVisible(false);
        jLabel202.setVisible(false);
        jLabel211.setVisible(false);
        jLabel220.setVisible(false);
        jLabel229.setVisible(false);
        jLabel238.setVisible(false);
        jLabel247.setVisible(false);
        jLabel256.setVisible(false);
        jLabel265.setVisible(false);
        jLabel274.setVisible(false);
    }
    private void borra_columna2(){
        jLabel3.setVisible(false);
        jLabel12.setVisible(false);
        jLabel30.setVisible(false);
        jLabel21.setVisible(false);
        jLabel62.setVisible(false);
        jLabel39.setVisible(false);
        jLabel58.setVisible(false);
        jLabel48.setVisible(false);
        jLabel96.setVisible(false);
        jLabel87.setVisible(false);
        jLabel78.setVisible(false);
        jLabel114.setVisible(false);
        jLabel130.setVisible(false);
        jLabel127.setVisible(false);
        jLabel136.setVisible(false);
        
        jLabel165.setVisible(false);
        jLabel147.setVisible(false);
        jLabel160.setVisible(false);
        jLabel174.setVisible(false);
        jLabel183.setVisible(false);
        jLabel192.setVisible(false);
        jLabel201.setVisible(false);
        jLabel210.setVisible(false);
        jLabel219.setVisible(false);
        jLabel228.setVisible(false);
        jLabel237.setVisible(false);
        jLabel246.setVisible(false);
        jLabel255.setVisible(false);
        jLabel264.setVisible(false);
        jLabel273.setVisible(false);
    }
    private void borra_colors9(){
        jButton9.setVisible(false);
        
        
    }
    private void borra_colors8(){
        jButton8.setVisible(false);
        
    }
    private void borra_colors7(){
        jButton7.setVisible(false);
        
    }
    private void borra_colors6(){
        jButton6.setVisible(false);
        
    }
    private void borra_colors5(){
        jButton5.setVisible(false);
       
    }
    private void borra_colors4(){
        jButton4.setVisible(false);
        
    }
    private void borra_colors3(){
        jButton3.setVisible(false);
        
    }
    private void borra_colors2(){
        jButton2.setVisible(false);
        
    }
    //pre:  fila comença des del 1, no des del 0
    //      posicions es un ArrayList que té 9 elements. Si la partida que s'està jugant només té 5 colors, la resta queden a 0, però han de tenir valor
    //      els colors comencen des de l'1 fins al 9
    public void afegeix_linea(int fila, List<Integer> posicions){
        switch (fila){
            case 1:
                
                jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                
                break;
            case 2:
                jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 3:
                jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 4:
                jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 5:
                jLabel51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel63.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel72.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel69.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel71.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel70.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel73.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 6:
                jLabel38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel39.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel45.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 7:
                jLabel57.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel58.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel59.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel67.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel61.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel66.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel65.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel68.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 8:
                jLabel47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel49.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 9:
                jLabel95.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel96.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel98.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel97.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel75.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel99.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel74.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel100.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel76.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 10:
                jLabel86.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel87.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel89.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel88.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel93.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel90.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel92.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel91.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel94.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 11:
                jLabel77.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel78.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel80.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel79.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel84.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel81.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel83.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel82.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel85.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 12:
                jLabel131.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel114.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel111.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel119.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel109.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel123.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel103.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel135.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 13:
                jLabel105.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel130.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel125.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel116.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel107.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel117.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel112.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel133.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel121.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 14:
                jLabel110.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel127.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel120.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel124.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel113.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel104.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel118.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel122.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel126.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
            case 15:
                jLabel106.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(0) + "_25.png")));
                jLabel136.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(1) + "_25.png")));
                jLabel128.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(2) + "_25.png")));
                jLabel132.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(3) + "_25.png")));
                jLabel134.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(4) + "_25.png")));
                jLabel102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(5) + "_25.png")));
                jLabel108.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(6) + "_25.png")));
                jLabel115.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(7) + "_25.png")));
                jLabel129.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + posicions.get(8) + "_25.png")));
                break;
        }
    }
    
    
    
    
    public void redimensiona(int files, int columnes, int colors){
        this.columnes = columnes;
        this.files = files;
        this.colors = colors;
        posicions_marcades = new ArrayList<Integer>(columnes);
        crea_posicions();
            
        //files
        if (files < 15) jPanel16.setVisible(false);
        if (files < 14) jPanel15.setVisible(false);
        if (files < 13) jPanel14.setVisible(false);
        if (files < 12) jPanel13.setVisible(false);
        if (files < 11) jPanel12.setVisible(false);
        if (files < 10) jPanel11.setVisible(false);
        if (files < 9) jPanel10.setVisible(false);
        if (files < 8) jPanel9.setVisible(false);
        if (files < 7) jPanel8.setVisible(false);
        if (files < 6) jPanel7.setVisible(false);
        if (files < 5) jPanel6.setVisible(false);
        if (files < 4) jPanel5.setVisible(false);
        if (files < 3) jPanel4.setVisible(false);
        if (files < 2) jPanel3.setVisible(false);

        
        //columnes
        if (columnes < 9) borra_columna9();
        if (columnes < 8) borra_columna8();
        if (columnes < 7) borra_columna7();
        if (columnes < 6) borra_columna6();
        if (columnes < 5) borra_columna5();
        if (columnes < 4) borra_columna4();
        if (columnes < 3) borra_columna3();
        if (columnes < 2) borra_columna2();
        
        
        //colors
        if (colors < 9) borra_colors9();
        if (colors < 8) borra_colors8();
        if (colors < 7) borra_colors7();
        if (colors < 6) borra_colors6();
        if (colors < 5) borra_colors5();
        if (colors < 4) borra_colors4();
        if (colors < 3) borra_colors3();
        if (colors < 2) borra_colors2();
        
        actualitza_numero_labels_codi();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel17 = new javax.swing.JPanel();
        jLabel137 = new javax.swing.JLabel();
        jLabel138 = new javax.swing.JLabel();
        jLabel139 = new javax.swing.JLabel();
        jLabel140 = new javax.swing.JLabel();
        jLabel141 = new javax.swing.JLabel();
        jLabel142 = new javax.swing.JLabel();
        jLabel143 = new javax.swing.JLabel();
        jLabel144 = new javax.swing.JLabel();
        jLabel145 = new javax.swing.JLabel();
        jLabel281 = new javax.swing.JLabel();
        jLabel282 = new javax.swing.JLabel();
        jLabel283 = new javax.swing.JLabel();
        jLabel284 = new javax.swing.JLabel();
        jLabel285 = new javax.swing.JLabel();
        jLabel286 = new javax.swing.JLabel();
        jLabel287 = new javax.swing.JLabel();
        jLabel288 = new javax.swing.JLabel();
        jLabel289 = new javax.swing.JLabel();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jButton16 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel164 = new javax.swing.JLabel();
        jLabel165 = new javax.swing.JLabel();
        jLabel167 = new javax.swing.JLabel();
        jLabel166 = new javax.swing.JLabel();
        jLabel169 = new javax.swing.JLabel();
        jLabel168 = new javax.swing.JLabel();
        jLabel170 = new javax.swing.JLabel();
        jLabel171 = new javax.swing.JLabel();
        jLabel172 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel146 = new javax.swing.JLabel();
        jLabel147 = new javax.swing.JLabel();
        jLabel148 = new javax.swing.JLabel();
        jLabel149 = new javax.swing.JLabel();
        jLabel150 = new javax.swing.JLabel();
        jLabel151 = new javax.swing.JLabel();
        jLabel152 = new javax.swing.JLabel();
        jLabel153 = new javax.swing.JLabel();
        jLabel154 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel155 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel156 = new javax.swing.JLabel();
        jLabel157 = new javax.swing.JLabel();
        jLabel158 = new javax.swing.JLabel();
        jLabel159 = new javax.swing.JLabel();
        jLabel160 = new javax.swing.JLabel();
        jLabel161 = new javax.swing.JLabel();
        jLabel162 = new javax.swing.JLabel();
        jLabel163 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel173 = new javax.swing.JLabel();
        jLabel174 = new javax.swing.JLabel();
        jLabel175 = new javax.swing.JLabel();
        jLabel176 = new javax.swing.JLabel();
        jLabel177 = new javax.swing.JLabel();
        jLabel178 = new javax.swing.JLabel();
        jLabel179 = new javax.swing.JLabel();
        jLabel180 = new javax.swing.JLabel();
        jLabel181 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel73 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel182 = new javax.swing.JLabel();
        jLabel183 = new javax.swing.JLabel();
        jLabel184 = new javax.swing.JLabel();
        jLabel185 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel186 = new javax.swing.JLabel();
        jLabel187 = new javax.swing.JLabel();
        jLabel188 = new javax.swing.JLabel();
        jLabel189 = new javax.swing.JLabel();
        jLabel190 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel44 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel191 = new javax.swing.JLabel();
        jLabel192 = new javax.swing.JLabel();
        jLabel193 = new javax.swing.JLabel();
        jLabel194 = new javax.swing.JLabel();
        jLabel195 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel196 = new javax.swing.JLabel();
        jLabel197 = new javax.swing.JLabel();
        jLabel198 = new javax.swing.JLabel();
        jLabel199 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        jLabel200 = new javax.swing.JLabel();
        jLabel201 = new javax.swing.JLabel();
        jLabel202 = new javax.swing.JLabel();
        jLabel203 = new javax.swing.JLabel();
        jLabel204 = new javax.swing.JLabel();
        jLabel205 = new javax.swing.JLabel();
        jLabel206 = new javax.swing.JLabel();
        jLabel207 = new javax.swing.JLabel();
        jLabel208 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel216 = new javax.swing.JLabel();
        jLabel215 = new javax.swing.JLabel();
        jLabel211 = new javax.swing.JLabel();
        jLabel209 = new javax.swing.JLabel();
        jLabel217 = new javax.swing.JLabel();
        jLabel210 = new javax.swing.JLabel();
        jLabel212 = new javax.swing.JLabel();
        jLabel214 = new javax.swing.JLabel();
        jLabel213 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel223 = new javax.swing.JLabel();
        jLabel98 = new javax.swing.JLabel();
        jLabel95 = new javax.swing.JLabel();
        jLabel96 = new javax.swing.JLabel();
        jLabel100 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jLabel97 = new javax.swing.JLabel();
        jLabel99 = new javax.swing.JLabel();
        jLabel221 = new javax.swing.JLabel();
        jLabel76 = new javax.swing.JLabel();
        jLabel219 = new javax.swing.JLabel();
        jLabel220 = new javax.swing.JLabel();
        jLabel222 = new javax.swing.JLabel();
        jLabel225 = new javax.swing.JLabel();
        jLabel226 = new javax.swing.JLabel();
        jLabel218 = new javax.swing.JLabel();
        jLabel224 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel227 = new javax.swing.JLabel();
        jLabel91 = new javax.swing.JLabel();
        jLabel228 = new javax.swing.JLabel();
        jLabel229 = new javax.swing.JLabel();
        jLabel230 = new javax.swing.JLabel();
        jLabel231 = new javax.swing.JLabel();
        jLabel232 = new javax.swing.JLabel();
        jLabel233 = new javax.swing.JLabel();
        jLabel234 = new javax.swing.JLabel();
        jLabel235 = new javax.swing.JLabel();
        jLabel88 = new javax.swing.JLabel();
        jLabel94 = new javax.swing.JLabel();
        jLabel87 = new javax.swing.JLabel();
        jLabel90 = new javax.swing.JLabel();
        jLabel86 = new javax.swing.JLabel();
        jLabel93 = new javax.swing.JLabel();
        jLabel92 = new javax.swing.JLabel();
        jLabel89 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel236 = new javax.swing.JLabel();
        jLabel237 = new javax.swing.JLabel();
        jLabel238 = new javax.swing.JLabel();
        jLabel239 = new javax.swing.JLabel();
        jLabel240 = new javax.swing.JLabel();
        jLabel241 = new javax.swing.JLabel();
        jLabel242 = new javax.swing.JLabel();
        jLabel243 = new javax.swing.JLabel();
        jLabel244 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        jLabel77 = new javax.swing.JLabel();
        jLabel83 = new javax.swing.JLabel();
        jLabel85 = new javax.swing.JLabel();
        jLabel79 = new javax.swing.JLabel();
        jLabel84 = new javax.swing.JLabel();
        jLabel78 = new javax.swing.JLabel();
        jLabel81 = new javax.swing.JLabel();
        jLabel82 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel131 = new javax.swing.JLabel();
        jLabel111 = new javax.swing.JLabel();
        jLabel135 = new javax.swing.JLabel();
        jLabel114 = new javax.swing.JLabel();
        jLabel119 = new javax.swing.JLabel();
        jLabel245 = new javax.swing.JLabel();
        jLabel246 = new javax.swing.JLabel();
        jLabel123 = new javax.swing.JLabel();
        jLabel247 = new javax.swing.JLabel();
        jLabel101 = new javax.swing.JLabel();
        jLabel248 = new javax.swing.JLabel();
        jLabel249 = new javax.swing.JLabel();
        jLabel103 = new javax.swing.JLabel();
        jLabel250 = new javax.swing.JLabel();
        jLabel251 = new javax.swing.JLabel();
        jLabel252 = new javax.swing.JLabel();
        jLabel253 = new javax.swing.JLabel();
        jLabel109 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel130 = new javax.swing.JLabel();
        jLabel133 = new javax.swing.JLabel();
        jLabel112 = new javax.swing.JLabel();
        jLabel116 = new javax.swing.JLabel();
        jLabel117 = new javax.swing.JLabel();
        jLabel121 = new javax.swing.JLabel();
        jLabel125 = new javax.swing.JLabel();
        jLabel254 = new javax.swing.JLabel();
        jLabel105 = new javax.swing.JLabel();
        jLabel255 = new javax.swing.JLabel();
        jLabel107 = new javax.swing.JLabel();
        jLabel256 = new javax.swing.JLabel();
        jLabel257 = new javax.swing.JLabel();
        jLabel258 = new javax.swing.JLabel();
        jLabel259 = new javax.swing.JLabel();
        jLabel260 = new javax.swing.JLabel();
        jLabel261 = new javax.swing.JLabel();
        jLabel262 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel110 = new javax.swing.JLabel();
        jLabel113 = new javax.swing.JLabel();
        jLabel118 = new javax.swing.JLabel();
        jLabel120 = new javax.swing.JLabel();
        jLabel122 = new javax.swing.JLabel();
        jLabel124 = new javax.swing.JLabel();
        jLabel126 = new javax.swing.JLabel();
        jLabel104 = new javax.swing.JLabel();
        jLabel127 = new javax.swing.JLabel();
        jLabel263 = new javax.swing.JLabel();
        jLabel264 = new javax.swing.JLabel();
        jLabel265 = new javax.swing.JLabel();
        jLabel266 = new javax.swing.JLabel();
        jLabel267 = new javax.swing.JLabel();
        jLabel268 = new javax.swing.JLabel();
        jLabel269 = new javax.swing.JLabel();
        jLabel270 = new javax.swing.JLabel();
        jLabel271 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jLabel132 = new javax.swing.JLabel();
        jLabel134 = new javax.swing.JLabel();
        jLabel136 = new javax.swing.JLabel();
        jLabel115 = new javax.swing.JLabel();
        jLabel102 = new javax.swing.JLabel();
        jLabel106 = new javax.swing.JLabel();
        jLabel128 = new javax.swing.JLabel();
        jLabel129 = new javax.swing.JLabel();
        jLabel108 = new javax.swing.JLabel();
        jLabel272 = new javax.swing.JLabel();
        jLabel273 = new javax.swing.JLabel();
        jLabel274 = new javax.swing.JLabel();
        jLabel275 = new javax.swing.JLabel();
        jLabel276 = new javax.swing.JLabel();
        jLabel277 = new javax.swing.JLabel();
        jLabel278 = new javax.swing.JLabel();
        jLabel279 = new javax.swing.JLabel();
        jLabel280 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel290 = new javax.swing.JLabel();
        jLabel291 = new javax.swing.JLabel();
        jLabel292 = new javax.swing.JLabel();
        jLabel293 = new javax.swing.JLabel();
        jLabel294 = new javax.swing.JLabel();
        jLabel295 = new javax.swing.JLabel();
        jLabel296 = new javax.swing.JLabel();
        jLabel297 = new javax.swing.JLabel();
        jLabel298 = new javax.swing.JLabel();

        jPanel17.setBackground(new java.awt.Color(204, 102, 50));

        jLabel137.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel137.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel137MousePressed(evt);
            }
        });

        jLabel138.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel138.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel138MousePressed(evt);
            }
        });

        jLabel139.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel139.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel139MousePressed(evt);
            }
        });

        jLabel140.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel140.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel140MousePressed(evt);
            }
        });

        jLabel141.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel141.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel141MousePressed(evt);
            }
        });

        jLabel142.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel142.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel142MousePressed(evt);
            }
        });

        jLabel143.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel143.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel143MousePressed(evt);
            }
        });

        jLabel144.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel144.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel144MousePressed(evt);
            }
        });

        jLabel145.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel145.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel145MousePressed(evt);
            }
        });

        jLabel281.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel281.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        jLabel282.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel282.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        jLabel283.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel283.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        jLabel284.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel284.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        jLabel285.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel285.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        jLabel286.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel286.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        jLabel287.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel287.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        jLabel288.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel288.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        jLabel289.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel289.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel142)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel139)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel143)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel137)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel138)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel141)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel145)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel140)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel144)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel281)
                    .addComponent(jLabel282))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel284)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel286)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel288))
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel283)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel285)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel287)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel289)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel142)
                .addComponent(jLabel139)
                .addComponent(jLabel143)
                .addComponent(jLabel137)
                .addComponent(jLabel138)
                .addComponent(jLabel141)
                .addComponent(jLabel145)
                .addComponent(jLabel140)
                .addComponent(jLabel144))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel17Layout.createSequentialGroup()
                    .addComponent(jLabel283)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel284))
                .addGroup(jPanel17Layout.createSequentialGroup()
                    .addComponent(jLabel281)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel282))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                    .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel285)
                        .addComponent(jLabel287)
                        .addComponent(jLabel289))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel286, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel288, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);

        jButton4.setBackground(new java.awt.Color(255, 255, 255));
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/4.png"))); // NOI18N
        jButton4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jButton4.setBorderPainted(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton6.setBackground(new java.awt.Color(255, 255, 255));
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/6.png"))); // NOI18N
        jButton6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jButton6.setBorderPainted(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/2.png"))); // NOI18N
        jButton2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        jButton2.setBorderPainted(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/1.png"))); // NOI18N
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jButton1.setBorderPainted(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setBackground(new java.awt.Color(255, 255, 255));
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/3.png"))); // NOI18N
        jButton3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jButton3.setBorderPainted(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton10.setText("¡Comprueba!");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton8.setBackground(new java.awt.Color(255, 255, 255));
        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/8.png"))); // NOI18N
        jButton8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jButton8.setBorderPainted(false);
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton5.setBackground(new java.awt.Color(255, 255, 255));
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/5.png"))); // NOI18N
        jButton5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jButton5.setBorderPainted(false);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton9.setBackground(new java.awt.Color(255, 255, 255));
        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/9.png"))); // NOI18N
        jButton9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jButton9.setBorderPainted(false);
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton7.setBackground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/7.png"))); // NOI18N
        jButton7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jButton7.setBorderPainted(false);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton11.setText("Guardar");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton12.setText("Salir");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton13.setText("Pista color");
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jButton14.setText("Pista posición aleatoria y color");
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        jButton15.setText("Pista posición dada y color");
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        jButton16.setText("¡Siguiente!");
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });

        jDesktopPane1.setLayer(jButton4, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton6, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton10, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton8, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton5, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton9, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton7, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton11, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton12, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton13, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton14, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton15, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(jButton16, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jDesktopPane1Layout.createSequentialGroup()
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDesktopPane1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton12))
        );

        jDesktopPane1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jButton10, jButton11, jButton12});

        jPanel1.setBackground(new java.awt.Color(204, 102, 0));

        jPanel2.setBackground(new java.awt.Color(204, 102, 50));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel2MousePressed(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel3MousePressed(evt);
            }
        });

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel5MousePressed(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel4MousePressed(evt);
            }
        });

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel9MousePressed(evt);
            }
        });

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel6MousePressed(evt);
            }
        });

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel8MousePressed(evt);
            }
        });

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel7MousePressed(evt);
            }
        });

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel10MousePressed(evt);
            }
        });

        jLabel164.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel164.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel164MousePressed(evt);
            }
        });

        jLabel165.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel165.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel165.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel165MousePressed(evt);
            }
        });

        jLabel167.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel167.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel167.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel167MousePressed(evt);
            }
        });

        jLabel166.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel166.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel166.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel166MousePressed(evt);
            }
        });

        jLabel169.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel169.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel169.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel169MousePressed(evt);
            }
        });

        jLabel168.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel168.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel168.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel168MousePressed(evt);
            }
        });

        jLabel170.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel170.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel170.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel170MousePressed(evt);
            }
        });

        jLabel171.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel171.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel171.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel171MousePressed(evt);
            }
        });

        jLabel172.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel172.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel172.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel172MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel164)
                    .addComponent(jLabel165))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel167)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel169)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel171))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel166)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel168)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel170)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel172)))
                .addGap(18, 18, 18))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel2)
                .addComponent(jLabel3)
                .addComponent(jLabel5)
                .addComponent(jLabel4)
                .addComponent(jLabel9)
                .addComponent(jLabel6)
                .addComponent(jLabel8)
                .addComponent(jLabel7)
                .addComponent(jLabel10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(jLabel166)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel167))
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(jLabel164)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel165))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel168)
                        .addComponent(jLabel170)
                        .addComponent(jLabel172))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel169, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel171, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel3.setBackground(new java.awt.Color(214, 102, 50));

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel12MousePressed(evt);
            }
        });

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel19MousePressed(evt);
            }
        });

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel17MousePressed(evt);
            }
        });

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel15MousePressed(evt);
            }
        });

        jLabel146.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel146.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel146.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel146MousePressed(evt);
            }
        });

        jLabel147.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel147.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel147.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel147MousePressed(evt);
            }
        });

        jLabel148.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel148.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel148.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel148MousePressed(evt);
            }
        });

        jLabel149.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel149.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel149.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel149MousePressed(evt);
            }
        });

        jLabel150.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel150.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel150.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel150MousePressed(evt);
            }
        });

        jLabel151.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel151.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel151.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel151MousePressed(evt);
            }
        });

        jLabel152.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel152.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel152.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel152MousePressed(evt);
            }
        });

        jLabel153.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel153.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel153.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel153MousePressed(evt);
            }
        });

        jLabel154.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel154.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel154.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel154MousePressed(evt);
            }
        });

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel14MousePressed(evt);
            }
        });

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel11MousePressed(evt);
            }
        });

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel13MousePressed(evt);
            }
        });

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel18MousePressed(evt);
            }
        });

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel16MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel146)
                    .addComponent(jLabel147))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel149)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel151)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel153))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel148)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel150)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel152)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel154)))
                .addGap(18, 18, 18))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel11)
                .addComponent(jLabel12)
                .addComponent(jLabel14)
                .addComponent(jLabel13)
                .addComponent(jLabel18)
                .addComponent(jLabel15)
                .addComponent(jLabel17)
                .addComponent(jLabel16)
                .addComponent(jLabel19))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addComponent(jLabel148)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel149))
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addComponent(jLabel146)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel147))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel150)
                        .addComponent(jLabel152)
                        .addComponent(jLabel154))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel151, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel153, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel4.setBackground(new java.awt.Color(204, 102, 50));

        jLabel37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel37.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel37MousePressed(evt);
            }
        });

        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel34.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel34MousePressed(evt);
            }
        });

        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel29MousePressed(evt);
            }
        });

        jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel32MousePressed(evt);
            }
        });

        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel30MousePressed(evt);
            }
        });

        jLabel155.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel155.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel155.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel155MousePressed(evt);
            }
        });

        jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel36.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel36MousePressed(evt);
            }
        });

        jLabel156.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel156.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel156.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel156MousePressed(evt);
            }
        });

        jLabel157.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel157.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel157.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel157MousePressed(evt);
            }
        });

        jLabel158.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel158.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel158.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel158MousePressed(evt);
            }
        });

        jLabel159.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel159.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel159.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel159MousePressed(evt);
            }
        });

        jLabel160.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel160.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel160.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel160MousePressed(evt);
            }
        });

        jLabel161.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel161.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel161.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel161MousePressed(evt);
            }
        });

        jLabel162.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel162.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel162.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel162MousePressed(evt);
            }
        });

        jLabel163.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel163.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel163.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel163MousePressed(evt);
            }
        });

        jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel35.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel35MousePressed(evt);
            }
        });

        jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel33.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel33MousePressed(evt);
            }
        });

        jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel31MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel29)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel32)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel36)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel33)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel35)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel34)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel37)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel155)
                    .addComponent(jLabel160))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel156)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel157)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel158))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel159)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel162)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel163)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel161)))
                .addGap(30, 30, 30))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel29)
                .addComponent(jLabel30)
                .addComponent(jLabel32)
                .addComponent(jLabel31)
                .addComponent(jLabel36)
                .addComponent(jLabel33)
                .addComponent(jLabel35)
                .addComponent(jLabel34)
                .addComponent(jLabel37))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addComponent(jLabel159)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel156))
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addComponent(jLabel155)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel160))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel162)
                        .addComponent(jLabel163)
                        .addComponent(jLabel161))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel157, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel158, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel5.setBackground(new java.awt.Color(214, 102, 50));

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel25MousePressed(evt);
            }
        });

        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel23MousePressed(evt);
            }
        });

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel20MousePressed(evt);
            }
        });

        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel28MousePressed(evt);
            }
        });

        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel26.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel26MousePressed(evt);
            }
        });

        jLabel173.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel173.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel173.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel173MousePressed(evt);
            }
        });

        jLabel174.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel174.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel174.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel174MousePressed(evt);
            }
        });

        jLabel175.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel175.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel175.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel175MousePressed(evt);
            }
        });

        jLabel176.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel176.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel176.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel176MousePressed(evt);
            }
        });

        jLabel177.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel177.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel177.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel177MousePressed(evt);
            }
        });

        jLabel178.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel178.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel178.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel178MousePressed(evt);
            }
        });

        jLabel179.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel179.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel179.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel179MousePressed(evt);
            }
        });

        jLabel180.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel180.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel180.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel180MousePressed(evt);
            }
        });

        jLabel181.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel181.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel181.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel181MousePressed(evt);
            }
        });

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel27MousePressed(evt);
            }
        });

        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel22MousePressed(evt);
            }
        });

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel24MousePressed(evt);
            }
        });

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel21MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel27)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel28)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel173)
                    .addComponent(jLabel174))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel176)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel178)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel180))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel175)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel177)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel179)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel181)))
                .addGap(18, 18, 18))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel20)
            .addComponent(jLabel21)
            .addComponent(jLabel23)
            .addComponent(jLabel22)
            .addComponent(jLabel27)
            .addComponent(jLabel24)
            .addComponent(jLabel26)
            .addComponent(jLabel25)
            .addComponent(jLabel28)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addComponent(jLabel175)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel176))
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addComponent(jLabel173)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel174))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel177)
                        .addComponent(jLabel179)
                        .addComponent(jLabel181))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel178, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel180, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel6.setBackground(new java.awt.Color(204, 102, 50));

        jLabel73.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel73.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel73MousePressed(evt);
            }
        });

        jLabel70.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel70.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel70MousePressed(evt);
            }
        });

        jLabel71.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel71.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel71MousePressed(evt);
            }
        });

        jLabel62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel62.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel62MousePressed(evt);
            }
        });

        jLabel182.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel182.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel182.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel182MousePressed(evt);
            }
        });

        jLabel183.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel183.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel183.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel183MousePressed(evt);
            }
        });

        jLabel184.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel184.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel184.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel184MousePressed(evt);
            }
        });

        jLabel185.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel185.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel185.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel185MousePressed(evt);
            }
        });

        jLabel69.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel69.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel69MousePressed(evt);
            }
        });

        jLabel63.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel63.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel63MousePressed(evt);
            }
        });

        jLabel72.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel72.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel72MousePressed(evt);
            }
        });

        jLabel64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel64.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel64MousePressed(evt);
            }
        });

        jLabel186.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel186.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel186.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel186MousePressed(evt);
            }
        });

        jLabel187.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel187.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel187.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel187MousePressed(evt);
            }
        });

        jLabel188.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel188.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel188.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel188MousePressed(evt);
            }
        });

        jLabel189.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel189.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel189.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel189MousePressed(evt);
            }
        });

        jLabel190.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel190.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel190.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel190MousePressed(evt);
            }
        });

        jLabel51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel51.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel51MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel51)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel62)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel64)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel63)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel72)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel69)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel71)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel70)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel73)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel182)
                    .addComponent(jLabel183))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel185)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel187)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel189))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel184)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel186)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel188)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel190)))
                .addGap(18, 18, 18))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addComponent(jLabel184)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel185))
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addComponent(jLabel182)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel183))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel186)
                        .addComponent(jLabel188)
                        .addComponent(jLabel190))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel187, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel189, javax.swing.GroupLayout.Alignment.TRAILING))))
            .addComponent(jLabel51)
            .addComponent(jLabel62)
            .addComponent(jLabel64)
            .addComponent(jLabel63)
            .addComponent(jLabel72)
            .addComponent(jLabel69)
            .addComponent(jLabel71)
            .addComponent(jLabel70)
            .addComponent(jLabel73)
        );

        jPanel7.setBackground(new java.awt.Color(214, 102, 50));

        jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel44MousePressed(evt);
            }
        });

        jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel41.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel41MousePressed(evt);
            }
        });

        jLabel46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel46.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel46MousePressed(evt);
            }
        });

        jLabel38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel38.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel38MousePressed(evt);
            }
        });

        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel42.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel42MousePressed(evt);
            }
        });

        jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel40MousePressed(evt);
            }
        });

        jLabel191.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel191.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel191.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel191MousePressed(evt);
            }
        });

        jLabel192.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel192.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel192.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel192MousePressed(evt);
            }
        });

        jLabel193.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel193.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel193.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel193MousePressed(evt);
            }
        });

        jLabel194.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel194.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel194.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel194MousePressed(evt);
            }
        });

        jLabel195.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel195.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel195.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel195MousePressed(evt);
            }
        });

        jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel43.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel43MousePressed(evt);
            }
        });

        jLabel39.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel39.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel39MousePressed(evt);
            }
        });

        jLabel196.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel196.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel196.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel196MousePressed(evt);
            }
        });

        jLabel197.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel197.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel197.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel197MousePressed(evt);
            }
        });

        jLabel198.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel198.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel198.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel198MousePressed(evt);
            }
        });

        jLabel199.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel199.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel199.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel199MousePressed(evt);
            }
        });

        jLabel45.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel45.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel45MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel38)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel39)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel41)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel45)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel42)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel44)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel43)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel46)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel191)
                    .addComponent(jLabel192))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel194)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel196)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel198))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel193)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel195)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel197)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel199)))
                .addGap(18, 18, 18))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel38)
            .addComponent(jLabel39)
            .addComponent(jLabel41)
            .addComponent(jLabel40)
            .addComponent(jLabel45)
            .addComponent(jLabel42)
            .addComponent(jLabel44)
            .addComponent(jLabel43)
            .addComponent(jLabel46)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addComponent(jLabel193)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel194))
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addComponent(jLabel191)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel192))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel195)
                        .addComponent(jLabel197)
                        .addComponent(jLabel199))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel196, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel198, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel8.setBackground(new java.awt.Color(204, 102, 50));

        jLabel58.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel58.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel58MousePressed(evt);
            }
        });

        jLabel60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel60.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel60MousePressed(evt);
            }
        });

        jLabel59.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel59.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel59MousePressed(evt);
            }
        });

        jLabel57.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel57.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel57MousePressed(evt);
            }
        });

        jLabel68.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel68.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel68MousePressed(evt);
            }
        });

        jLabel67.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel67.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel67MousePressed(evt);
            }
        });

        jLabel65.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel65.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel65MousePressed(evt);
            }
        });

        jLabel66.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel66.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel66MousePressed(evt);
            }
        });

        jLabel61.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel61.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel61MousePressed(evt);
            }
        });

        jLabel200.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel200.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel200.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel200MousePressed(evt);
            }
        });

        jLabel201.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel201.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel201.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel201MousePressed(evt);
            }
        });

        jLabel202.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel202.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel202.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel202MousePressed(evt);
            }
        });

        jLabel203.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel203.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel203.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel203MousePressed(evt);
            }
        });

        jLabel204.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel204.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel204.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel204MousePressed(evt);
            }
        });

        jLabel205.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel205.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel205.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel205MousePressed(evt);
            }
        });

        jLabel206.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel206.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel206.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel206MousePressed(evt);
            }
        });

        jLabel207.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel207.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel207.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel207MousePressed(evt);
            }
        });

        jLabel208.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel208.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel208.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel208MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel57)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel58)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel60)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel59)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel67)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel61)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel66)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel65)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel68)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel200)
                    .addComponent(jLabel201))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel203)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel205)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel207))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel202)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel204)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel206)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel208)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel57)
                .addComponent(jLabel58)
                .addComponent(jLabel60)
                .addComponent(jLabel59)
                .addComponent(jLabel67)
                .addComponent(jLabel61)
                .addComponent(jLabel66)
                .addComponent(jLabel65)
                .addComponent(jLabel68))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addComponent(jLabel202)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel203))
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addComponent(jLabel200)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel201))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel204)
                        .addComponent(jLabel206)
                        .addComponent(jLabel208))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel205, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel207, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel9.setBackground(new java.awt.Color(214, 102, 50));

        jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel52.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel52MousePressed(evt);
            }
        });

        jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel53.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel53MousePressed(evt);
            }
        });

        jLabel56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel56.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel56MousePressed(evt);
            }
        });

        jLabel47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel47.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel47MousePressed(evt);
            }
        });

        jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel54.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel54MousePressed(evt);
            }
        });

        jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel55.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel55MousePressed(evt);
            }
        });

        jLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel50.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel50MousePressed(evt);
            }
        });

        jLabel48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel48.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel48MousePressed(evt);
            }
        });

        jLabel49.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel49.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel49MousePressed(evt);
            }
        });

        jLabel216.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel216.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel216.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel216MousePressed(evt);
            }
        });

        jLabel215.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel215.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel215.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel215MousePressed(evt);
            }
        });

        jLabel211.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel211.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel211.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel211MousePressed(evt);
            }
        });

        jLabel209.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel209.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel209.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel209MousePressed(evt);
            }
        });

        jLabel217.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel217.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel217.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel217MousePressed(evt);
            }
        });

        jLabel210.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel210.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel210MousePressed(evt);
            }
        });

        jLabel212.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel212.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel212.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel212MousePressed(evt);
            }
        });

        jLabel214.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel214.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel214.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel214MousePressed(evt);
            }
        });

        jLabel213.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel213.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel213.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel213MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel47)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel48)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel50)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel49)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel55)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel52)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel54)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel53)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel56)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel209)
                    .addComponent(jLabel210))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel212)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel214)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel216))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel211)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel213)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel215)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel217)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel47)
                        .addComponent(jLabel48)
                        .addComponent(jLabel50)
                        .addComponent(jLabel49)
                        .addComponent(jLabel55)
                        .addComponent(jLabel52)
                        .addComponent(jLabel54)
                        .addComponent(jLabel53)
                        .addComponent(jLabel56))
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel9Layout.createSequentialGroup()
                            .addComponent(jLabel211)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel212))
                        .addGroup(jPanel9Layout.createSequentialGroup()
                            .addComponent(jLabel209)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel210))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel213)
                                .addComponent(jLabel215)
                                .addComponent(jLabel217))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel214, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel216, javax.swing.GroupLayout.Alignment.TRAILING)))))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(204, 102, 50));

        jLabel223.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel223.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel223.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel223MousePressed(evt);
            }
        });

        jLabel98.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel98.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel98MousePressed(evt);
            }
        });

        jLabel95.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel95.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel95MousePressed(evt);
            }
        });

        jLabel96.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel96.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel96MousePressed(evt);
            }
        });

        jLabel100.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel100.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel100MousePressed(evt);
            }
        });

        jLabel75.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel75.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel75MousePressed(evt);
            }
        });

        jLabel74.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel74.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel74MousePressed(evt);
            }
        });

        jLabel97.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel97.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel97MousePressed(evt);
            }
        });

        jLabel99.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel99.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel99MousePressed(evt);
            }
        });

        jLabel221.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel221.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel221.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel221MousePressed(evt);
            }
        });

        jLabel76.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel76.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel76MousePressed(evt);
            }
        });

        jLabel219.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel219.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel219.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel219MousePressed(evt);
            }
        });

        jLabel220.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel220.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel220.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel220MousePressed(evt);
            }
        });

        jLabel222.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel222.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel222.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel222MousePressed(evt);
            }
        });

        jLabel225.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel225.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel225.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel225MousePressed(evt);
            }
        });

        jLabel226.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel226.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel226.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel226MousePressed(evt);
            }
        });

        jLabel218.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel218.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel218.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel218MousePressed(evt);
            }
        });

        jLabel224.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel224.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel224.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel224MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel95)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel96)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel98)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel97)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel75)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel99)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel74)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel100)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel76)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel218)
                    .addComponent(jLabel219))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel221)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel223)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel225))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel220)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel222)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel224)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel226)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel95)
                .addComponent(jLabel96)
                .addComponent(jLabel98)
                .addComponent(jLabel97)
                .addComponent(jLabel75)
                .addComponent(jLabel99)
                .addComponent(jLabel74)
                .addComponent(jLabel100)
                .addComponent(jLabel76))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel10Layout.createSequentialGroup()
                    .addComponent(jLabel220)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel221))
                .addGroup(jPanel10Layout.createSequentialGroup()
                    .addComponent(jLabel218)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel219))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel222)
                        .addComponent(jLabel224)
                        .addComponent(jLabel226))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel223, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel225, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel11.setBackground(new java.awt.Color(214, 102, 50));

        jLabel227.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel227.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel227.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel227MousePressed(evt);
            }
        });

        jLabel91.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel91.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel91MousePressed(evt);
            }
        });

        jLabel228.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel228.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel228.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel228MousePressed(evt);
            }
        });

        jLabel229.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel229.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel229.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel229MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabel229MouseReleased(evt);
            }
        });

        jLabel230.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel230.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel230.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel230MousePressed(evt);
            }
        });

        jLabel231.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel231.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel231.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel231MousePressed(evt);
            }
        });

        jLabel232.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel232.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel232.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel232MousePressed(evt);
            }
        });

        jLabel233.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel233.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel233.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel233MousePressed(evt);
            }
        });

        jLabel234.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel234.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel234.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel234MousePressed(evt);
            }
        });

        jLabel235.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel235.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel235.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel235MousePressed(evt);
            }
        });

        jLabel88.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel88.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel88MousePressed(evt);
            }
        });

        jLabel94.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel94.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel94MousePressed(evt);
            }
        });

        jLabel87.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel87.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel87MousePressed(evt);
            }
        });

        jLabel90.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel90.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel90MousePressed(evt);
            }
        });

        jLabel86.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel86.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel86MousePressed(evt);
            }
        });

        jLabel93.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel93.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel93MousePressed(evt);
            }
        });

        jLabel92.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel92.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel92MousePressed(evt);
            }
        });

        jLabel89.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel89.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel89MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel86)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel87)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel89)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel88)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel93)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel90)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel92)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel91)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel94)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel227)
                    .addComponent(jLabel228))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel230)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel232)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel234))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel229)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel231)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel233)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel235)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel86)
                .addComponent(jLabel87)
                .addComponent(jLabel89)
                .addComponent(jLabel88)
                .addComponent(jLabel93)
                .addComponent(jLabel90)
                .addComponent(jLabel92)
                .addComponent(jLabel91)
                .addComponent(jLabel94))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel11Layout.createSequentialGroup()
                    .addComponent(jLabel229)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel230))
                .addGroup(jPanel11Layout.createSequentialGroup()
                    .addComponent(jLabel227)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel228))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel231)
                        .addComponent(jLabel233)
                        .addComponent(jLabel235))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel232, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel234, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel12.setBackground(new java.awt.Color(204, 102, 50));

        jLabel236.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel236.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel236.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel236MousePressed(evt);
            }
        });

        jLabel237.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel237.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel237.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel237MousePressed(evt);
            }
        });

        jLabel238.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel238.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel238.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel238MousePressed(evt);
            }
        });

        jLabel239.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel239.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel239.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel239MousePressed(evt);
            }
        });

        jLabel240.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel240.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel240.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel240MousePressed(evt);
            }
        });

        jLabel241.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel241.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel241.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel241MousePressed(evt);
            }
        });

        jLabel242.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel242.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel242.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel242MousePressed(evt);
            }
        });

        jLabel243.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel243.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel243.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel243MousePressed(evt);
            }
        });

        jLabel244.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel244.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel244.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel244MousePressed(evt);
            }
        });

        jLabel80.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel80.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel80MousePressed(evt);
            }
        });

        jLabel77.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel77.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel77MousePressed(evt);
            }
        });

        jLabel83.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel83.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel83MousePressed(evt);
            }
        });

        jLabel85.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel85.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel85MousePressed(evt);
            }
        });

        jLabel79.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel79.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel79MousePressed(evt);
            }
        });

        jLabel84.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel84.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel84MousePressed(evt);
            }
        });

        jLabel78.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel78.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel78MousePressed(evt);
            }
        });

        jLabel81.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel81.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel81MousePressed(evt);
            }
        });

        jLabel82.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel82.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel82MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel77)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel78)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel80)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel79)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel84)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel81)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel83)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel82)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel85)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel236)
                    .addComponent(jLabel237))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel239)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel241)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel243))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel238)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel240)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel242)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel244)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel77)
                .addComponent(jLabel78)
                .addComponent(jLabel80)
                .addComponent(jLabel79)
                .addComponent(jLabel84)
                .addComponent(jLabel81)
                .addComponent(jLabel83)
                .addComponent(jLabel82)
                .addComponent(jLabel85))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel12Layout.createSequentialGroup()
                    .addComponent(jLabel238)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel239))
                .addGroup(jPanel12Layout.createSequentialGroup()
                    .addComponent(jLabel236)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel237))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel240)
                        .addComponent(jLabel242)
                        .addComponent(jLabel244))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel241, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel243, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel13.setBackground(new java.awt.Color(214, 102, 50));

        jLabel131.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel131.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel131MousePressed(evt);
            }
        });

        jLabel111.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel111.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel111MousePressed(evt);
            }
        });

        jLabel135.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel135.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel135MousePressed(evt);
            }
        });

        jLabel114.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel114.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel114MousePressed(evt);
            }
        });

        jLabel119.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel119.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel119MousePressed(evt);
            }
        });

        jLabel245.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel245.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel245.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel245MousePressed(evt);
            }
        });

        jLabel246.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel246.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel246.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel246MousePressed(evt);
            }
        });

        jLabel123.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel123.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel123MousePressed(evt);
            }
        });

        jLabel247.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel247.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel247.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel247MousePressed(evt);
            }
        });

        jLabel101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel101MousePressed(evt);
            }
        });

        jLabel248.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel248.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel248.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel248MousePressed(evt);
            }
        });

        jLabel249.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel249.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel249.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel249MousePressed(evt);
            }
        });

        jLabel103.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel103.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel103MousePressed(evt);
            }
        });

        jLabel250.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel250.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel250.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel250MousePressed(evt);
            }
        });

        jLabel251.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel251.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel251.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel251MousePressed(evt);
            }
        });

        jLabel252.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel252.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel252.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel252MousePressed(evt);
            }
        });

        jLabel253.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel253.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel253.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel253MousePressed(evt);
            }
        });

        jLabel109.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel109.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel109MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel131)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel114)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel111)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel119)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel109)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel123)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel103)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel135)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel101)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel245)
                    .addComponent(jLabel246))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel248)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel250)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel252))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel247)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel249)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel251)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel253)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel114)
                .addComponent(jLabel111)
                .addComponent(jLabel119)
                .addComponent(jLabel109)
                .addComponent(jLabel123)
                .addComponent(jLabel103)
                .addComponent(jLabel135)
                .addComponent(jLabel101)
                .addComponent(jLabel131))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel13Layout.createSequentialGroup()
                    .addComponent(jLabel247)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel248))
                .addGroup(jPanel13Layout.createSequentialGroup()
                    .addComponent(jLabel245)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel246))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel249)
                        .addComponent(jLabel251)
                        .addComponent(jLabel253))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel250, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel252, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel14.setBackground(new java.awt.Color(204, 102, 50));

        jLabel130.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel130.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel130MousePressed(evt);
            }
        });

        jLabel133.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel133.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel133MousePressed(evt);
            }
        });

        jLabel112.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel112.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel112MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel112MousePressed(evt);
            }
        });

        jLabel116.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel116.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel116MousePressed(evt);
            }
        });

        jLabel117.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel117.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel117MousePressed(evt);
            }
        });

        jLabel121.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel121.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel121MousePressed(evt);
            }
        });

        jLabel125.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel125.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel125MousePressed(evt);
            }
        });

        jLabel254.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel254.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel254.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel254MousePressed(evt);
            }
        });

        jLabel105.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel105.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel105MousePressed(evt);
            }
        });

        jLabel255.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel255.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel255.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel255MousePressed(evt);
            }
        });

        jLabel107.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel107.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel107MousePressed(evt);
            }
        });

        jLabel256.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel256.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel256.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel256MousePressed(evt);
            }
        });

        jLabel257.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel257.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel257.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel257MousePressed(evt);
            }
        });

        jLabel258.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel258.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel258.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel258MousePressed(evt);
            }
        });

        jLabel259.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel259.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel259.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel259MousePressed(evt);
            }
        });

        jLabel260.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel260.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel260.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel260MousePressed(evt);
            }
        });

        jLabel261.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel261.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel261.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel261MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel261MousePressed(evt);
            }
        });

        jLabel262.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel262.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel262.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel262MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel105)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel130)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel125)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel116)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel107)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel117)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel112)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel133)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel121)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel254)
                    .addComponent(jLabel255))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel257)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel259)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel261))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel256)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel258)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel260)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel262)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel105)
                        .addComponent(jLabel130)
                        .addComponent(jLabel125)
                        .addComponent(jLabel116)
                        .addComponent(jLabel107)
                        .addComponent(jLabel117)
                        .addComponent(jLabel112)
                        .addComponent(jLabel133)
                        .addComponent(jLabel121))
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel14Layout.createSequentialGroup()
                            .addComponent(jLabel256)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel257))
                        .addGroup(jPanel14Layout.createSequentialGroup()
                            .addComponent(jLabel254)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel255))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel258)
                                .addComponent(jLabel260)
                                .addComponent(jLabel262))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel259, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel261, javax.swing.GroupLayout.Alignment.TRAILING)))))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel15.setBackground(new java.awt.Color(214, 102, 50));

        jLabel110.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel110.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel110MousePressed(evt);
            }
        });

        jLabel113.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel113.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel113MousePressed(evt);
            }
        });

        jLabel118.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel118.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel118MousePressed(evt);
            }
        });

        jLabel120.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel120.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel120MousePressed(evt);
            }
        });

        jLabel122.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel122.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabel122MouseReleased(evt);
            }
        });

        jLabel124.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel124.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel124MousePressed(evt);
            }
        });

        jLabel126.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel126.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel126MousePressed(evt);
            }
        });

        jLabel104.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel104.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel104MousePressed(evt);
            }
        });

        jLabel127.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel127.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel127MousePressed(evt);
            }
        });

        jLabel263.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel263.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel263.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel263MousePressed(evt);
            }
        });

        jLabel264.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel264.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel264.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel264MousePressed(evt);
            }
        });

        jLabel265.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel265.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel265.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel265MousePressed(evt);
            }
        });

        jLabel266.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel266.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel266.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel266MousePressed(evt);
            }
        });

        jLabel267.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel267.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel267.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel267MousePressed(evt);
            }
        });

        jLabel268.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel268.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel268.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel268MousePressed(evt);
            }
        });

        jLabel269.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel269.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel269.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel269MousePressed(evt);
            }
        });

        jLabel270.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel270.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel270.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel270MousePressed(evt);
            }
        });

        jLabel271.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel271.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel271.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel271MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel110)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel127)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel120)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel124)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel113)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel104)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel118)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel122)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel126)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel263)
                    .addComponent(jLabel264))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel266)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel268)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel270))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel265)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel267)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel269)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel271)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel110)
                .addComponent(jLabel127)
                .addComponent(jLabel120)
                .addComponent(jLabel124)
                .addComponent(jLabel113)
                .addComponent(jLabel104)
                .addComponent(jLabel118)
                .addComponent(jLabel122)
                .addComponent(jLabel126))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel15Layout.createSequentialGroup()
                    .addComponent(jLabel265)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel266))
                .addGroup(jPanel15Layout.createSequentialGroup()
                    .addComponent(jLabel263)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel264))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel267)
                        .addComponent(jLabel269)
                        .addComponent(jLabel271))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel268, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel270, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel16.setBackground(new java.awt.Color(204, 102, 50));

        jLabel132.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel132.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel132MousePressed(evt);
            }
        });

        jLabel134.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel134.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel134MousePressed(evt);
            }
        });

        jLabel136.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel136.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel136MousePressed(evt);
            }
        });

        jLabel115.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel115.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel115MousePressed(evt);
            }
        });

        jLabel102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel102MousePressed(evt);
            }
        });

        jLabel106.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel106.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel106MousePressed(evt);
            }
        });

        jLabel128.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel128.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel128MousePressed(evt);
            }
        });

        jLabel129.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel129.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel129MousePressed(evt);
            }
        });

        jLabel108.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel108.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel108MousePressed(evt);
            }
        });

        jLabel272.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel272.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel272.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel272MousePressed(evt);
            }
        });

        jLabel273.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel273.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel273.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel273MousePressed(evt);
            }
        });

        jLabel274.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel274.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel274.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel274MousePressed(evt);
            }
        });

        jLabel275.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel275.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel275.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel275MousePressed(evt);
            }
        });

        jLabel276.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel276.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel276.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel276MousePressed(evt);
            }
        });

        jLabel277.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel277.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel277.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel277MousePressed(evt);
            }
        });

        jLabel278.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel278.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel278.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel278MousePressed(evt);
            }
        });

        jLabel279.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel279.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel279.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel279MousePressed(evt);
            }
        });

        jLabel280.setFont(new java.awt.Font("Tahoma", 0, 5)); // NOI18N
        jLabel280.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/negre_9.png"))); // NOI18N
        jLabel280.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel280MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel106)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel136)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel128)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel132)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel134)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel102)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel108)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel115)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel129)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel272)
                    .addComponent(jLabel273))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel275)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel277)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel279))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel274)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel276)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel278)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel280)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel106)
                .addComponent(jLabel136)
                .addComponent(jLabel128)
                .addComponent(jLabel132)
                .addComponent(jLabel134)
                .addComponent(jLabel102)
                .addComponent(jLabel108)
                .addComponent(jLabel115)
                .addComponent(jLabel129))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel16Layout.createSequentialGroup()
                    .addComponent(jLabel274)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel275))
                .addGroup(jPanel16Layout.createSequentialGroup()
                    .addComponent(jLabel272)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel273))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel276)
                        .addComponent(jLabel278)
                        .addComponent(jLabel280))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel277, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel279, javax.swing.GroupLayout.Alignment.TRAILING))))
        );

        jPanel18.setBackground(new java.awt.Color(153, 153, 153));

        jLabel290.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel290.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel290MousePressed(evt);
            }
        });

        jLabel291.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel291.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel291MousePressed(evt);
            }
        });

        jLabel292.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel292.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel292MousePressed(evt);
            }
        });

        jLabel293.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel293.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel293MousePressed(evt);
            }
        });

        jLabel294.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel294.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel294MousePressed(evt);
            }
        });

        jLabel295.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel295.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel295MousePressed(evt);
            }
        });

        jLabel296.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel296.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel296MousePressed(evt);
            }
        });

        jLabel297.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel297.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel297MousePressed(evt);
            }
        });

        jLabel298.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/marro_25.png"))); // NOI18N
        jLabel298.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel298MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel295)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel292)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel296)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel290)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel291)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel294)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel298)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel293)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel297)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel295)
                    .addComponent(jLabel292)
                    .addComponent(jLabel296)
                    .addComponent(jLabel290)
                    .addComponent(jLabel291)
                    .addComponent(jLabel294)
                    .addComponent(jLabel298)
                    .addComponent(jLabel293)
                    .addComponent(jLabel297))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MousePressed
        if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel2MousePressed

    private void jLabel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MousePressed
        if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel3MousePressed

    private void jLabel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MousePressed
         if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel5MousePressed

    private void jLabel4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MousePressed
        if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel4MousePressed

    private void jLabel9MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MousePressed
         if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel9MousePressed

    private void jLabel6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MousePressed
         if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel6MousePressed

    private void jLabel8MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MousePressed
         if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel8MousePressed

    private void jLabel7MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MousePressed
         if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel7MousePressed

    private void jLabel10MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MousePressed
          if (fila_actual == 1 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel10MousePressed

    private void jLabel11MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MousePressed
         if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel11MousePressed

    private void jLabel12MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MousePressed
         if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel12MousePressed

    private void jLabel14MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel14MousePressed
        if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel14MousePressed

    private void jLabel13MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MousePressed
         if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel13MousePressed

    private void jLabel18MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MousePressed
        if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel18MousePressed

    private void jLabel15MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel15MousePressed
         if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel15MousePressed

    private void jLabel17MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MousePressed
         if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel17MousePressed

    private void jLabel16MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MousePressed
         if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel16MousePressed

    private void jLabel19MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MousePressed
         if (fila_actual == 2 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel19MousePressed

    private void jLabel29MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel29MousePressed
         if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel29MousePressed

    private void jLabel30MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MousePressed
         if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel30MousePressed

    private void jLabel32MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel32MousePressed
         if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel32MousePressed

    private void jLabel31MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel31MousePressed
         if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel31MousePressed

    private void jLabel36MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel36MousePressed
        if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel36MousePressed

    private void jLabel33MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel33MousePressed
        if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel33MousePressed

    private void jLabel35MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel35MousePressed
         if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel35MousePressed

    private void jLabel34MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel34MousePressed
         if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel34MousePressed

    private void jLabel37MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel37MousePressed
        if (fila_actual == 3 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel37.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel37MousePressed

    private void jLabel20MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MousePressed
         if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel20MousePressed

    private void jLabel21MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MousePressed
         if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel21MousePressed

    private void jLabel23MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel23MousePressed
         if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel23MousePressed

    private void jLabel22MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel22MousePressed
          if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel22MousePressed

    private void jLabel27MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel27MousePressed
         if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel27MousePressed

    private void jLabel24MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel24MousePressed
         if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel24MousePressed

    private void jLabel26MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel26MousePressed
         if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel26MousePressed

    private void jLabel25MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel25MousePressed
         if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel25MousePressed

    private void jLabel28MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel28MousePressed
        if (fila_actual == 4 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel28MousePressed

    private void jLabel51MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel51MousePressed
         if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel51.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel51MousePressed

    private void jLabel62MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel62MousePressed
         if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel62.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel62MousePressed

    private void jLabel64MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel64MousePressed
         if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel64.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel64MousePressed

    private void jLabel63MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel63MousePressed
         if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel63.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel63MousePressed

    private void jLabel72MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel72MousePressed
        if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel72.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel72MousePressed

    private void jLabel69MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel69MousePressed
         if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel69.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel69MousePressed

    private void jLabel71MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel71MousePressed
         if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel71.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel71MousePressed

    private void jLabel70MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel70MousePressed
         if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel70.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel70MousePressed

    private void jLabel73MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel73MousePressed
         if (fila_actual == 5 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel73.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel73MousePressed

    private void jLabel38MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel38MousePressed
         if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel38.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel38MousePressed

    private void jLabel39MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MousePressed
         if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel39.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel39MousePressed

    private void jLabel41MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel41MousePressed
        if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel41MousePressed

    private void jLabel40MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MousePressed
         if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel40MousePressed

    private void jLabel45MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel45MousePressed
         if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel45.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel45MousePressed

    private void jLabel42MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel42MousePressed
         if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel42MousePressed

    private void jLabel44MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel44MousePressed
         if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel44MousePressed

    private void jLabel43MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel43MousePressed
         if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel43MousePressed

    private void jLabel46MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel46MousePressed
         if (fila_actual == 6 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel46.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel46MousePressed

    private void jLabel57MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel57MousePressed
        if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel57.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel57MousePressed

    private void jLabel58MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel58MousePressed
        if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel58.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel58MousePressed

    private void jLabel60MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel60MousePressed
         if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel60.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel60MousePressed

    private void jLabel59MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel59MousePressed
         if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel59.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel59MousePressed

    private void jLabel67MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel67MousePressed
         if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel67.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel67MousePressed

    private void jLabel61MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel61MousePressed
         if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel61.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel61MousePressed

    private void jLabel66MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel66MousePressed
         if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel66.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel66MousePressed

    private void jLabel65MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel65MousePressed
         if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel65.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel65MousePressed

    private void jLabel68MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel68MousePressed
         if (fila_actual == 7 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel68.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel68MousePressed

    private void jLabel47MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel47MousePressed
         if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel47.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel47MousePressed

    private void jLabel48MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel48MousePressed
         if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel48.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel48MousePressed

    private void jLabel50MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel50MousePressed
         if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel50MousePressed

    private void jLabel49MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel49MousePressed
         if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel49.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel49MousePressed

    private void jLabel55MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel55MousePressed
         if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel55MousePressed

    private void jLabel52MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel52MousePressed
         if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel52MousePressed

    private void jLabel54MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel54MousePressed
        if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel54MousePressed

    private void jLabel53MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel53MousePressed
         if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel53MousePressed

    private void jLabel56MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel56MousePressed
         if (fila_actual == 8 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel56.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel56MousePressed

    private void jLabel95MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel95MousePressed
        if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel95.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel95MousePressed

    private void jLabel96MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel96MousePressed
         if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel96.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel96MousePressed

    private void jLabel98MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel98MousePressed
        if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel98.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel98MousePressed

    private void jLabel97MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel97MousePressed
         if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel97.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel97MousePressed

    private void jLabel75MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel75MousePressed
        if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel75.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel75MousePressed

    private void jLabel99MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel99MousePressed
         if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel99.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel99MousePressed

    private void jLabel74MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel74MousePressed
         if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel74.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel74MousePressed

    private void jLabel100MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel100MousePressed
         if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel100.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel100MousePressed

    private void jLabel76MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel76MousePressed
        if (fila_actual == 9 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel76.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel76MousePressed

    private void jLabel86MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel86MousePressed
         if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel86.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel86MousePressed

    private void jLabel87MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel87MousePressed
        if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel87.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel87MousePressed

    private void jLabel89MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel89MousePressed
         if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel89.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel89MousePressed

    private void jLabel88MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel88MousePressed
         if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel88.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel88MousePressed

    private void jLabel93MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel93MousePressed
        if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel93.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel93MousePressed

    private void jLabel90MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel90MousePressed
         if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel90.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel90MousePressed

    private void jLabel92MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel92MousePressed
         if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel92.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel92MousePressed

    private void jLabel91MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel91MousePressed
         if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel91.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel91MousePressed

    private void jLabel94MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel94MousePressed
         if (fila_actual == 10 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel94.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel94MousePressed

    private void jLabel77MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel77MousePressed
        if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel77.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel77MousePressed

    private void jLabel78MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel78MousePressed
         if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel78.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel78MousePressed

    private void jLabel80MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel80MousePressed
         if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel80.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel80MousePressed

    private void jLabel79MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel79MousePressed
        if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel79.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel79MousePressed

    private void jLabel84MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel84MousePressed
         if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel84.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel84MousePressed

    private void jLabel81MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel81MousePressed
         if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel81.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel81MousePressed

    private void jLabel83MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel83MousePressed
         if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel83.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel83MousePressed

    private void jLabel82MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel82MousePressed
         if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel82.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel82MousePressed

    private void jLabel85MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel85MousePressed
          if (fila_actual == 11 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel85.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel85MousePressed

    private void jLabel131MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel131MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel131.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel131MousePressed

    private void jLabel114MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel114MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel114.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel114MousePressed

    private void jLabel111MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel111MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel111.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel111MousePressed

    private void jLabel119MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel119MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel119.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel119MousePressed

    private void jLabel109MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel109MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel109.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel109MousePressed

    private void jLabel123MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel123MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel123.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel123MousePressed

    private void jLabel103MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel103MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel103.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel103MousePressed

    private void jLabel135MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel135MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel135.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel135MousePressed

    private void jLabel101MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel101MousePressed
         if (fila_actual == 12 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel101.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel101MousePressed

    private void jLabel105MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel105MousePressed
        if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel105.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel105MousePressed

    private void jLabel130MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel130MousePressed
         if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel130.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel130MousePressed

    private void jLabel125MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel125MousePressed
         if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel125.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel125MousePressed

    private void jLabel116MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel116MousePressed
         if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel116.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel116MousePressed

    private void jLabel107MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel107MousePressed
         if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel107.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel107MousePressed

    private void jLabel117MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel117MousePressed
         if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel117.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel117MousePressed

    private void jLabel112MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel112MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel112MouseExited

    private void jLabel112MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel112MousePressed
         if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel112.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel112MousePressed

    private void jLabel133MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel133MousePressed
         if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel133.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel133MousePressed

    private void jLabel121MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel121MousePressed
        if (fila_actual == 13 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel121.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel121MousePressed

    private void jLabel110MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel110MousePressed
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel110.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel110MousePressed

    private void jLabel127MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel127MousePressed
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel127.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel127MousePressed

    private void jLabel120MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel120MousePressed
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel120.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel120MousePressed

    private void jLabel124MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel124MousePressed
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel124.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel124MousePressed

    private void jLabel113MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel113MousePressed
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel113.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel113MousePressed

    private void jLabel104MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel104MousePressed
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel104.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel104MousePressed

    private void jLabel118MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel118MousePressed
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel118.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel118MousePressed

    private void jLabel122MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel122MouseReleased
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel122.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel122MouseReleased

    private void jLabel126MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel126MousePressed
         if (fila_actual == 14 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel126.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel126MousePressed

    private void jLabel106MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel106MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(0, color_seleccionat);
                jLabel106.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel106MousePressed

    private void jLabel136MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel136MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(1, color_seleccionat);
                jLabel136.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel136MousePressed

    private void jLabel128MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel128MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(2, color_seleccionat);
                jLabel128.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel128MousePressed

    private void jLabel132MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel132MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(3, color_seleccionat);
                jLabel132.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel132MousePressed

    private void jLabel134MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel134MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(4, color_seleccionat);
                jLabel134.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel134MousePressed

    private void jLabel102MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel102MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(5, color_seleccionat);
                jLabel102.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel102MousePressed

    private void jLabel108MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel108MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(6, color_seleccionat);
                jLabel108.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel108MousePressed

    private void jLabel115MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel115MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(7, color_seleccionat);
                jLabel115.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel115MousePressed

    private void jLabel129MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel129MousePressed
         if (fila_actual == 15 & estat_joc_breaker){
            String peca_source = "/presentacio/" + color_seleccionat + "_25.png";
            if (color_seleccionat != 0){
                posicions_marcades.set(8, color_seleccionat);
                jLabel129.setIcon(new javax.swing.ImageIcon(getClass().getResource(peca_source)));
            }
        }
    }//GEN-LAST:event_jLabel129MousePressed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        Object[] options = {"Sí", "No"};
        int res = javax.swing.JOptionPane.showOptionDialog(this, "Realmente quiere salir? Perderá todo el progreso no guardado", "Salir", javax.swing.JOptionPane.YES_NO_OPTION, javax.swing.JOptionPane.WARNING_MESSAGE, null, options, options[1]);
        if(res == 0){
            this.setVisible(false);
            vistaMenuJugador.setVisible(true);
        }
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        cp.guardar_partida();
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        canvia_color_seleccionat(7);
        jButton1.setBorderPainted(false);
        jButton2.setBorderPainted(false);
        jButton3.setBorderPainted(false);
        jButton4.setBorderPainted(false);
        jButton5.setBorderPainted(false);
        jButton6.setBorderPainted(false);
        jButton7.setBorderPainted(true);
        jButton8.setBorderPainted(false);
        jButton9.setBorderPainted(false);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        canvia_color_seleccionat(9);
        jButton1.setBorderPainted(false);
        jButton2.setBorderPainted(false);
        jButton3.setBorderPainted(false);
        jButton4.setBorderPainted(false);
        jButton5.setBorderPainted(false);
        jButton6.setBorderPainted(false);
        jButton7.setBorderPainted(false);
        jButton8.setBorderPainted(false);
        jButton9.setBorderPainted(true);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        canvia_color_seleccionat(5);
        jButton1.setBorderPainted(false);
        jButton2.setBorderPainted(false);
        jButton3.setBorderPainted(false);
        jButton4.setBorderPainted(false);
        jButton5.setBorderPainted(true);
        jButton6.setBorderPainted(false);
        jButton7.setBorderPainted(false);
        jButton8.setBorderPainted(false);
        jButton9.setBorderPainted(false);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        canvia_color_seleccionat(8);
        jButton1.setBorderPainted(false);
        jButton2.setBorderPainted(false);
        jButton3.setBorderPainted(false);
        jButton4.setBorderPainted(false);
        jButton5.setBorderPainted(false);
        jButton6.setBorderPainted(false);
        jButton7.setBorderPainted(false);
        jButton8.setBorderPainted(true);
        jButton9.setBorderPainted(false);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        boolean segueix = true;
        int totals_be = 0;
        for (int i = 0; i < columnes; i++){
            System.out.println(posicions_marcades.get(i) );
            if (posicions_marcades.get(i) == 0) segueix = false;
        }
        boolean repes = false;
        for(int i = 0; i < columnes; i++){
            for(int j = i+1; j < columnes; j++){
                if (posicions_marcades.get(i) == posicions_marcades.get(j)) repes = true;
            }
        }
        if (repes){
            javax.swing.JOptionPane.showMessageDialog(this, "No puedes repetir colores", "Colores repetidos", javax.swing.JOptionPane.ERROR_MESSAGE);
        }else{
            if (segueix){
                List<Integer> resultat = new ArrayList<Integer>();
                try{
                    resultat = cp.jugaFila(posicions_marcades, fila_actual); //Retorna pair<posicio+color, posicio>
                }catch (Exception e){
                    System.out.println("error");
                }

                neteja_posicions();

                //Posar quines ha encertat i quines no
                marca_encerts(fila_actual, resultat.get(0), resultat.get(1)); //vermell blanc
                cp.set_encert_pos_col(fila_actual, resultat.get(0));
                cp.set_encert_pos(fila_actual, resultat.get(1));
                totals_be = resultat.get(0);

                fila_actual++;
            }else{
                javax.swing.JOptionPane.showMessageDialog(this, "Marca todas las casillas", "Casillas no marcadas", javax.swing.JOptionPane.ERROR_MESSAGE);
            }
            if (totals_be == columnes){
                int puntuacio = cp.getpuntuacio(fila_actual-2);
                javax.swing.JOptionPane.showMessageDialog(this, "Felicidades! Has ganado. Tu puntuación ha sido: " + puntuacio, "Has ganado", javax.swing.JOptionPane.ERROR_MESSAGE);
                boolean millor = cp.afegeix_puntuacio_jugador(puntuacio);
                if (millor){
                    javax.swing.JOptionPane.showMessageDialog(this, "Felicidades! Has mejorado tu puntuación personal!", "Puntuación mejorada", javax.swing.JOptionPane.ERROR_MESSAGE);
                }
                cp.afegir_ranking(puntuacio);
                this.setVisible(false);
                vistaMenuJugador.setVisible(true);
            }else if (fila_actual > files){
                javax.swing.JOptionPane.showMessageDialog(this, "Oooh, no has conseguido resolver la partida :(", "Has perdido", javax.swing.JOptionPane.ERROR_MESSAGE); 
                this.setVisible(false);
                vistaMenuJugador.setVisible(true);
            }
        }
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        canvia_color_seleccionat(3);
        jButton1.setBorderPainted(false);
        jButton2.setBorderPainted(false);
        jButton3.setBorderPainted(true);
        jButton4.setBorderPainted(false);
        jButton5.setBorderPainted(false);
        jButton6.setBorderPainted(false);
        jButton7.setBorderPainted(false);
        jButton8.setBorderPainted(false);
        jButton9.setBorderPainted(false);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        canvia_color_seleccionat(1);
        jButton1.setBorderPainted(true);
        jButton2.setBorderPainted(false);
        jButton3.setBorderPainted(false);
        jButton4.setBorderPainted(false);
        jButton5.setBorderPainted(false);
        jButton6.setBorderPainted(false);
        jButton7.setBorderPainted(false);
        jButton8.setBorderPainted(false);
        jButton9.setBorderPainted(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        canvia_color_seleccionat(2);
        jButton1.setBorderPainted(false);
        jButton2.setBorderPainted(true);
        jButton3.setBorderPainted(false);
        jButton4.setBorderPainted(false);
        jButton5.setBorderPainted(false);
        jButton6.setBorderPainted(false);
        jButton7.setBorderPainted(false);
        jButton8.setBorderPainted(false);
        jButton9.setBorderPainted(false);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        canvia_color_seleccionat(6);
        jButton1.setBorderPainted(false);
        jButton2.setBorderPainted(false);
        jButton3.setBorderPainted(false);
        jButton4.setBorderPainted(false);
        jButton5.setBorderPainted(false);
        jButton6.setBorderPainted(true);
        jButton7.setBorderPainted(false);
        jButton8.setBorderPainted(false);
        jButton9.setBorderPainted(false);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        canvia_color_seleccionat(4);
        jButton1.setBorderPainted(false);
        jButton2.setBorderPainted(false);
        jButton3.setBorderPainted(false);
        jButton4.setBorderPainted(true);
        jButton5.setBorderPainted(false);
        jButton6.setBorderPainted(false);
        jButton7.setBorderPainted(false);
        jButton8.setBorderPainted(false);
        jButton9.setBorderPainted(false);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        pistes_colors++;
        Random rn = new Random();
        int answer = rn.nextInt(columnes) + 1;
        mostra_pista_color(answer);
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        Random rn = new Random();
        int answer = rn.nextInt(columnes) + 1;
        mostra_pista(answer);
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        VDemanarPista vistaDP = new VDemanarPista(this, columnes);
        this.setVisible(false);
        vistaDP.setVisible(true);
       
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jLabel137MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel137MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel137MousePressed

    private void jLabel138MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel138MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel138MousePressed

    private void jLabel139MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel139MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel139MousePressed

    private void jLabel140MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel140MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel140MousePressed

    private void jLabel141MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel141MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel141MousePressed

    private void jLabel142MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel142MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel142MousePressed

    private void jLabel143MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel143MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel143MousePressed

    private void jLabel144MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel144MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel144MousePressed

    private void jLabel145MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel145MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel145MousePressed

    private void jLabel298MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel298MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel298MousePressed

    private void jLabel297MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel297MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel297MousePressed

    private void jLabel296MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel296MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel296MousePressed

    private void jLabel295MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel295MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel295MousePressed

    private void jLabel294MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel294MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel294MousePressed

    private void jLabel293MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel293MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel293MousePressed

    private void jLabel292MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel292MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel292MousePressed

    private void jLabel291MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel291MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel291MousePressed

    private void jLabel290MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel290MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel290MousePressed

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
        if (comprova_solucio_possible()){
            if(avis_vermell == cp.getcolumnes()){
               javax.swing.JOptionPane.showMessageDialog(this, "La solución ha sido encontrada"); 
               this.setVisible(false);
               vistaMenuJugador.setVisible(true);
            }
            else if(fila_actual == cp.getfiles()){
               javax.swing.JOptionPane.showMessageDialog(this, "No se ha encontrado la solución"); 
               this.setVisible(false);
               vistaMenuJugador.setVisible(true);
            }
            else{
                //cp.setFilaComp(fila_actual-1, avis_vermell, avis_blanc); 
                fila_actual++;
                //afegeix_linea(fila_actual, cp.getCodi(fila_actual-1));
                List<Integer> codi_seguent = cp.seguentJugada(avis_vermell, avis_blanc);
                set_seguent_jugada(codi_seguent);
                afegeix_linea(fila_actual, codi_seguent);
                neteja_color_avis();
                avis_blanc = 0;
                avis_vermell = 0;
            }
        }else{
            javax.swing.JOptionPane.showMessageDialog(this, "¿Estás seguro de que esta es la respuesta correcta?"); 
        }
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jLabel164MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel164MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel164.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel164MousePressed

    private void jLabel165MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel165MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel165.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel165MousePressed

    private void jLabel166MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel166MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel166.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel166MousePressed

    private void jLabel167MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel167MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel167.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel167MousePressed

    private void jLabel168MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel168MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel168.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel168MousePressed

    private void jLabel169MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel169MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel169.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel169MousePressed

    private void jLabel170MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel170MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel170.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel170MousePressed

    private void jLabel171MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel171MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel171.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel171MousePressed

    private void jLabel172MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel172MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 1 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel172.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel172MousePressed

    private void jLabel146MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel146MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel146.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel146MousePressed

    private void jLabel147MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel147MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel147.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel147MousePressed

    private void jLabel148MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel148MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel148.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel148MousePressed

    private void jLabel149MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel149MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel149.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel149MousePressed

    private void jLabel150MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel150MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel150.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel150MousePressed

    private void jLabel151MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel151MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel151.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel151MousePressed

    private void jLabel152MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel152MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel152.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel152MousePressed

    private void jLabel153MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel153MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel153.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel153MousePressed

    private void jLabel154MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel154MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 2 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel154.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel154MousePressed

    private void jLabel155MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel155MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel155.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel155MousePressed

    private void jLabel160MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel160MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel160.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel160MousePressed

    private void jLabel159MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel159MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel159.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel159MousePressed

    private void jLabel156MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel156MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel156.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel156MousePressed

    private void jLabel162MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel162MousePressed
       //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel162.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel162MousePressed

    private void jLabel157MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel157MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel157.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel157MousePressed

    private void jLabel163MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel163MousePressed
       //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel163.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel163MousePressed

    private void jLabel158MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel158MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel158.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel158MousePressed

    private void jLabel161MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel161MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 3 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel161.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel161MousePressed

    private void jLabel173MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel173MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel173.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel173MousePressed

    private void jLabel174MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel174MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel174.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel174MousePressed

    private void jLabel175MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel175MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel175.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel175MousePressed

    private void jLabel176MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel176MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel176.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel176MousePressed

    private void jLabel177MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel177MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel177.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel177MousePressed

    private void jLabel178MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel178MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel178.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel178MousePressed

    private void jLabel179MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel179MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel179.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel179MousePressed

    private void jLabel180MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel180MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel180.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel180MousePressed

    private void jLabel181MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel181MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 4 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel181.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel181MousePressed

    private void jLabel182MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel182MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel182.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel182MousePressed

    private void jLabel183MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel183MousePressed
         //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel183.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel183MousePressed

    private void jLabel184MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel184MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel184.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel184MousePressed

    private void jLabel185MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel185MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel185.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel185MousePressed

    private void jLabel186MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel186MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel186.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel186MousePressed

    private void jLabel187MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel187MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel187.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel187MousePressed

    private void jLabel188MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel188MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel188.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel188MousePressed

    private void jLabel189MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel189MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel189.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel189MousePressed

    private void jLabel190MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel190MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 5 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel190.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel190MousePressed

    private void jLabel191MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel191MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel191.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel191MousePressed

    private void jLabel192MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel192MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel192.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel192MousePressed

    private void jLabel193MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel193MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel193.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel193MousePressed

    private void jLabel194MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel194MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel194.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel194MousePressed

    private void jLabel195MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel195MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel195.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel195MousePressed

    private void jLabel196MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel196MousePressed
       //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel196.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel196MousePressed

    private void jLabel197MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel197MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel197.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel197MousePressed

    private void jLabel198MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel198MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel198.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel198MousePressed

    private void jLabel199MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel199MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 6 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel199.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel199MousePressed

    private void jLabel200MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel200MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel200.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel200MousePressed

    private void jLabel201MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel201MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel201.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel201MousePressed

    private void jLabel202MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel202MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel202.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel202MousePressed

    private void jLabel203MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel203MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel203.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel203MousePressed

    private void jLabel204MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel204MousePressed
       //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel204.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel204MousePressed

    private void jLabel205MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel205MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel205.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel205MousePressed

    private void jLabel206MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel206MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel206.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel206MousePressed

    private void jLabel207MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel207MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel207.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel207MousePressed

    private void jLabel208MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel208MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 7 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel208.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel208MousePressed

    private void jLabel209MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel209MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel209.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel209MousePressed

    private void jLabel210MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel210MousePressed
         //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel210MousePressed

    private void jLabel211MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel211MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel211.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel211MousePressed

    private void jLabel212MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel212MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel212.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel212MousePressed

    private void jLabel213MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel213MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel213.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel213MousePressed

    private void jLabel214MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel214MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel214.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel214MousePressed

    private void jLabel215MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel215MousePressed
         //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel215.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel215MousePressed

    private void jLabel216MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel216MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel216.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel216MousePressed

    private void jLabel217MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel217MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 8 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel217.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel217MousePressed

    private void jLabel218MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel218MousePressed
       //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel218.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel218MousePressed

    private void jLabel219MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel219MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel219.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel219MousePressed

    private void jLabel220MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel220MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel220.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel220MousePressed

    private void jLabel221MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel221MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel221.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel221MousePressed

    private void jLabel222MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel222MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel222.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel222MousePressed

    private void jLabel223MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel223MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel223.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel223MousePressed

    private void jLabel224MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel224MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel224.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel224MousePressed

    private void jLabel225MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel225MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel225.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel225MousePressed

    private void jLabel226MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel226MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 9 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel226.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel226MousePressed

    private void jLabel227MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel227MousePressed
        //canviar la fila actual, el get i el set i el label
        if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel227.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel227MousePressed

    private void jLabel228MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel228MousePressed
       if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel228.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel228MousePressed

    private void jLabel229MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel229MouseReleased
        
    }//GEN-LAST:event_jLabel229MouseReleased

    private void jLabel229MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel229MousePressed
         if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel229.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel229MousePressed

    private void jLabel230MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel230MousePressed
        if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel230.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel230MousePressed

    private void jLabel231MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel231MousePressed
        if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel231.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel231MousePressed

    private void jLabel232MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel232MousePressed
        if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel232.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel232MousePressed

    private void jLabel233MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel233MousePressed
        if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel233.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel233MousePressed

    private void jLabel234MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel234MousePressed
        if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel234.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel234MousePressed

    private void jLabel235MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel235MousePressed
        if (fila_actual == 10 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel235.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel235MousePressed

    private void jLabel236MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel236MousePressed
        if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel236.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel236MousePressed

    private void jLabel237MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel237MousePressed
        if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel237.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel237MousePressed

    private void jLabel238MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel238MousePressed
        if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel238.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel238MousePressed

    private void jLabel239MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel239MousePressed
       if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel239.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel239MousePressed

    private void jLabel240MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel240MousePressed
        if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel240.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel240MousePressed

    private void jLabel241MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel241MousePressed
        if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel241.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel241MousePressed

    private void jLabel242MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel242MousePressed
       if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel242.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel242MousePressed

    private void jLabel243MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel243MousePressed
        if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel243.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel243MousePressed

    private void jLabel244MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel244MousePressed
        if (fila_actual == 11 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel244.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel244MousePressed

    private void jLabel245MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel245MousePressed
       if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel245.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel245MousePressed

    private void jLabel246MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel246MousePressed
         if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel246.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel246MousePressed

    private void jLabel247MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel247MousePressed
        if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel247.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel247MousePressed

    private void jLabel248MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel248MousePressed
       if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel248.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel248MousePressed

    private void jLabel249MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel249MousePressed
        if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel249.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel249MousePressed

    private void jLabel250MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel250MousePressed
        if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel250.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel250MousePressed

    private void jLabel251MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel251MousePressed
        if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel251.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel251MousePressed

    private void jLabel252MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel252MousePressed
        if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel252.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel252MousePressed

    private void jLabel253MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel253MousePressed
        if (fila_actual == 12 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel253.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel253MousePressed

    private void jLabel254MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel254MousePressed
        if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel254.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel254MousePressed

    private void jLabel255MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel255MousePressed
       if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel255.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel255MousePressed

    private void jLabel256MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel256MousePressed
        if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel256.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel256MousePressed

    private void jLabel257MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel257MousePressed
        if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel257.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel257MousePressed

    private void jLabel258MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel258MousePressed
       if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel258.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel258MousePressed

    private void jLabel259MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel259MousePressed
       if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel259.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel259MousePressed

    private void jLabel260MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel260MousePressed
        if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel260.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel260MousePressed

    private void jLabel261MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel261MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel261MouseExited

    private void jLabel261MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel261MousePressed
        if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel261.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel261MousePressed

    private void jLabel262MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel262MousePressed
        if (fila_actual == 13 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel262.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel262MousePressed

    private void jLabel263MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel263MousePressed
        if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel263.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel263MousePressed

    private void jLabel264MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel264MousePressed
        if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel264.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel264MousePressed

    private void jLabel265MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel265MousePressed
        if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel265.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel265MousePressed

    private void jLabel266MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel266MousePressed
       if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel266.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel266MousePressed

    private void jLabel267MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel267MousePressed
        if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel267.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel267MousePressed

    private void jLabel268MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel268MousePressed
        if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel268.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel268MousePressed

    private void jLabel269MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel269MousePressed
       if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel269.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel269MousePressed

    private void jLabel270MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel270MousePressed
        if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel270.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel270MousePressed

    private void jLabel271MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel271MousePressed
        if (fila_actual == 14 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel271.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel271MousePressed

    private void jLabel272MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel272MousePressed
       if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(0);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(0, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel272.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel272MousePressed

    private void jLabel273MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel273MousePressed
        if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(1);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(1, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel273.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel273MousePressed

    private void jLabel274MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel274MousePressed
        if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(2);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(2, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel274.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel274MousePressed

    private void jLabel275MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel275MousePressed
        if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(3);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(3, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel275.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel275MousePressed

    private void jLabel276MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel276MousePressed
         if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(4);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(4, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel276.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel276MousePressed

    private void jLabel277MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel277MousePressed
       if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(5);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(5, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel277.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel277MousePressed

    private void jLabel278MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel278MousePressed
        if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(6);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(6, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel278.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel278MousePressed

    private void jLabel279MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel279MousePressed
        if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(7);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(7, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel279.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel279MousePressed

    private void jLabel280MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel280MousePressed
       if (fila_actual == 15 & !estat_joc_breaker){
            int color_avis2 = color_avis.get(8);
            int nou_color_avis = 0;
            if (color_avis2 == 0){
                nou_color_avis = 1;
                avis_vermell++;
            }else if (color_avis2 == 1){
                nou_color_avis = 2;
                avis_vermell--;
                avis_blanc++;
            }else if (color_avis2 == 2){
                nou_color_avis = 0;
                avis_blanc--;
            }
            color_avis.set(8, nou_color_avis);
            String nom_del_color = "";
            switch(nou_color_avis){
                case 0:
                    nom_del_color = "negre";
                    break;
                case 1:
                    nom_del_color = "vermell";
                    break;
                case 2:
                    nom_del_color = "blanc";
                    break;
            }
            jLabel280.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentacio/" + nom_del_color + "_9.png")));
        }
    }//GEN-LAST:event_jLabel280MousePressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel100;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel102;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel105;
    private javax.swing.JLabel jLabel106;
    private javax.swing.JLabel jLabel107;
    private javax.swing.JLabel jLabel108;
    private javax.swing.JLabel jLabel109;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel110;
    private javax.swing.JLabel jLabel111;
    private javax.swing.JLabel jLabel112;
    private javax.swing.JLabel jLabel113;
    private javax.swing.JLabel jLabel114;
    private javax.swing.JLabel jLabel115;
    private javax.swing.JLabel jLabel116;
    private javax.swing.JLabel jLabel117;
    private javax.swing.JLabel jLabel118;
    private javax.swing.JLabel jLabel119;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel120;
    private javax.swing.JLabel jLabel121;
    private javax.swing.JLabel jLabel122;
    private javax.swing.JLabel jLabel123;
    private javax.swing.JLabel jLabel124;
    private javax.swing.JLabel jLabel125;
    private javax.swing.JLabel jLabel126;
    private javax.swing.JLabel jLabel127;
    private javax.swing.JLabel jLabel128;
    private javax.swing.JLabel jLabel129;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel130;
    private javax.swing.JLabel jLabel131;
    private javax.swing.JLabel jLabel132;
    private javax.swing.JLabel jLabel133;
    private javax.swing.JLabel jLabel134;
    private javax.swing.JLabel jLabel135;
    private javax.swing.JLabel jLabel136;
    private javax.swing.JLabel jLabel137;
    private javax.swing.JLabel jLabel138;
    private javax.swing.JLabel jLabel139;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel140;
    private javax.swing.JLabel jLabel141;
    private javax.swing.JLabel jLabel142;
    private javax.swing.JLabel jLabel143;
    private javax.swing.JLabel jLabel144;
    private javax.swing.JLabel jLabel145;
    private javax.swing.JLabel jLabel146;
    private javax.swing.JLabel jLabel147;
    private javax.swing.JLabel jLabel148;
    private javax.swing.JLabel jLabel149;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel150;
    private javax.swing.JLabel jLabel151;
    private javax.swing.JLabel jLabel152;
    private javax.swing.JLabel jLabel153;
    private javax.swing.JLabel jLabel154;
    private javax.swing.JLabel jLabel155;
    private javax.swing.JLabel jLabel156;
    private javax.swing.JLabel jLabel157;
    private javax.swing.JLabel jLabel158;
    private javax.swing.JLabel jLabel159;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel160;
    private javax.swing.JLabel jLabel161;
    private javax.swing.JLabel jLabel162;
    private javax.swing.JLabel jLabel163;
    private javax.swing.JLabel jLabel164;
    private javax.swing.JLabel jLabel165;
    private javax.swing.JLabel jLabel166;
    private javax.swing.JLabel jLabel167;
    private javax.swing.JLabel jLabel168;
    private javax.swing.JLabel jLabel169;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel170;
    private javax.swing.JLabel jLabel171;
    private javax.swing.JLabel jLabel172;
    private javax.swing.JLabel jLabel173;
    private javax.swing.JLabel jLabel174;
    private javax.swing.JLabel jLabel175;
    private javax.swing.JLabel jLabel176;
    private javax.swing.JLabel jLabel177;
    private javax.swing.JLabel jLabel178;
    private javax.swing.JLabel jLabel179;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel180;
    private javax.swing.JLabel jLabel181;
    private javax.swing.JLabel jLabel182;
    private javax.swing.JLabel jLabel183;
    private javax.swing.JLabel jLabel184;
    private javax.swing.JLabel jLabel185;
    private javax.swing.JLabel jLabel186;
    private javax.swing.JLabel jLabel187;
    private javax.swing.JLabel jLabel188;
    private javax.swing.JLabel jLabel189;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel190;
    private javax.swing.JLabel jLabel191;
    private javax.swing.JLabel jLabel192;
    private javax.swing.JLabel jLabel193;
    private javax.swing.JLabel jLabel194;
    private javax.swing.JLabel jLabel195;
    private javax.swing.JLabel jLabel196;
    private javax.swing.JLabel jLabel197;
    private javax.swing.JLabel jLabel198;
    private javax.swing.JLabel jLabel199;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel200;
    private javax.swing.JLabel jLabel201;
    private javax.swing.JLabel jLabel202;
    private javax.swing.JLabel jLabel203;
    private javax.swing.JLabel jLabel204;
    private javax.swing.JLabel jLabel205;
    private javax.swing.JLabel jLabel206;
    private javax.swing.JLabel jLabel207;
    private javax.swing.JLabel jLabel208;
    private javax.swing.JLabel jLabel209;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel210;
    private javax.swing.JLabel jLabel211;
    private javax.swing.JLabel jLabel212;
    private javax.swing.JLabel jLabel213;
    private javax.swing.JLabel jLabel214;
    private javax.swing.JLabel jLabel215;
    private javax.swing.JLabel jLabel216;
    private javax.swing.JLabel jLabel217;
    private javax.swing.JLabel jLabel218;
    private javax.swing.JLabel jLabel219;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel220;
    private javax.swing.JLabel jLabel221;
    private javax.swing.JLabel jLabel222;
    private javax.swing.JLabel jLabel223;
    private javax.swing.JLabel jLabel224;
    private javax.swing.JLabel jLabel225;
    private javax.swing.JLabel jLabel226;
    private javax.swing.JLabel jLabel227;
    private javax.swing.JLabel jLabel228;
    private javax.swing.JLabel jLabel229;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel230;
    private javax.swing.JLabel jLabel231;
    private javax.swing.JLabel jLabel232;
    private javax.swing.JLabel jLabel233;
    private javax.swing.JLabel jLabel234;
    private javax.swing.JLabel jLabel235;
    private javax.swing.JLabel jLabel236;
    private javax.swing.JLabel jLabel237;
    private javax.swing.JLabel jLabel238;
    private javax.swing.JLabel jLabel239;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel240;
    private javax.swing.JLabel jLabel241;
    private javax.swing.JLabel jLabel242;
    private javax.swing.JLabel jLabel243;
    private javax.swing.JLabel jLabel244;
    private javax.swing.JLabel jLabel245;
    private javax.swing.JLabel jLabel246;
    private javax.swing.JLabel jLabel247;
    private javax.swing.JLabel jLabel248;
    private javax.swing.JLabel jLabel249;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel250;
    private javax.swing.JLabel jLabel251;
    private javax.swing.JLabel jLabel252;
    private javax.swing.JLabel jLabel253;
    private javax.swing.JLabel jLabel254;
    private javax.swing.JLabel jLabel255;
    private javax.swing.JLabel jLabel256;
    private javax.swing.JLabel jLabel257;
    private javax.swing.JLabel jLabel258;
    private javax.swing.JLabel jLabel259;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel260;
    private javax.swing.JLabel jLabel261;
    private javax.swing.JLabel jLabel262;
    private javax.swing.JLabel jLabel263;
    private javax.swing.JLabel jLabel264;
    private javax.swing.JLabel jLabel265;
    private javax.swing.JLabel jLabel266;
    private javax.swing.JLabel jLabel267;
    private javax.swing.JLabel jLabel268;
    private javax.swing.JLabel jLabel269;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel270;
    private javax.swing.JLabel jLabel271;
    private javax.swing.JLabel jLabel272;
    private javax.swing.JLabel jLabel273;
    private javax.swing.JLabel jLabel274;
    private javax.swing.JLabel jLabel275;
    private javax.swing.JLabel jLabel276;
    private javax.swing.JLabel jLabel277;
    private javax.swing.JLabel jLabel278;
    private javax.swing.JLabel jLabel279;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel280;
    private javax.swing.JLabel jLabel281;
    private javax.swing.JLabel jLabel282;
    private javax.swing.JLabel jLabel283;
    private javax.swing.JLabel jLabel284;
    private javax.swing.JLabel jLabel285;
    private javax.swing.JLabel jLabel286;
    private javax.swing.JLabel jLabel287;
    private javax.swing.JLabel jLabel288;
    private javax.swing.JLabel jLabel289;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel290;
    private javax.swing.JLabel jLabel291;
    private javax.swing.JLabel jLabel292;
    private javax.swing.JLabel jLabel293;
    private javax.swing.JLabel jLabel294;
    private javax.swing.JLabel jLabel295;
    private javax.swing.JLabel jLabel296;
    private javax.swing.JLabel jLabel297;
    private javax.swing.JLabel jLabel298;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel97;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    // End of variables declaration//GEN-END:variables
}
